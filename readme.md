# natbraille-editor - web gui for Natbraille

<img alt="" aria-hidden="true" src="docs/screenshot.png" width="400" />

## License

natbraille-editor is free software, distributed under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE Version 3. 
For more information, see the file COPYING. 

## Build 

vite is used for dev and bundling:

- develop: `npm run serve`
- build  : `npm run build`

natbraille-editor serves as the client, requiring  [natbraille-editor-server](https://framagit.org/natbraille-association/natbraille/-/tree/dev/natbraille-editor-server) for its functionality.

## Used packages

This software relies primarily on the following packages

- [Prosemirror](https://prosemirror.net/)
- React
- Bootstrap
- sass

