export const disabledStyle = {
    pointerEvents: 'none', /* Prevents clicks and user interaction */
    opacity: '0.5', /* Makes the div appear greyed out */
}
