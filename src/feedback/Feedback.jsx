import React, { useState, useEffect, useId } from 'react';
import './feedback.css'
import { postFeedback } from '../serverio/feedback';
export const ContactForm = ({ defaultEmail, defaultOpened }) => {

    const labels = {
        "BUG": "Signaler un bug",
        "FEATURE": "Suggestion d’amélioration(s)",
        "OTHER": "Autre",
        "subject": "Objet",
        "description": "Description",
        "contact": "Adresse email de contact",
        "sentOk": "Merci pour votre retour !",
        "sentKo": "Un problème est survenu lors de l'envoi du message..."
    }

    const [opened, setOpened] = useState('');
    const [nature, setNature] = useState('');
    const [subject, setSubject] = useState('');
    const [description, setDescription] = useState('');
    const [email, setEmail] = useState('');

    const idSubject = useId()
    const idDescription = useId()
    const idEmail = useId()

    const SENT_STATE_NONE = 0;
    const SENT_STATE_WAITING = 1;
    const SENT_STATE_OK = 2;
    const SENT_STATE_KO = 3;

    const [sentState, setSentState] = useState(SENT_STATE_NONE);

    // Préremplir le champ email si une adresse par défaut est fournie
    useEffect(() => {
        if (defaultEmail) {
            setEmail(defaultEmail);
        }
    }, [defaultEmail]);

    const resetForm = () => {
        setOpened("")
        setNature("")
        setSubject("")
        setDescription("")
        setEmail("")
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        setSentState(SENT_STATE_WAITING)
        const formData = {
            nature,
            subject,
            description,
            email
        };
        // Envoyer les données du formulaire
        console.log('Form data submitted:', formData);

        const send = async () => {
            const ok = await postFeedback(formData);
            if (ok) {
                setSentState(SENT_STATE_OK)
                alert(labels['sentOk'])
                resetForm();
                setSentState(SENT_STATE_NONE)
            } else {
                setSentState(SENT_STATE_KO)
                alert(labels['sentKo'])
                setSentState(SENT_STATE_NONE)
            }
        }
        send();
    };


    return (
        <div className="contact-form">
            <h1>Contact</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <fieldset >
                        <p>Préciser la nature de votre message</p>
                        <div className="form-check">
                            <label className="form-check-label">{labels["BUG"]}</label>
                            <input className="form-check-input"
                                type="radio"
                                value="BUG"
                                checked={nature === 'BUG'}
                                onChange={(e) => setNature(e.target.value)}
                            />
                        </div>
                        <div className="form-check">
                            <label className="form-check-label">{labels["FEATURE"]}</label>
                            <input className="form-check-input"
                                type="radio"
                                value="FEATURE"
                                checked={nature === 'FEATURE'}
                                onChange={(e) => setNature(e.target.value)}
                            />
                        </div>
                        <div className="form-check">
                            <label className="form-check-label">{labels["OTHER"]}</label>
                            <input className="form-check-input"
                                type="radio"
                                value="OTHER"
                                checked={nature === 'OTHER'}
                                onChange={(e) => setNature(e.target.value)}
                            />
                        </div>
                    </fieldset>
                </div>
                <div className="mb-3">
                    <label className="form-label">{labels["subject"]}:</label>
                    <input className="form-control"
                        type="text"
                        value={subject}
                        onChange={(e) => setSubject(e.target.value)}
                    />
                </div>

                <div className="mb-3">
                    <label className="form-label">{labels["description"]}:</label>
                    <textarea className="form-control"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </div>

                <div className="mb-3">
                    <label className="form-label">{labels["contact"]}:</label>
                    <input className="form-control"
                        type="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </div>

                <button disabled={sentState !== SENT_STATE_NONE} className="btn btn-primary" type="submit">Envoyer</button>
            </form>
        </div>
    );

};