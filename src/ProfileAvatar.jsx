import React, { useEffect, useRef, useState } from "react";
import "./ProfileAvatar.css";

function nameToAvatarInitials(name) {
    if (!name) return '';
    const nameParts = name.split(' ');
    const initials = nameParts
        .filter(x => x !== undefined)
        .map(part => part.charAt(0).toUpperCase())
        .join('');
    return initials.slice(0, 2);
}

function ProfileAvatar({ name }) {
    const initials = nameToAvatarInitials(name);
    const [fontSize, setFontSize] = useState(0);
    const textRef = useRef(null);
    const circleRef = useRef(null);

    useEffect(() => {
        if (!textRef.current || !circleRef.current) return;
        const circle = circleRef.current;
        const text = textRef.current;

        const circleWidth = circle.offsetWidth;
        const circleHeight = circle.offsetHeight;

        const doesTextFit = () => {
            const marginRatio = 0.65;
            const textWidth = text.offsetWidth;
            const textHeight = text.offsetHeight;
            return textWidth <= (marginRatio * circleWidth) && textHeight <= (marginRatio * circleHeight);
        };

        let newFontSize = Math.min(circleWidth, circleHeight);

        while (newFontSize > 0) {
            text.style.fontSize = `${newFontSize}px`;
            if (doesTextFit()) break;
            newFontSize -= 0.1;
        }

        setFontSize(newFontSize);
    }, [name]);

    return (
        <div className="profile-avatar" ref={circleRef}>
            <span className="avatar-initials" ref={textRef} style={{ fontSize: `${fontSize}px` }}>
                {initials}
            </span>
        </div>
    );
}

export default ProfileAvatar;

