//
// - In development mode, vite serves web pages and assets on its dev server 
//   (by default on port 5173) while the java server runs independently,
//   so the server port must be specified for natbraille api requests.
// - In production mode, the js build bundle produced by `npm run build` is served
//   by the same java server that serves the natbraille api, so the server configuration
//   **must** be omited
//

const isProductionMode = import.meta.env.PROD
export const ClientConfig = isProductionMode ? ({
}) : {
    server: {
        port: '4567',
    }
}
