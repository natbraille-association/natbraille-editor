function rotPal() {
    const enc = str => str.split('').map(x => String.fromCharCode(x.charCodeAt(0) - 1)).join('')
    const dec = str => str.split('').map(x => String.fromCharCode(x.charCodeAt(0) + 1)).join('')
    const target = dec("uhsd tmdahdqd")
    const ca = ['color', 'background-color', 'border-color']
    let p = 0;
    let state = 0
    let intervalId = null;
    let els = []
  
    const start = () => {
      const rotate = () => {
        els.forEach(e => {
          const h = (e.hue + 0.1 * (Date.now() * 360 / 1000))
          const hoff = [0, 180, 90];
          ca.forEach((ca, i) => {
            e.$e.style[ca] = `hsl(${(h + hoff[i]) % 360},100%,50%)`
          })
        })
      }
      intervalId = setInterval(rotate, 16)
    }
  
    const stop = () => {
      if (state == 0) return;
      clearInterval(intervalId)
      els.forEach(e => {
        ca.forEach(a => {
          if (e.a == null) {
            e.$e.style.removeProperty(a)
          } else {
            e.$e.style[a] = e[a]
          }
        })
      })
      state = 0;
    }
  
    const match = () => {
      els = Array.from(document.querySelectorAll('*')).map($e => ({
        $e,
        hue: Math.random() * 360,
        ...Object.fromEntries(ca.map(n => [n, $e.style[n]]))
      }))
      start()
      setTimeout(stop, 20000)
    }
  
    window.addEventListener('keydown', ({ repeat, key }) => {
      if (repeat) return;
      if (state == 1) {
        stop()
      } else {
        p += (key === target[p]) ? 1 : (-p);
        if ((p === target.length)) {
          p = 0;
          state = 1;
          match();
        }
      }
    })
  }
  rotPal()
  