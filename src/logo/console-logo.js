import natbrailleTextLogo from './natbraille-text-logo.txt?raw'
import { ClientConfig } from '../client-config'

// logo
const paddedLogo = natbrailleTextLogo.split("\n").map(t => `    ${t}    `).join("\n").split('')
const s1 = paddedLogo.map(() => "%c%s").join('')
const s2 = paddedLogo.flatMap((c, i, a) => [`color:hsl(${360 * (i / a.length)}deg 80% 50%);background-color:#000;font-weight:1000;line-height:1.1;`, c])
console.log(s1, ...s2);

// dev/prod + client config
const vite_production_or_development = import.meta.env.MODE;
const css = `font-weight:1000`
console.log("%c%s", css, ['🏭', 'mode:', vite_production_or_development, JSON.stringify(ClientConfig)].join(" "))

