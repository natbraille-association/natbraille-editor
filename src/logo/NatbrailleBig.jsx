import logo from "../../assets/logoNat.svg"
export const NatbrailleBig = () => (
    <div className="mt-4 mb-5 mx-5 text-center ">
        <img src={logo} alt="natbraille logo" style={{ width: '5em' }} />
        <p className="display-2">natbraille</p>
        <hr />
    </div>
)
