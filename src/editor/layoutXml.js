const createFragmentIdMap = document => {
    const map = new Map()
    const iterator = document.evaluate("//*[@fragmentId]", document, null, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null)
    try {
        let thisNode = iterator.iterateNext();
        while (thisNode) {
            map.set(thisNode.getAttribute('fragmentId'), thisNode)
            thisNode = iterator.iterateNext();
        }
    } catch (e) {
        console.error(`Error: Document tree modified during iteration ${e}`);
    }
    return map
}
const getElementPath = element => {
    const path = [element]
    while (element = element?.parentElement) {
        path.push(element)
    }
    return path
}
export const parseBrailleLayoutXml = transcriptionResult => {

    const nolayoutXmlString = transcriptionResult._3_trans
    const noLayoutDoc = (new DOMParser()).parseFromString(nolayoutXmlString, "application/xml");

    const layoutXmlString = transcriptionResult._5_table
    //const layoutXmlString = transcriptionResult._4_mep_xml
    const layoutDoc = (new DOMParser()).parseFromString(layoutXmlString, "application/xml");


    //console.log('noLayoutDoc', noLayoutDoc)
    //console.log('layoutDoc', layoutDoc)

    const body = layoutDoc.firstChild;

    const noLayoutFragmentIdMaps = createFragmentIdMap(noLayoutDoc)

    //console.log('map', noLayoutFragmentIdMaps)
    if (body?.children)
    for (const $block of body.children) {
        // block level p or h1
        for (const child of $block.children) {
            //             
            const refFragmentId = child.getAttribute('ref-fragmentId')
            const refInnerOffset = child.getAttribute('ref-inner-offset')

            const referedNode = noLayoutFragmentIdMaps.get(refFragmentId)
            if (referedNode !== undefined) {
                const referedNodePath = getElementPath(referedNode)
                const referedNodeFragmentIdPath = referedNodePath.map(e => e.getAttribute('fragmentId'))
                //   console.log('~~', child.tagName, '@', refFragmentId, refInnerOffset, '=>', referedNode, '[]', referedNodePath, referedNodeFragmentIdPath)//
            }
        }
    }
    //console.log('noLayoutFragmentIdMaps',noLayoutFragmentIdMaps)
    return {
        layoutXmlString,
        layoutDoc,
        noLayoutFragmentIdMaps
    }
    //console.log('noLayoutDoc',noLayoutDoc.getElementsByTagName('body'))
    //block-first-line-indentation

    //console.log('children',body)
}
export const removeDebugTextPadding = txt => txt
    // paragraphe mode padding
    .replaceAll('↝', ' ')
    .replaceAll('P', ' ')
    // padding for centering
    .replaceAll('C', ' ')
    // space
    .replaceAll('⎵', ' ')
    // padding to page number
    .replaceAll('⇉', ' ')
    .replaceAll('N', ' ')
