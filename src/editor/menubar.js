//
// Menu
//

import { setBlockType, wrapIn } from "prosemirror-commands"
import { Plugin } from "prosemirror-state"

/**
 * Helpers for toolbar/menus creation
 */
export function MenubarIcon(text, name) {
  return {
    create: () => {
      let $button = document.createElement("button")
      $button.classList.add("natbraille-editor-menuicon")
      $button.setAttribute('aria-label', name)
      $button.title = name
      $button.innerHTML = text
      return $button
    }
  }
}
export function MenuBarComboItem(value, caption) {
  return {
    create: () => {
      let option = document.createElement("option")
      option.setAttribute('value', value)
      option.textContent = caption
      return option
    }
  }
}
export function MenuBarCombo(name, items) {
  const create = () => {
    let $select = document.createElement("select")
    $select.className = "natbraille-editor-select " + name
    return $select;
  }
  return {
    type: 'menu',
    name: name,
    dom: { create },
    childItems: items
  }
}


const createMenubarSubbar = () => {
  const subbar = document.createElement("div")
  subbar.className = "natbraille-editor-menubar-subbar"
  return subbar
}

class MenuView {
  constructor(itemGroups, editorView) {

    this.editorView = editorView

    this.dom = document.createElement("div")
    this.dom.className = "natbraille-editor-menubar rounded-bottom-border"
    this.dom.id = "natbraille-editor-noir-menubar"
    this.commandButtons = []


    this.items = []


    itemGroups.forEach((itemGroup) => {

      // create a subbar (group of tool items)
      const $subbar = createMenubarSubbar()
      this.dom.appendChild($subbar)

      itemGroup.forEach(item => {

        if (item.type === 'menu') {
          // toolbar 'menu' (combo-box)          
          const $combo = item.dom.create()
          this.items.push({ ...item, dom: $combo })
          $combo.classList.add("toolbar-item")
          $subbar.appendChild($combo)
          item.childItems.forEach(menuItem => {
            // menu (combo box) items
            const item2 = { ...menuItem, dom: menuItem.dom.create() }
            $combo.appendChild(item2.dom)
            this.commandButtons.push(item2)
            this.items.push(item2)
          })
        } else {
          // toolbar item
          const item2 = { ...item, dom: item.dom.create() }
          $subbar.appendChild(item2.dom)
          this.commandButtons.push(item2)
          item2.dom.classList.add("toolbar-item")
          this.items.push(item2)

        }
      })
    })


    // burger menu.
    if (true) {

      const $subbar = createMenubarSubbar()
      $subbar.classList.add("dropdown")
      this.dom.appendChild($subbar)
      {
        const $burgerIcon = document.createElement('button')
        $burgerIcon.classList.add("bi", "bi-list", "toolbar-item", /*"nav-link",*/ /*"dropdown-toggle",*/
          "natbraille-editor-menuicon");
        //        $burgerIcon.classList.add("nav-link","dropdown-toggle")

        //$burgerIcon.setAttribute("id", "navbarDropdown")
        //$burgerIcon.setAttribute("role", "button")
        $burgerIcon.setAttribute("data-bs-toggle", "dropdown")
        //$burgerIcon.setAttribute("aria-haspopup", "true")
        //$burgerIcon.setAttribute("aria-expanded", "false")        
        $subbar.appendChild($burgerIcon)

        const $burgerMenu = document.createElement("div")
        $burgerMenu.classList.add("dropdown-menu")
        $subbar.appendChild($burgerMenu)
        {

          itemGroups.forEach((itemGroup) => {

            // create a subbar (group of tool items)
            const $subbar = createMenubarSubbar()
            $burgerMenu.appendChild($subbar)

            itemGroup.forEach(item => {
              if (item.type === 'menu') {
                const $combo = item.dom.create()
                this.items.push({ ...item, dom: $combo })
                $combo.classList.add("toolbar-item")
                $subbar.appendChild($combo)
                item.childItems.forEach(menuItem => {
                  const item2 = { ...menuItem, dom: menuItem.dom.create() }
                  $combo.appendChild(item2.dom)
                  this.commandButtons.push(item2)
                  this.items.push(item2)
                })
              } else {
                const item2 = { ...item, dom: item.dom.create() }
                $subbar.appendChild(item2.dom)
                this.commandButtons.push(item2)
                item2.dom.classList.add("toolbar-item")
                this.items.push(item2)

              }
            })
          })
        }
      }

    }

    this.update()

    this.dom.addEventListener("mousedown", e => {
      editorView.focus()
      this.commandButtons.forEach(({ command, dom }) => {
        if (command) {
          if (dom.contains(e.target)) {
            command(editorView.state, editorView.dispatch, editorView)
            //e.preventDefault()
          }
        }
      })
    })


    this.dom.addEventListener("change", (e) => {
      // for chrome select/option
      // *    In Chrome: The mousedown event does not fire on <option> elements. It only fires on the <select> element itself.
      // *   In Firefox: The mousedown event does fire on <option> elements inside a <select>. This can be useful for custom dropdown behaviors but also creates inconsistencies across browsers.
      if (e.target?.tagName?.toLowerCase() === 'select') {
        const $select = e.target
        this.commandButtons.forEach(({ command, dom }) => {
          if ($select.contains(dom) && ($select.value === dom.value)) {
            if (command) {
              command(editorView.state, editorView.dispatch, editorView);
            }
          }
        });
      }
    });

  }

  // TODO : each item should have an update method
  update() {
    {
      // BUTTONS
      // set active / inactive according to command result
      this.commandButtons.forEach(({ command, dom }) => {
        if (command) {
          let active = command(this.editorView.state, null, this.editorView)
          if (active) {
            dom.classList.remove('menuicon-inactive')
          } else {
            dom.classList.add('menuicon-inactive')
          }
        }
      })
    }
    {
      const selectionHead = this.editorView.state.selection.$head
      {
        // select paragraph type at cusrsor position
        const parent = selectionHead?.parent
        if (parent) {
          let value;
          if (parent.type.name === 'heading') {
            const { level } = parent.attrs
            value = `${parent.type.name}${level}`
          } else if (parent.type.name === 'paragraph') {
            value = `${parent.type.name}`
          }
          if (value) {
            ;[...(this.items)].filter(({ type }) => type === 'menu').forEach(
              menuSelect => {
                if (menuSelect.name === 'paragraph-style') {
                  menuSelect => menuSelect.dom.value = value
                }
              }
            )
          }
        }
      }
      {
        // select used grade mark at cusrsor position
        const marks = selectionHead.marks();
        const maybeGradeMark = marks.find(mark => mark.type === this.editorView.state.schema.marks.grade)
          ;[...(this.items)].filter(({ type }) => type === 'menu').forEach(
            menuSelect => {
              if (menuSelect.name === 'grade') {
                if (maybeGradeMark) {
                  menuSelect.dom.value = maybeGradeMark.attrs.grade
                } else {
                  menuSelect.dom.value = 'grade_none'
                }
              }
            }
          )

        /*
        marks.forEach(mark => {
          console.log(mark.type.name); // e.g., "strong" for bold, "em" for italic
          console.log(mark.attrs);     // e.g., { href: "https://example.com" } for a link
        })
        */

      }
    }
  }

  destroy() { this.dom.remove() }
}

export function MenuPlugin(itemGroups) {
  return new Plugin({
    view(editorView) {
      let menuView = new MenuView(itemGroups, editorView)
      editorView.dom.parentNode.insertBefore(menuView.dom, editorView.dom)
      return menuView
    }
  })
}

