import React, { useContext, useEffect, useState } from 'react';

import { OptionsContext } from '../context/optionsContext';
import { UI_OPTION_COLORED_BRAILLE_COLORS, getUIOption } from '../context/uiOptions';

const byDbgRefType = t => `character[dbg-ref-type="${t}"]`

const cssClassForElementType = {
    // color: var(--braille-text-color1);
    'letters': byDbgRefType('letters'),
    'digits': byDbgRefType('digits'),
    'punctuation': byDbgRefType('punctuation'),
    'list-bullet': 'list-bullet',
    'math': byDbgRefType('pre-typeset-math'),
    'math-prefix': byDbgRefType('math-prefix'),
    'uppercase-prefix': byDbgRefType('uppercase-prefix'),
}

const BrailleColorsStyle = () => {
    const { uiOptions } = useContext(OptionsContext)
    const [styleContent, setStyleContent] = useState('');
    const containingClass = 'tinted-braille'
    useEffect(() => {
        const generateStyleContent = () => {
            const colors = getUIOption(uiOptions)(UI_OPTION_COLORED_BRAILLE_COLORS)
            let style = '';
            for (const type in colors) {
                if (Object.hasOwnProperty.call(colors, type)) {
                    const { text, background } = colors[type];
                    const cssClass = cssClassForElementType[type]
                    const cssTextColor = (text === undefined) ? '' : (`color:${text};`)
                    const cssBackgroundColor = (background === undefined) ? '' : (`background-color:${background};`)
                    if (!((text === undefined) && (background === undefined))) {
                        if (cssClass === undefined) {
                            const msg = `/* no css selector for type '${type}'*/\n`
                            console.error(msg)
                            style += msg
                        } else {
                            style += `.${containingClass} ${cssClass} { ${cssTextColor} ${cssBackgroundColor} }\n`;
                        }
                    }
                }
            }
            return style;
        };
        setStyleContent(generateStyleContent());
    }, [uiOptions]);

    return (
        <style>
            {styleContent}
        </style>
    );
};

export default BrailleColorsStyle;