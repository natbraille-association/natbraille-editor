import { useState, useEffect } from 'react';

export function MyMenu({ editorView, itemGroups }) {
  console.log("I do the oli constructor", itemGroups)

  const [editorState, setEditorState] = useState(editorView.state);

  useEffect(() => {
    console.log("I do the oli useEffect", itemGroups)
    const onUpdate = () => setEditorState(editorView.state);

    // Register a transaction listener
    const updateListener = editorView.updateState;
    editorView.updateState = (state) => {
      updateListener.call(editorView, state);
      onUpdate();
    };

    return () => {
      editorView.updateState = updateListener; // Restore original function on unmount
    };
  }, [editorView]);



  return (
    <div className="menuz" style={{
      /*display: "block",
      zIndex:25,
      position:"absolute",*/
      x: 0, y: 0
    }}>
      {itemGroups.map((group, index) => {
        return (
          <div key={index} className="menu-group">
            {group.map((item,index) => (
              <button className="btn btn-primary"
                key={index}
                onClick={() => {
                  item.command(editorView.state, editorView.dispatch, editorView)
                  console.log('clicked', item)
                  //item.command(editorView)
                }}
                disabled={!item.command(editorView.state, null, editorView)}    >
                {item.label}
              </button>
            ))}
          </div>
        )
      })}
    </div>
  );
}

import { Plugin } from 'prosemirror-state';
//import { Plugin } from 'prosemirror-state';
import { createRoot } from 'react-dom/client';
//import Menu from './Menu';

export function MyMenuPlugin(itemGroups) {
  return new Plugin({
    view(editorView) {
      const container = document.createElement('div');
      editorView.dom.parentNode.insertBefore(container, editorView.dom);

      // Create React root
      const root = createRoot(container);

      function render() {
        root.render(<MyMenu editorView={editorView} itemGroups={itemGroups} />);
      }

      render();

      return {
        update: render,
        destroy() {
          root.unmount();
          container.remove();
        },
      };
    },
  });
}
