import { PluginKey } from "prosemirror-state";
/**
 * The React props used by ProseMirror instance state are stored under "reactProps" key by the PluginKey ProseMirror plugin 
 */
export const reactPropsKey = new PluginKey("reactProps");
