import { Schema, NodeSpec, MarkSpec, DOMOutputSpec } from "prosemirror-model"

const subItemSelectable = false



const type0 = [
    'math',
    //'semantics',
    //'annotation',
    'annotation-xml',
]

const type1 = [
    'semantics',
    'maction',
    'maligngroup',
    'malignmark',
    'menclose',
    'mfenced',
    'mfrac',
    'mlongdiv',
    'mmultiscripts',
    'mover',
    'mpadded',
    'mphantom',
    'mprescripts',
    'mroot',
    'mrow',
    'ms',
    'mscarries',
    'mscarry',
    'msgroup',
    'msline',
    'mspace',
    'msqrt',
    'msrow',
    'mstack',
    'mstyle',
    'msub',
    'msubsup',
    'msup',
    'munder',
    'munderover',
    'none',
    'mtable',
    'mtd',
    'mtr',
    'mlabeledtr',
    'annotation'
]

const type2 = [
    //'merror',
    //'mglyph',
    'mn',
    'mo',
    'mi',
    //'mtext',
]
const makeContentChoice = (names: String[]) => `(${names.join("|")})*`
const modelNsTag = (tag: String) => `http://www.w3.org/1998/Math/MathML ${tag}`
const modelTagName = (tag: String) => `m_${tag}`
/*
const makeType1ModelNode = (tag: String) => {
    const nodes = {}
    nodes[modelTagName(tag)] = {
        content: makeContentChoice(type1.map(modelTagName)),
        selectable: subItemSelectable,
        parseDOM: [{ tag }],
        toDOM: node => { return [modelNsTag(tag), {}, 0] }
    } as NodeSpec
    return nodes;
}
*/

//console.log('aaazzz', makeType1ModelNode("zob"));

// locked node:
// https://prosemirror.net/docs/ref/version/0.7.0.html#NodeType.locked

export const MathMLModelNodes = {
    math: {
        inline: true,
        inlineContent: true,
        group: "inline",
        selectable: false,
        // isolating : true,
        content: 'm_semantics',
        attrs: {
            //xmlns: { default: "http://www.w3.org/1998/Math/MathML" },
            ///source: { default: "" },
            //contenteditable: { default: "false" },
            display: { default: "inline" },
            fragmentid: { default: "" },
            chemistry : { default: "false"}
        },
        parseDOM: [{
            tag: "math",
            getAttrs: dom => {
                const innerHTML = dom.innerHTML;
                return {
                    //xmlns: "http://www.w3.org/1998/Math/MathML",
                    //source: dom.innerHTML
                    display: dom.getAttribute('display'),
                    fragmentid: dom.getAttribute('fragmentid'),
                    chemistry : dom.getAttribute('chemistry')
                }
            }
        }],
        toDOM: node => {
            return [
                "http://www.w3.org/1998/Math/MathML math",
                //"math",
                {
                    xmlns: "http://www.w3.org/1998/Math/MathML",
                    // source: node.attrs.source,
                    //  contenteditable: "false",
                    //display:"inline-block",
                    display: node.attrs.display,//"inline"
                    fragmentid: node.attrs.fragmentid,
                    chemistry: node.attrs.chemistry
                },
                0
            ]
        },
        // locked:true,
    } as NodeSpec,

    ...(Object.fromEntries(type1.filter(tag => !(['annotation'].includes(tag))).map((tag: String) => {
        return [
            modelTagName(tag),
            {
                content: makeContentChoice([...type1, ...type2].map(modelTagName)),
                selectable: subItemSelectable,
                parseDOM: [{ tag }],
                toDOM: node => { return [modelNsTag(tag), {}, 0] },
                // locked:true,
            }
        ]
    }))),
    /*
    m_semantics: {
        content: "(m_mrow|m_annotation)*",
        //  content:"m_annotation*",
        //content:"m_mrow*",
        inline: false,
        selectable: subItemSelectable,
        parseDOM: [{ tag: 'semantics' }],
        toDOM: node => { return ["http://www.w3.org/1998/Math/MathML semantics", {}, 0] }
    } as NodeSpec,

    m_mrow: {
        content: "(m_mrow|m_mi|m_mo|m_mn|m_sqrt|m_mfrac)*",
        //isBlock:true,
        //inline:false,
        selectable: subItemSelectable,
        parseDOM: [{ tag: 'mrow' }],
        toDOM: node => { return ["http://www.w3.org/1998/Math/MathML mrow", {}, 0] }
    } as NodeSpec,

    m_mfrac: { content: "(m_mrow|m_mi|m_mo|m_mn|m_sqrt)*", inline: false, selectable: subItemSelectable, parseDOM: [{ tag: 'mfrac' }], toDOM: node => { return ["http://www.w3.org/1998/Math/MathML mfrac", {}, 0] } } as NodeSpec,
    m_sqrt: {
        content: "(m_mrow)*",
        //isBlock:true,
        //inline:false,
        selectable: subItemSelectable,
        parseDOM: [{ tag: 'msqrt' }],
        toDOM: node => { return ["http://www.w3.org/1998/Math/MathML msqrt", {}, 0] }
    } as NodeSpec,



    */

    m_annotation: {
        attrs: {
            'encoding': { default: "StarMath 5.0" }
        },
        content: "text*", inline: false, selectable: subItemSelectable,
        parseDOM: [{
            tag: 'annotation',
            getAttrs: dom => {
                return {
                    encoding: dom.getAttribute("encoding")
                }
            }
        }],
        toDOM: node => {
            return [
                "http://www.w3.org/1998/Math/MathML annotation",
                {
                    encoding : node.attrs.encoding
                },
                0
            ]
        },
    } as NodeSpec,
    m_mi: { /* locked:true,*/ content: "text*", inline: false, selectable: subItemSelectable, parseDOM: [{ tag: 'mi' }], toDOM: node => { return ["http://www.w3.org/1998/Math/MathML mi", {}, 0] } } as NodeSpec,
    m_mo: { /* locked:true,*/ content: "text*", inline: false, selectable: subItemSelectable, parseDOM: [{ tag: 'mo' }], toDOM: node => { return ["http://www.w3.org/1998/Math/MathML mo", {}, 0] } } as NodeSpec,
    m_mn: { /* locked:true,*/ content: "text*", inline: false, selectable: subItemSelectable, parseDOM: [{ tag: 'mn' }], toDOM: node => { return ["http://www.w3.org/1998/Math/MathML mn", {}, 0] } } as NodeSpec,

}


console.log('mathml', MathMLModelNodes)
/*
<math>
-> <semantics>
   -> <annotation>
   -> <annotation-xml>
*/

/*
<maction>
<maligngroup>
<malignmark>
<math>
<menclose>
<merror>
<mfenced>
<mfenced>
<mfrac>
<mglyph>
<mi>
<mlabeledtr>
<mlongdiv>
<mmultiscripts>
<mn>
<mo>
<mover>
<mpadded>
<mphantom>
<mprescripts>
<mroot>
<mrow>
<ms>
<mscarries>
<mscarry>
<msgroup>
<msline>
<mspace>
<msqrt>
<msrow>
<mstack>
<mstyle>
<msub>
<msubsup>
<msup>
<mtable>
<mtd>
<mtext>
<mtr>
<munder>
<munderover>
<none>

*/

