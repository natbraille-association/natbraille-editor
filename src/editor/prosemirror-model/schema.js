//const SOURCE_SCHEMA = nonbrailleStructuredSchema

import { Schema } from "prosemirror-model"
import { schema as prosemirrorSchemaBasic } from "./prosemirror-schema-basic2";
import { addListNodes } from "prosemirror-schema-list";

// SOURCE_DOCUMENT_ROOT_TAGNAME = "doc"
export const SOURCE_SCHEMA = new Schema({
    // list handling
    // https://prosemirror.net/examples/basic/
    // https://github.com/ProseMirror/prosemirror-schema-list/tree/master/src
    // https://prosemirror.net/docs/ref/#schema-list
    nodes: addListNodes(prosemirrorSchemaBasic.spec.nodes, "paragraph block*", "block"),
    marks: prosemirrorSchemaBasic.spec.marks
})


export const SOURCE_DOCUMENT_ROOT_TAGNAME = "html"
