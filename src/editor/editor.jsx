window.DEBUG_VIEW_STRUCTURE = false

import { EditorState, Plugin, PluginKey } from "prosemirror-state";
import { EditorView } from "prosemirror-view";
import { useCallback, useContext, useEffect, useId, useRef, useState } from "react";
import './prosemirror-style.css'
import './prosemirror-document-style.scss'
import { parseBrailleLayoutXml } from "./layoutXml";
//import applyDevTools from 'prosemirror-dev-tools'
const applyDevTools = undefined
import { keymap } from "prosemirror-keymap"
import { baseKeymap, toggleMark } from "prosemirror-commands"
import { DOMParser } from "prosemirror-model";
import { getDomOuterString } from "./memo";
import { postBlackToBrailleTranscription } from "../serverio/transcription";



// import { InputRule, inputRules, textblockTypeInputRule, wrappingInputRule } from 'prosemirror-inputrules'
// import { findWrapping, canJoin } from "prosemirror-transform"

// const showInterval = (doc, from, to) => {
//   for (let i = from; i <= to; i++) {
//     const $i = doc.resolve(i)
//     const nodeType = $i.node($i.depth).type.name
//     //console.log(i, `node "${nodeType}": <${$i.node($i.depth).text}> :${$i.start($i.depth)}->${$i.end($i.depth)}`, $i)
//   }
// }

function reactProps(initialProps) {
  return new Plugin({
    key: reactPropsKey,
    state: {
      init: () => initialProps,
      apply: (tr, prev) => tr.getMeta(reactPropsKey) || prev,
    },
  });
}

import debounce from "debounce";
//import throttle from "throttleit";

let QUIRKY_PARSED = {
  current: undefined,
  brailleSelectedElement: undefined
}

const updateBraillePanel = (oldState, xmlString, filterOptions, parentFragmentId, parentOffset, isJustACursorMove) => {

  const { setBrailleContent, setBrailleContentMarkup, brailleContentMarkupRef } = reactPropsKey.getState(oldState);

  // console.log('I want to syncrhonize to', parentOffset)

  if (isJustACursorMove) {
    if (QUIRKY_PARSED.current) {
      removeBrailleSelectionMarks()
      updateBrailleCursorPosition(QUIRKY_PARSED.current);
    }

  } else {

    postBlackToBrailleTranscription(xmlString, filterOptions).then(transcriptionResult => {

      const parsed = parseBrailleLayoutXml(transcriptionResult)
      QUIRKY_PARSED.current = parsed;

      if (brailleContentMarkupRef?.current === null) {
        return
      }
      brailleContentMarkupRef.current.innerHTML = ''

      if (parsed?.layoutXmlString == undefined) {
        return
      }

      const innerHTML = parsed.layoutXmlString
        .replace('<?xml version="1.0" encoding="UTF-8"?>', '')
        .replace('<body>', '')
        .replace('</body>', '')
        .replaceAll(/<line-break .*?\/>/g, '<br>')
        .replaceAll(/<page-break .*?\/>/g, '<hr>')

      brailleContentMarkupRef.current.innerHTML = innerHTML
      updateBrailleCursorPosition(parsed);
    })
  }
  function removeBrailleSelectionMarks() {
    brailleContentMarkupRef.current.querySelectorAll("*[selected='true']").forEach(($selected, i) => {
      $selected.removeAttribute("selected")
    })


  }
  function updateBrailleCursorPosition(parsed) {
    const noLayoutMatchingNode = parsed.noLayoutFragmentIdMaps.get(parentFragmentId);
    const noirs = noLayoutMatchingNode?.querySelectorAll("noir")
    //            console.log('noirs', noirs)
    if (noirs) {
      let noirOffset = 0;
      let found = undefined
      for (let i = 0; i < noirs.length; i++) {
        /**
         * find child matching noir string offset
         */
        const noirText = noirs[i].textContent;
        const noirLength = noirText.length;
        if ((noirOffset + noirLength) > parentOffset) {
          found = {
            nodeNoir: noirs[i],
            offset: parentOffset - noirOffset
          }
          break;
        } else {
          noirOffset += noirLength
        }
      }                                    //          console.log('found', found)
      if (found) {
        //                    console.log("noir ->",found)
        const isSpacing = found.nodeNoir.parentNode.nodeName === "spacing"
        const isPunctuation = found.nodeNoir.parentNode.nodeName === "punctuation"

        const nodeBraille = found.nodeNoir.parentNode.querySelector('braille')
        //          console.log('found braille', nodeBraille)
        if (nodeBraille) {
          //                    console.log("braille ->",nodeBraille)
          const nodeBrailleFragmentId = nodeBraille.getAttribute('fragmentId')
          const childFragmentIdNodesIds = [...nodeBraille.querySelectorAll('[fragmentId]')].map(x => x.getAttribute('fragmentId'))
          const ids = [nodeBrailleFragmentId, ...childFragmentIdNodesIds]
          if (isSpacing || isPunctuation) {
            ids.push(found.nodeNoir.parentNode.getAttribute('fragmentId'))
          }
          //          console.log('yead ids', ids)

          let scrollTo = undefined;
          ids.forEach(nodeBrailleFragmentId => {
            //          console.log('* nodeBrailleFragmentId', nodeBrailleFragmentId)
            const brailleSelecteds = brailleContentMarkupRef.current.querySelectorAll("*[ref-fragmentid=" + nodeBrailleFragmentId + "]")
            //        console.log('->', brailleSelecteds)
            if (brailleSelecteds) {
              brailleSelecteds.forEach(brailleSelected => {
                brailleSelected.setAttribute('selected', true)
                brailleSelected.setAttribute('tabindex', -1)

                scrollTo = brailleSelected;
                QUIRKY_PARSED.brailleSelectedElement = scrollTo
                //console.log('TYEA?', brailleSelected)
              })
            }
          })
          scrollTo?.scrollIntoView({ /*behavior: "smooth", */block: "center", inline: "nearest" })

          // -> focus only on button scrollTo?.focus()
        }
      } else {
        //console.log(noLayoutMatchingNode)
      }
    }
  }
}


const debouncedUpdateBraillePanel = debounce(updateBraillePanel, 200)
//const throttledUpdateBraillePanel = throttle(updateBraillePanel, 500)



function postDocumentPlugin(filterOptions) {
  // TODO : appendTransaction ? transaction.docChanged ?
  return new Plugin({
    state: {
      init: () => undefined,
      apply: (tr, prev, oldState, newState) => {

        const isJustACursorMove = (tr.selectionSet && !tr.docChanged)

        //   console.log('isACursorMove', isJustACursorMove)

        // todo restore log
        // console.log('doc changed ?', tr.docChanged)

        //console.log('Do a transcription at', Date.now())
        //console.log(tr.doc.toString())
        const xmlString = getDomOuterString(tr.doc, SOURCE_SCHEMA, SOURCE_DOCUMENT_ROOT_TAGNAME)
        //console.log('post xmlString', xmlString)

        //        const postTranscription = () => {
        const $head = newState.selection.$head
        const parentTypeName = $head?.parent?.type?.name
        const parentFragmentId = $head?.parent?.attrs?.fragmentid
        const parentOffset = $head.parentOffset

        const updateFunction = updateBraillePanel
        // const updateFunction = debouncedUpdateBraillePanel
        //const updateFunction = throttledUpdateBraillePanel

        if (!isJustACursorMove) {
          const updateFunction = debouncedUpdateBraillePanel
          updateFunction(oldState, xmlString, filterOptions, parentFragmentId, parentOffset, isJustACursorMove)
        } else {
          const updateFunction = updateBraillePanel
          updateFunction(oldState, xmlString, filterOptions, parentFragmentId, parentOffset, isJustACursorMove)
        }
        return tr;
      }
    }
  })
}
function addFragmentId() {

  // lets consider id will never be greater than Number.MAX_SAFE_INTEGER =  (2^53 -1).
  let currentId = 1
  const idAttributeName = "fragmentid"
  const getNewId = () => {
    currentId++
    return "s" + currentId
  }
  return new Plugin({
    // https://discuss.prosemirror.net/t/how-i-can-attach-attribute-with-dynamic-value-when-new-paragraph-is-inserted/751/3
    appendTransaction: (transactions, prevState, nextState) => {


      const usedIds = new Set()

      const tr = nextState.tr;
      let modified = false;

      if (transactions.some((transaction) => transaction.docChanged)) {
        nextState.doc.descendants((node, pos) => {
          const nodeTypeSupportsFragmentId = node.type.attrs?.[idAttributeName]
          if (nodeTypeSupportsFragmentId) {
            // Adds a unique id to a node
            const currentNodeId = node.attrs?.[idAttributeName]
            let isCurrentNodeIdValid = currentNodeId && (!usedIds.has(currentNodeId))
            if (isCurrentNodeIdValid) {
              // current id exists and is valid, noop
              usedIds.add(currentNodeId)
            } else {
              // no current id or is a duplicate, create new id
              const attrs = node.attrs;
              tr.setNodeMarkup(pos, undefined, { ...attrs, [idAttributeName]: getNewId() });
              usedIds.add(currentId)
              modified = true;
            }
          }
        });
      }
      return modified ? tr : null;
    },
  });
}

function watchCursor() {
  const findFirstParentHavingFragmentId = $node => {
    for (let i = ($node.length - 1); i >= 0; i--) {
      if ($node[i]?.type?.attrs?.fragmentid) {
        return $node[i]
      }
    }
  }

  return new Plugin({
    view(view) {
      return {
        update: function (view, prevState) {
          // react props
          const state = view.state;
          const props = reactPropsKey.getState(state)

          // console.log('$anchor', view.state.selection.$anchor)
          //console.log('$head', view.state.selection.$head)
          const $head = view.state.selection.$head
          if ($head) {
            // fragmentId Path to String
            //console.log('OOOO', '=============', $head)
            const selectionPathFragmentIdString = $head.path.map(p => {
              //console.log('OOOO',p)
              if (p instanceof Object) {
                return `@${p?.attrs?.fragmentid}`
              } else {
                return p
              }
            }).filter(x => x).join(",") + "::" + $head.parentOffset
            //            console.log('selectionPathFragmentIdString',selectionPathFragmentIdString)
            props.setBrailleCursorPosition2(selectionPathFragmentIdString)
          }


          const parentWithId = findFirstParentHavingFragmentId($head.path)
          if (parentWithId !== undefined) {
            const fragmentid = parentWithId.attrs.fragmentid
            //console.log('fragmentid', fragmentid)

            //console.log('ABC', state)

            //console.log('ABC',)//(reactPropsKey))

            props.setBrailleCursorPosition(parentWithId.attrs.fragmentid)

            const dom = props.brailleContentMarkupRef.current
            //console.log('domdom', dom)

            dom.querySelectorAll("*[fragmentid]").forEach(e => {
              if (e.getAttribute('fragmentid') === fragmentid) {
                e.setAttribute("POPOPF", "YEAD")
                //e.classList.add('YEAH')
              }
            })

          } else {
            console.error('has no parent with id')
          }

          /*  
            if (prevState && prevState.doc.eq(state.doc) &&
                prevState.selection.eq(state.selection))
                return;
*/
          // selection changed
        }
      }
    }
  });
}


import { placeholderPlugin, InsertMathCommand, findFirstParentNodeAtResolvedPosNamed } from './mathCommands'


// menu

import { setBlockType } from "prosemirror-commands"
import { MenuPlugin, MenubarIcon, MenuBarCombo, MenuBarComboItem } from "./menubar";

const MENU_BAR_ICON = {
  //"new-document": '<i class="bi bi-file-text"></i>',
  "save-document": '<i class="bi bi-hdd"></i>',

  //"delete-all": '<i class="bi bi-trash"></i>',
  "em": '<i class="bi bi-type-italic"></i>',
  "h1": '<i class="bi bi-type-h1"></i>',
  "h2": '<i class="bi bi-type-h2"></i>',
  "h3": '<i class="bi bi-type-h3"></i>',
  "h4": '<i class="bi bi-type-h4"></i>',
  "h5": '<i class="bi bi-type-h5"></i>',
  "h6": '<i class="bi bi-type-h6"></i>',
  "liftUl": '<i class="bi bi bi-text-indent-right"></i>',
  "p": '<i class="bi bi-paragraph"></i>',
  "redo": '<i class="bi bi-arrow-clockwise"></i>',
  "export-odt": '<i class="bi bi-download">odt</i>',
  "export-xml": '<i class="bi bi-download">xml</i>',
  "sinkUl": '<i class="bi bi bi-text-indent-left"></i>',
  "ul": '<i class="bi bi-list-ul"></i>',
  "ol": '<i class="bi bi-list-ol"></i>',
  "undo": '<i class="bi bi-arrow-counterclockwise"></i>',
  "math": '<i class="bi bi-infinity"></i>',
  "focus-on-braille": '<i class="bi bi-arrow-right-square"></i>',
  "bold": '<i class="bi bi-type-bold"></i>',
  "g0": "<i>g0</i>",
  "g1": "<i>g1</i>",
  "g2": "<i>g2</i>",
  "grade_none":"aucun",
}
const MENU_BAR_ITEM_LABEL = {
  //"new-document": 'nouveau document',
  "save-document": "enregistrer",

  "em": "mise en évidence",
  "h1": "titre 1",
  "h2": "titre 2",
  "h3": "titre 3",
  "h4": "titre 4",
  "h5": "titre 5",
  "h6": "titre 6",
  "braille_block": "bloc braille",
  "liftUl": "déliste",
  "p": "paragraphe",
  "redo": 'rétablir',
  "export-odt": "enregistrer localement au format odt (OpenDocument Text)",
  "export-xml": "enregistrer le xml localement",
  "sinkUl": "surliste",
  "ul": "liste à puces",
  "ol": 'liste ordonnée',
  "undo": 'annuler',
  "math": 'insérer une formule mathématique',
  "focus-on-braille": "mettre le focus sur le panneau Braille",
  "bold": 'emphase',
  "g0": "G0",
  "g1": "G1",
  "g2": "G2",
  "grade_none":"aucun",
}

export function isMarkActive(state, markType, attrs = null) {
  const { from, to, empty } = state.selection;

  if (empty) {
    // For a cursor (empty selection), check marks at the cursor position
    const marks = state.selection.$from.marks();
    return marks.some(function (mark) {
      return mark.type === markType && (!attrs || mark.eq(markType.create(attrs)));
    });
  } else {
    // For a range selection, check if the mark exists anywhere in the range
    let found = false;
    state.doc.nodesBetween(from, to, function (node) {
      if (found) return false; // Stop searching if already found
      if (node.marks) {
        found = node.marks.some(function (mark) {
          return mark.type === markType && (!attrs || mark.eq(markType.create(attrs)));
        });
      }
      return !found; // Continue searching if not found
    });
    return found;
  }
}


function toggleOrReplaceGradeMark(grade) {
  return (state, dispatch) => {
    const markType = state.schema.marks.grade;

    // Check if the command is active (i.e., the mark is already applied with the same grade)
    const isActive = isMarkActive(state, markType, { grade });

   // console.log('isActive', isActive, grade)

    // If no dispatch function is provided, return whether the command is active
    if (!dispatch) return isActive;

    const { from, to } = state.selection;

    // Create a transaction
    const tr = state.tr;

    if (isActive) {
      // If the same grade mark is already present, remove it
      tr.removeMark(from, to, markType);
    } else {
      // Otherwise, replace any existing grade mark with the new one
      tr.removeMark(from, to, markType); // Remove existing grade marks
      tr.addMark(from, to, markType.create({ grade })); // Add the new grade mark
    }

    // Dispatch the transaction
    dispatch(tr);
    return true;
  };
}
function replaceGradeMark(grade) {
  return (state, dispatch) => {
    const markType = state.schema.marks.grade;

    const isActive = isMarkActive(state, markType, { grade });

    // If no dispatch function is provided, return whether the command is applicable
    if (!dispatch) return isActive;

    const { from, to } = state.selection;

    // Create a transaction
    const tr = state.tr;

    // Remove any existing grade marks in the selection range
    tr.removeMark(from, to, markType);

    // Add the new grade mark
    tr.addMark(from, to, markType.create({ grade }));

    // Dispatch the transaction
    dispatch(tr);
    return true;
  };
}
function findMarkRange($pos, mark) {
  const parent = $pos.parent;
  const currentIndex = $pos.index();
  let startIndex = currentIndex;
  let endIndex = currentIndex;

  // Find the start of the range (check previous siblings)
  while (startIndex > 0) {
    const prevNode = parent.child(startIndex - 1);
    if (!prevNode.marks.some(m => m.eq(mark))) break;
    startIndex--;
  }

  // Find the end of the range (check next siblings)
  while (endIndex < parent.childCount - 1) {
    const nextNode = parent.child(endIndex + 1);
    if (!nextNode.marks.some(m => m.eq(mark))) break;
    endIndex++;
  }

  // Calculate the actual start/end positions in the document
  let startPos = $pos.start(); // Absolute position of the start of the parent node
  for (let i = 0; i < startIndex; i++) {
    startPos += parent.child(i).nodeSize;
  }

  let endPos = startPos;
  for (let i = startIndex; i <= endIndex; i++) {
    endPos += parent.child(i).nodeSize;
  }

  return { start: startPos, end: endPos };
}

function removeGradeMark() {
  // remove the whole grade mark (of same grade) if selection is collapsed
  // remove on the selection if not collapsed.
  return (state, dispatch) => {
    const markType = state.schema.marks.grade;
    if (!markType) return false;

    const { from, to } = state.selection;

    // Check if the mark exists anywhere in the selection range
    let hasMark = false;
    state.doc.nodesBetween(from, to, (node) => {
      if (markType.isInSet(node.marks)) {
        hasMark = true;
        return false; // Stop iterating
      }
      return true;
    });

    if (!hasMark) return false; // No mark in the selection range

    if (dispatch) {
      const tr = state.tr;

      if (from === to) {
        // Collapsed selection: find the full range of the mark
        const $pos = state.doc.resolve(from);
        const mark = markType.isInSet($pos.marks());
        if (mark) {
          const range = findMarkRange($pos, mark);
          tr.removeMark(range.start, range.end, markType);
        }
      } else {
        // Non-collapsed selection: remove the mark over the selected range
        tr.removeMark(from, to, markType);
      }

      dispatch(tr);
    }

    return true;
  };
}
/**
 * Structured black document menu
 */
let menu = MenuPlugin([
  [
    //   { command: NewDocumentCommand(), dom: MenubarIcon(MENU_BAR_ICON["new-document"], MENU_BAR_ITEM_LABEL["new-document"]) },
    { command: SaveCommand(), dom: MenubarIcon(MENU_BAR_ICON['save-document'], MENU_BAR_ITEM_LABEL['save-document']) },
  ],
  [
    // { command: toggleMark(SOURCE_SCHEMA.marks.em), dom: MenubarIcon(MENU_BAR_ICON["em"], MENU_BAR_ITEM_LABEL["em"]) },
    { command: undo, dom: MenubarIcon(MENU_BAR_ICON["undo"], MENU_BAR_ITEM_LABEL["undo"]) },
    { command: redo, dom: MenubarIcon(MENU_BAR_ICON["redo"], MENU_BAR_ITEM_LABEL["redo"]) },
    //{ dom: MenubarSeparator() },
  ], /*[

    { command: toggleOrReplaceGradeMark("g0"), dom: MenubarIcon(MENU_BAR_ICON["g0"], MENU_BAR_ITEM_LABEL["g0"]) },
    { command: toggleOrReplaceGradeMark("g1"), dom: MenubarIcon(MENU_BAR_ICON["g1"], MENU_BAR_ITEM_LABEL["g1"]) },
    { command: toggleOrReplaceGradeMark("g2"), dom: MenubarIcon(MENU_BAR_ICON["g2"], MENU_BAR_ITEM_LABEL["g2"]) },
  ], */[
    { command: toggleMark(SOURCE_SCHEMA.marks.strong), dom: MenubarIcon(MENU_BAR_ICON["bold"], MENU_BAR_ITEM_LABEL["bold"]) },
  ], [
    MenuBarCombo('paragraph-style', [
      { command: setBlockType(SOURCE_SCHEMA.nodes.paragraph), dom: MenuBarComboItem('paragraph', MENU_BAR_ITEM_LABEL['p']) },
      { command: setBlockType(SOURCE_SCHEMA.nodes.heading, { level: 1 }), dom: MenuBarComboItem('heading1', MENU_BAR_ITEM_LABEL['h1']) },
      { command: setBlockType(SOURCE_SCHEMA.nodes.heading, { level: 2 }), dom: MenuBarComboItem('heading2', MENU_BAR_ITEM_LABEL['h2']) },
      { command: setBlockType(SOURCE_SCHEMA.nodes.heading, { level: 3 }), dom: MenuBarComboItem('heading3', MENU_BAR_ITEM_LABEL['h3']) },
      { command: setBlockType(SOURCE_SCHEMA.nodes.heading, { level: 4 }), dom: MenuBarComboItem('heading4', MENU_BAR_ITEM_LABEL['h4']) },
      { command: setBlockType(SOURCE_SCHEMA.nodes.heading, { level: 5 }), dom: MenuBarComboItem('heading5', MENU_BAR_ITEM_LABEL['h5']) },
      { command: setBlockType(SOURCE_SCHEMA.nodes.heading, { level: 6 }), dom: MenuBarComboItem('heading6', MENU_BAR_ITEM_LABEL['h6']) },
      { command: setBlockType(SOURCE_SCHEMA.nodes.braille_block), dom: MenuBarComboItem('bloc braille', MENU_BAR_ITEM_LABEL['braille_block']) },
    ]),
  ], [
    { command: wrapInList(SOURCE_SCHEMA.nodes.ordered_list), dom: MenubarIcon(MENU_BAR_ICON["ol"], MENU_BAR_ITEM_LABEL["ol"]) },
    { command: wrapInList(SOURCE_SCHEMA.nodes.bullet_list), dom: MenubarIcon(MENU_BAR_ICON["ul"], MENU_BAR_ITEM_LABEL["ul"]) },
    { command: liftListItem(SOURCE_SCHEMA.nodes.list_item), dom: MenubarIcon(MENU_BAR_ICON["liftUl"], MENU_BAR_ITEM_LABEL["liftUl"]) },
    { command: sinkListItem(SOURCE_SCHEMA.nodes.list_item), dom: MenubarIcon(MENU_BAR_ICON["sinkUl"], MENU_BAR_ITEM_LABEL["sinkUl"]) },
  ],
  [
    { command: InsertMathCommand(), dom: MenubarIcon(MENU_BAR_ICON["math"], MENU_BAR_ITEM_LABEL["math"]) },
  ],
  [
    { command: FocusOnBraillePanel(QUIRKY_PARSED), dom: MenubarIcon(MENU_BAR_ICON["focus-on-braille"], MENU_BAR_ITEM_LABEL["focus-on-braille"]) },
  ],

  //{ dom: MenubarSeparator() },
  // { command: setBlockType(SOURCE_SCHEMA.nodes.paragraph), dom: MenubarIcon(MENU_BAR_ICON["p"], MENU_BAR_ITEM_LABEL["p"]) },
  // MenuBarHeading(SOURCE_SCHEMA.nodes.heading, 1, MENU_BAR_ICON['h1'], MENU_BAR_ITEM_LABEL['h1']),
  // MenuBarHeading(SOURCE_SCHEMA.nodes.heading, 2, MENU_BAR_ICON['h2'], MENU_BAR_ITEM_LABEL['h2']),
  // MenuBarHeading(SOURCE_SCHEMA.nodes.heading, 3, MENU_BAR_ICON['h3'], MENU_BAR_ITEM_LABEL['h3']),

  //{ dom: MenubarSeparator() },
  [
    { command: ExportXMLCommand(), dom: MenubarIcon(MENU_BAR_ICON['export-xml'], MENU_BAR_ITEM_LABEL['export-xml']) },
    { command: OdtExportCommand(), dom: MenubarIcon(MENU_BAR_ICON['export-odt'], MENU_BAR_ITEM_LABEL['export-odt']) }
  ],
  /*
  { command: toggleOrReplaceGradeMark("g0"), dom: MenubarIcon(MENU_BAR_ICON["g0"], MENU_BAR_ITEM_LABEL["g0"]) },
    { command: toggleOrReplaceGradeMark("g1"), dom: MenubarIcon(MENU_BAR_ICON["g1"], MENU_BAR_ITEM_LABEL["g1"]) },
    { command: toggleOrReplaceGradeMark("g2"), dom: MenubarIcon(MENU_BAR_ICON["g2"], MENU_BAR_ITEM_LABEL["g2"]) },
  */
  [
    MenuBarCombo('grade', [
    { command: removeGradeMark(), dom: MenuBarComboItem("grade_none", MENU_BAR_ITEM_LABEL["grade_none"]) },
    { command: replaceGradeMark("g0"), dom: MenuBarComboItem("g0", MENU_BAR_ITEM_LABEL["g0"]) },
    { command: replaceGradeMark("g1"), dom: MenuBarComboItem("g1", MENU_BAR_ITEM_LABEL["g1"]) },
    { command: replaceGradeMark("g2"), dom: MenuBarComboItem("g2", MENU_BAR_ITEM_LABEL["g2"]) },
    ])
  ],
  
  [
    //{ command: }
    //{ command: wrapIn(SOURCE_SCHEMA.nodes.blockquote), dom: icon(">", "blockquote") }
  ]
])

import { getNatbrailleDocumentFromLocalStorage, loadEditorProgressFromLocalStorage, saveEditorProgressToLocalStorage } from "../localstorage/store";
function localStorageExportPlugin() {
  return new Plugin({
    state: {
      init: () => undefined,
      apply: (tr, prev, oldState, newState) => {
        // todo restore log
        // console.log(tr)
        //console.log('Save to localstorage', Date.now())
        //console.log(newState.toJSON())
        saveEditorProgressToLocalStorage(newState)
        //console.log(tr.doc.toString())
        //const xmlString = getDomOuterString(tr.doc, SOURCE_SCHEMA, SOURCE_DOCUMENT_ROOT_TAGNAME)
      }
    }
  })
}

function displayTransactionPlugin() {
  return new Plugin({
    state: {
      init: () => undefined,
      apply: (tr, prev, oldState, newState) => {
        const { setLastTransactionTimestamp } = reactPropsKey.getState(newState)
        //
        // TODO : find why this is responsible for 20/25 react redraws at each change.
        //
        // setLastTransactionTimestamp(printDate(new Date(tr.time)))

      }
    }
  })
}

import { undo, redo, history } from "prosemirror-history"
import { createDefaultStartDocument } from "./defaultStartDocument";
import { liftListItem, sinkListItem, wrapInList } from "prosemirror-schema-list";
import { OptionsContext } from "../context/optionsContext";
import { savePlainText } from "../localio/save";

import { useNavigate, useSearchParams } from "react-router-dom";
import { editorGetFileSearchParam, navigateToReverseEditor } from "../clientRoutes";
import { reactPropsKey } from "./reactPropKey";
import { ExportXMLCommand, FocusOnBraillePanel, OdtExportCommand, SaveCommand } from "./commands";
import { SOURCE_DOCUMENT_ROOT_TAGNAME, SOURCE_SCHEMA } from "./prosemirror-model/schema";
import { printDate } from "./date";
import { UI_OPTION_BRAILLE_FONT_SIZE, UI_OPTION_COLORED_BRAILLE_OUTPUT, UI_OPTION_PERKINS_KEYS, getUIOption, setUIOption } from "../context/uiOptions";

const preventMathEdit = () => {
  return new Plugin({
    filterTransaction: (tr, editorState) => {
      const $head = tr.selection.$head
      const maybeMath = findFirstParentNodeAtResolvedPosNamed($head, SOURCE_SCHEMA.nodes.math.name)
      if (maybeMath) {
        if (tr.steps.every(step => step instanceof Selection)) {
          return true; // Only cursor move
        } else {
          return false
        }
      }
      return true
    }
  });
};

/*************************** */

// import { Plugin } from "prosemirror-state";


//export default perkinsInputPlugin;


/******************************** */


export function Editor(props) {

  const viewHost = useRef();
  const view = useRef(null);
  const { options } = useContext(OptionsContext) // does not need to be reactive as editor will be reloaded when option page closes
  const [searchParams/*, setSearchParams*/] = useSearchParams();

  useEffect(() => {
    console.log('Editor', 'initial render')
    // initial render
    const schema = SOURCE_SCHEMA
    const plugins = [
      /*
      new MyMenuPlugin([
        [
          { name: "one", label: "H1", command: setBlockType(SOURCE_SCHEMA.nodes.heading, { level: 1 }) },
          { name: "two", label: "B", command: toggleMark(SOURCE_SCHEMA.marks.strong)},
          { command: wrapInList(SOURCE_SCHEMA.nodes.ordered_list), icon: MENU_BAR_ICON["ol"],     label : MENU_BAR_ITEM_LABEL["ol"] },
          { command: wrapInList(SOURCE_SCHEMA.nodes.bullet_list),  icon: MENU_BAR_ICON["ul"],     label : MENU_BAR_ITEM_LABEL["ul"] },
          { command: liftListItem(SOURCE_SCHEMA.nodes.list_item),  icon: MENU_BAR_ICON["liftUl"], label : MENU_BAR_ITEM_LABEL["liftUl"] },
          { command: sinkListItem(SOURCE_SCHEMA.nodes.list_item),  icon: MENU_BAR_ICON["sinkUl"], label : MENU_BAR_ITEM_LABEL["sinkUl"] },

        ]
      ]),
      */
      menu,
      reactProps(props),
      perkinsInputPlugin,
      history(),
      keymap({
        ...baseKeymap,
        "Ctrl-b": toggleMark(schema.marks.strong),
        "Mod-z": undo,
        "Mod-y": redo,
        //"Ctrl-e": wrapInList(schema.nodes.bullet_list)
        "Ctrl-e": liftListItem(SOURCE_SCHEMA.nodes.list_item),
        "Ctrl-s": SaveCommand(),
        "Ctrl-o": FocusOnBraillePanel(QUIRKY_PARSED)
      }),
      preventMathEdit(),
      placeholderPlugin,
      // inputRulePlugin,
      addFragmentId(),
      //watchCursor(),
      displayTransactionPlugin(),
      postDocumentPlugin(options),
      localStorageExportPlugin(),
    ]

    // load initial state
    let state;
    if (true) {
      const localStorageDocument = loadEditorProgressFromLocalStorage()
      if (localStorageDocument !== undefined) {
        // restore document from document storage
        // this is the full editor state (todo restore undo/redo history)
        // in prosemirror format
        state = EditorState.fromJSON({ schema, plugins }, localStorageDocument)
      } else {
        // if no prosemirror document is found in local storage
        // restore document from local storage via the document file name
        // when a document in loaded/imported, it is put in the localstorage
        // as an xml file
        const filenameInUrl = editorGetFileSearchParam(searchParams)
        console.log(`restore filename in url is this : '${filenameInUrl}'`)
        if (filenameInUrl) {
          // read from local storage

          const documentBuffer = getNatbrailleDocumentFromLocalStorage(filenameInUrl)
          const domParser = new window.DOMParser()
          const $startDocWhole = domParser.parseFromString(documentBuffer, "application/xml")
          const $startDoc = $startDocWhole//.getElementsByTagName("body");
          const startDoc = DOMParser.fromSchema(schema).parse($startDoc)
          state = EditorState.create({ doc: startDoc, schema, plugins });
        } else {
          // if no url is provided, we present use a default document
          // but we should not be here
          // create new from default start doc
          console.warn("should not be here")
          const $startDoc = createDefaultStartDocument()
          const startDoc = DOMParser.fromSchema(schema).parse($startDoc)
          state = EditorState.create({ doc: startDoc, schema, plugins });
        }
      }
    }
    //state = EditorState.create({ schema, plugins });
    view.current = new EditorView(viewHost.current.parentNode, { state });

    if (applyDevTools)
      applyDevTools(view.current)

    return () => view.current?.destroy();
  }, []);


  useEffect(() => {
    console.log('Editor', 'every render')
    // every render
    const tr = view.current.state.tr.setMeta(reactPropsKey, props);
    /*
    if (window.DEBUG_VIEW_STRUCTURE) {
      view.current.dom.classList.add('debug-view-structure')
    } else {
      view.current.dom.classList.remove('debug-view-structure')
    }
    */
    view.current.dispatch(tr);
  });

  return <div ref={viewHost} />

}

const saveBrailleQuirky = (refCurrent) => {
  console.log('saveBrailleQuirky', refCurrent)
  let child = refCurrent.firstChild;
  const plainText = []
  //while (child !== null) {
  plainText.push(refCurrent.innerText)
  console.log("-->", refCurrent.innerText)
  // plainText.push( refCurrent.innerText )
  /*
  const tagName = child.tagName;
  const textContent = child.textContent;
  //console.log(tagName, `'${textContent}'`);
  console.log(tagName)
  if (tagName === 'BR') {
    plainText.push("\n");
  } if (tagName.toLowerCase() === 'hr') {
    console.log("hr!!!!!", tagName, `'${textContent}'`);
    plainText.push("\f");
  } else {
    plainText.push(textContent);
  }
  child = child.nextElementSibling;*/
  //}
  savePlainText(plainText.join(''), "braille.brl")
}

export const NewDocumentBox = () => {
  return (<>
    {/* Button trigger modal */}
    <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
      Launch static backdrop modal
    </button>

    {/* Modal */}
    <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="staticBackdropLabel">Nom du document</h1>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div className="modal-body">
            <input type="text" style={{ "width": "100%" }}></input>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Annuler</button>
            <button type="button" className="btn btn-primary">Ok</button>
          </div>
        </div>
      </div>
    </div>
  </>
  )


}

//import pThrottle from 'p-throttle';
//import pDebounce from "p-debounce";


/*
const throttledStarMath5ToMathElement = pThrottle({
  limit: 1,
  interval: 500
})(starMath5ToMathElement)
*/
//const debouncedStarMath5ToMathElement = pDebounce(starMath5ToMathElement, 300)




import { MathEditor } from './MathEditor'
import { getNatFilterOptionValue } from "../context/natfilterOptions";
import BrailleColorsStyle from "./BrailleColorStyle";
import { DraggableDiv, getSplitViewColumnsCss } from "./ResizeHandle";
import { HamburgerDropDown } from "./HamburgerDropDown";
import { perkinsInputPlugin } from "./perkinsInputPlugin";
import { MyMenuPlugin } from "./menubar-REACT";
import { useEditedFileContext } from "../context/EditedFileContext";
// import { MyMenuPlugin } from "./menubar-REACT";
/**
 * Split-Editor component
 */

export const EditorSplit = () => {
  useEffect(() => {
    console.log('EditorSplit', 'initial render')
  }, [])
  useEffect(() => {
    console.log('EditorSplit', 'every render')
  })



  const navigate = useNavigate()

  const { options, setBrailleSource, uiOptions, setUIOptions } = useContext(OptionsContext) // does not need to be reactive as editor will be reloaded when option page closes
  const editedFileContext = useEditedFileContext()

  const [brailleContent, setBrailleContent] = useState('brl')
  /* const [brailleContentMarkup, setBrailleContentMarkup] = useState({__html : '<tague>gogo</tague>'})*/
  const [brailleCursorPosition, setBrailleCursorPosition] = useState('')
  const [brailleCursorPosition2, setBrailleCursorPosition2] = useState('')

  const [lastSaved, setLastSaved] = useState('-')
  const [lastTransactionTimestamp, setLastTransactionTimestamp] = useState(0)

  const [searchParams/*, setSearchParams*/] = useSearchParams();
  // document.title = editorGetFileSearchParam(searchParams) || 'Natbraille'

  const brailleContentMarkupRef = useRef(null)

  const buttonSaveBrailleClick = () => {
    saveBrailleQuirky(brailleContentMarkupRef.current)
  }

  const fontFamily = (getNatFilterOptionValue(options)('FORMAT_OUTPUT_BRAILLE_TABLE') === "brailleUTF8") ? ({ fontFamily: "LouisLouis" }) : ({});

  const fontSizeChanged = e => setUIOption(setUIOptions, uiOptions)(UI_OPTION_BRAILLE_FONT_SIZE, e.target.value)
  const tintedChanged = e => setUIOption(setUIOptions, uiOptions)(UI_OPTION_COLORED_BRAILLE_OUTPUT, e.target.checked)

  // const tintedLabelRef = useRef()

  // math
  const [currentStarMathString, setCurrentStarMathString] = useState('%alpha = %beta')
  const [currentLatexString, setCurrentLatexString] = useState('\\sqrt{-1}')

  const [currentIsChemistry, setCurrentIsChemistry] = useState(false)
  const mathEditorModalOpenButtonRef = useRef()
  const [mathEditorFinishedPromiseAccept, setMathEditorFinishedPromiseAccept] = useState();
  const [mathEditorFinishedPromiseReject, setMathEditorFinishedPromiseReject] = useState();
  const openMathEditorModal = (starMath5, latex, isChemistry, accept, reject) => {
    console.log('openMathEditorModal - starMath5,accept,reject', { starMath5, latex, isChemistry, accept, reject })
    if (accept) {
      setMathEditorFinishedPromiseAccept(accept)
    }
    if (reject) {
      setMathEditorFinishedPromiseReject(reject)
    }

    setCurrentIsChemistry(isChemistry)
    setCurrentStarMathString(starMath5)
    setCurrentLatexString(latex)


    mathEditorModalOpenButtonRef.current.click()
  }
  const finishEditing = () => {
    console.log('finish editing')
    setMathEditorFinishedPromiseAccept(() => { })
    setMathEditorFinishedPromiseReject(() => { })
    setCurrentStarMathString("x")
  }

  // navigation to detranscription
  const setDetranscriptionSourceThenNavigate = () => {
    setBrailleSource(brailleContentMarkupRef?.current?.innerText || "")
    navigateToReverseEditor(navigate)
  }

  const brailleToolBarLabels = {
    "save-braille": "enregistrer le braille",
    "braille-font-size": "taille de la police braille",
    "color-braille": "braille en couleur",
    "set-detranscription-braille": "détranscrire"
  }


  //const gridTemplateColumnsDefault = `calc(50% - ${editorColumnGap} / 2) ${editorColumnGap} calc(50% - ${editorColumnGap} / 2)`;

  const gridTemplateColumnsDefault = `calc(50% - var(--editor-column-gap) / 2 ) var(--editor-column-gap) calc(50% - var(--editor-column-gap) / 2 )`;
  const [gridTemplateColumns, setGridTemplateColumns] = useState(gridTemplateColumnsDefault);
  const splitEditorRef = useRef(null);

  const draggableDivMoved = (splitEditorRef) => ({ handleLeft, handleRef }) => {
    setGridTemplateColumns(getSplitViewColumnsCss(splitEditorRef, handleRef, handleLeft))
  }

  const rightToolbarContent = <>
    <div className="natbraille-editor-menubar-subbar">
      <button className="natbraille-editor-menuicon toolbar-item" onClick={buttonSaveBrailleClick}
        aria-label={brailleToolBarLabels["save-braille"]}
        title={brailleToolBarLabels["save-braille"]}
      >{brailleToolBarLabels["save-braille"]}</button>
    </div>
    <div className="natbraille-editor-menubar-subbar">
      <input width={'2em'} className="natbraille-editor-menuicon toolbar-item" type="number" style={{ width: '5em' }}
        min={12} max={100} value={getUIOption(uiOptions)(UI_OPTION_BRAILLE_FONT_SIZE)} onChange={fontSizeChanged}
        aria-label={brailleToolBarLabels["braille-font-size"]}
        title={brailleToolBarLabels["braille-font-size"]}
      ></input>
    </div>
    <div className="natbraille-editor-menubar-subbar">
      <label className="form-check-label">
        <input className="natbraille-editor-menuicon toolbar-item form-check-input" type="checkbox" checked={getUIOption(uiOptions)(UI_OPTION_COLORED_BRAILLE_OUTPUT)} onChange={tintedChanged}
          aria-label={brailleToolBarLabels["color-braille"]}
          title={brailleToolBarLabels["color-braille"]}
        ></input>
        &nbsp;{brailleToolBarLabels["color-braille"]}
      </label>
    </div>
    <div className="natbraille-editor-menubar-subbar">
      <button className="natbraille-editor-menuicon toolbar-item" onClick={setDetranscriptionSourceThenNavigate}
        aria-label={brailleToolBarLabels["set-detranscription-braille"]}
        title={brailleToolBarLabels["set-detranscription-braille"]}
      >{brailleToolBarLabels["set-detranscription-braille"]}</button>
    </div>
  </>

  return <div id="split-editor" ref={splitEditorRef} style={{ gridTemplateColumns }}>
    {/*<div id="common-toolbar">common toolbar</div>*/}
    <div id="common-toolbar">
      {/*<NewDocumentBox></NewDocumentBox>*/}
    </div>
    {/* left toolbar is added by editor */}
    <Editor
      /*filename={editorGetFileSearchParam(searchParams)}*/
      editedFileContext={editedFileContext}
      navigate={navigate}
      brailleContentMarkupRef={brailleContentMarkupRef}
      setLastSaved={setLastSaved}
      setLastTransactionTimestamp={setLastTransactionTimestamp}
      setBrailleCursorPosition={setBrailleCursorPosition}
      setBrailleCursorPosition2={setBrailleCursorPosition2}
      currentStarMathString={currentStarMathString}
      currentLatexString={currentLatexString}
      currentIsChemistry={currentIsChemistry}
      openMathEditorModal={openMathEditorModal}
      options={options}
      uiOptions={uiOptions}

    />
    {/*<div className="braille-layout-container">
      <div className="braille-layout">{brailleContent}</div>
    </div>
*/}
    {/*<div className="braille-layout-container-markup" cursorposition={brailleCursorPosition} cursorposition2={brailleCursorPosition2}>*/}

    <DraggableDiv id="resize-handle" moved={draggableDivMoved(splitEditorRef)}></DraggableDiv>

    <div id="right-toolbar" className="rounded-bottom-border">
      {rightToolbarContent}
      <HamburgerDropDown content={rightToolbarContent} />
    </div>
    {/*<div className="braille-layout-container-markup" >*/}
    {/*<div className="braille-layout-markup">{brailleContentMarkup}</div>*/}
    {/*<div className="braille-layout-markup" dangerouslySetInnerHTML={brailleContentMarkup} ></div>*/}
    <BrailleColorsStyle></BrailleColorsStyle>
    <div className={`braille-layout-markup ${getUIOption(uiOptions)(UI_OPTION_COLORED_BRAILLE_OUTPUT) ? "tinted-braille" : ""}`} style={{ ...fontFamily, "fontSize": getUIOption(uiOptions)(UI_OPTION_BRAILLE_FONT_SIZE) + "px" }} ref={brailleContentMarkupRef}></div>
    {/*</div>*/}
    <div id="editor-footer">dernière modification: {lastTransactionTimestamp}, dernière sauvegarde : {lastSaved}</div>

    <MathEditor finishEditing={finishEditing}
      mathEditorFinishedPromiseAccept={mathEditorFinishedPromiseAccept}
      mathEditorFinishedPromiseReject={mathEditorFinishedPromiseReject}
      currentLatexString={currentLatexString}
      currentStarMathString={currentStarMathString}

      //setCurrentStarMathString={setCurrentStarMathString} 
      mathEditorModalOpenButtonRef={mathEditorModalOpenButtonRef}
      currentIsChemistry={currentIsChemistry}
      setCurrentIsChemistry={setCurrentIsChemistry}>
    </MathEditor>

  </div>
}

// openMathEditorModal={openMathEditorModal}