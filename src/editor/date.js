export function printDate(date) {
    const pad = (i) => (i < 10) ? "0" + i : "" + i;
    return [
        pad(date.getHours()),
        pad(date.getMinutes()),
        pad(date.getSeconds()),
        pad(date.getUTCMilliseconds())
    ].join(':')
}
