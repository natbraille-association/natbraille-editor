
/////////////
///////////// end math
/////////////
import { SOURCE_SCHEMA } from "./prosemirror-model/schema"
import { Decoration, DecorationSet } from "prosemirror-view"
import { Plugin } from "prosemirror-state";
import { reactPropsKey } from "./reactPropKey";
import { DOMParser, DOMSerializer } from "prosemirror-model";
import { getDomOuterString } from "./memo";
import { postStarMathToMathMLTranscription } from "../serverio/transcription";
import katex from "katex"

export const createMathNodeFromXml = (mathMLString, isChemistry) => {

    //   const documentBuffer = `<?xml version="1.0" encoding="UTF-8"?>
    // 
    // <math xmlns="http://www.w3.org/1998/Math/MathML" display="block">
    // <semantics>
    // <mn>5</mn>
    // <annotation encoding="StarMath 5.0">5</annotation>
    // </semantics>
    // </math>
    //   `
    const domParser = new window.DOMParser()
    const $startDocWhole = domParser.parseFromString(mathMLString, "application/xml")
    const $startDoc = $startDocWhole.getElementsByTagName("math")[0];
    $startDoc.setAttribute("display", "inline")
    const startDoc = DOMParser.fromSchema(SOURCE_SCHEMA).parse($startDoc)


    let mathNode = undefined
    startDoc.descendants((node, pos, parent, index) => {
        if (node.type.name === 'math') {
            if (isChemistry) {
                node.attrs.chemistry = isChemistry
            } else {
                node.attrs.chemistry = isChemistry
            }
            mathNode = node
            return false
        }
        //return true
    })
    return mathNode;
}

export const findFirstParentNodeAtResolvedPosNamed = ($resolvedPos, nodeName) => {
    for (let i = $resolvedPos.depth; i >= 0; i--) {
        const node = $resolvedPos.node(i);
        if (node.type.name === nodeName) {
            // const start = $resolvedPos.start(i);   //The (absolute) position at the start of the node at the given level.
            // const end = $resolvedPos.end(i);   //The (absolute) position at the start of the node at the given level.
            const start = $resolvedPos.before(i);   //The (absolute) position at the start of the node at the given level.
            const end = $resolvedPos.after(i);   //The (absolute) position at the start of the node at the given level.
            return { node, start, end }
        }
    }
}

export const findSemanticAnnotationInMath = ($mathNode) => {
    // find text content of the first semantics node in a mathml node
    let semanticsNode = undefined
    $mathNode.descendants((aNode, _, aParent) => {
        if (aParent.type.name === SOURCE_SCHEMA.nodes.m_annotation.name) {
            semanticsNode = aParent;
            return false;
        }
    })
    if (semanticsNode) {
        const text = semanticsNode?.textContent
        const encoding = semanticsNode?.attrs?.encoding;
        return { text, encoding }
    } else {
        return undefined
    }
}


export const placeholderPlugin = new Plugin({
    state: {
        init() { return DecorationSet.empty },
        apply(tr, set) {
            // Adjust decoration positions to changes made by the transaction
            set = set.map(tr.mapping, tr.doc)
            // See if the transaction adds or removes any placeholders
            let action = tr.getMeta(this)
            if (action && action.add) {
                let widget = document.createElement("placeholder")
                let deco = Decoration.widget(action.add.pos, widget, { id: action.add.id })
                set = set.add(tr.doc, [deco])
            } else if (action && action.remove) {
                set = set.remove(set.find(null, null, spec => spec.id == action.remove.id))
            }
            return set
        }
    },
    props: {
        decorations(state) { return this.getState(state) }
    }
})


export function findPlaceholder(state, id) {
    let decos = placeholderPlugin.getState(state)
    let found = decos.find(null, null, spec => spec.id == id)
    return found.length ? found[0].from : null
}


async function startEquationEdition(view) {


    // A fresh object to act as the ID for this upload
    let id = {}

    // Replace the selection with a placeholder
    let tr = view.state.tr


    let maybeStarMath = ''
    let maybeLatex = ''

    let maybeMath = undefined
    let maybeIsChemistry = false
    if (tr.selection.$head) {
        // get selection $head (todo : other positions, whole selection using nodesBetween() )
        const $head = tr.selection.$head
        maybeMath = findFirstParentNodeAtResolvedPosNamed($head, SOURCE_SCHEMA.nodes.math.name)
        if (maybeMath) {
            maybeIsChemistry = maybeMath.node?.attrs?.chemistry ? true : false
            const maybeAnnotation = findSemanticAnnotationInMath(maybeMath.node)
            if (maybeAnnotation) {
                if (maybeAnnotation.encoding === 'StarMath 5.0') {
                    maybeStarMath = maybeAnnotation.text
                } else if (maybeAnnotation.encoding === 'application/x-tex') {
                    maybeLatex = maybeAnnotation.text
                }
            }
        }
    }


    //
    if (!tr.selection.empty) {
        tr.deleteSelection()
    }
    tr.setMeta(placeholderPlugin, { add: { id, pos: tr.selection.from } })
    view.dispatch(tr)

    // modal window
    const { openMathEditorModal } = reactPropsKey.getState(view.state)

    let acceptPromise = undefined
    let rejectPromise = undefined
    const editFinishedPromise = new Promise((accept, reject) => {
        acceptPromise = () => accept
        rejectPromise = () => { }; //reject
    })
    openMathEditorModal(maybeStarMath, maybeLatex, maybeIsChemistry, acceptPromise, rejectPromise)


    let clean = false
    const cleanupPlaceHolder = () => {
        if (!clean) {
            view.dispatch(tr.setMeta(placeholderPlugin, { remove: { id } }))
            clean = true
        }
    }
    {
        try {
            const { starMath5, latex, isChemistry } = await editFinishedPromise
            let mathMLString
            if (latex && latex.length) {
                mathMLString = katex.renderToString(latex);
            } else if (starMath5 && starMath5.length) {
                mathMLString = await postStarMathToMathMLTranscription(starMath5);
            }

            let mathNode = createMathNodeFromXml(mathMLString, isChemistry)
            let pos = findPlaceholder(view.state, id)
            // If the content around the placeholder has been deleted, drop
            // the image
            if (pos == null) return
            // Otherwise, insert it at the placeholder's position, and remove
            // the placeholder

            if (maybeMath) {
                //
                //
                // WARNING : we should recompute positions of the math node around our placeholder
                //
                //
                view.dispatch(view.state.tr
                    //.replaceWith(pos, pos, SOURCE_SCHEMA.nodes.image.create({ src: dataURL }))
                    .replaceWith(maybeMath.start, maybeMath.end, mathNode)
                    .setMeta(placeholderPlugin, { remove: { id } }))
            } else {
                view.dispatch(view.state.tr
                    //.replaceWith(pos, pos, SOURCE_SCHEMA.nodes.image.create({ src: dataURL }))
                    .replaceWith(pos, pos, mathNode)
                    .setMeta(placeholderPlugin, { remove: { id } }))
            }



        } catch (e) {
            console.error(e)
            cleanupPlaceHolder()
        }
    }
}

export const InsertMathCommand = () => {
    return function save(state, dispatch, view) {
        if (dispatch) {
            startEquationEdition(view)
        }
        // can always add/modify a math formula
        return true
    }
}
