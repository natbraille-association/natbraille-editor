import { DOMSerializer } from "prosemirror-model"

const replaceEntitiesByUnicodeEquivalent = string => {
    // when the document contains standard html entities, they won't be parsed
    // because they are not defined in the java xml counterpart doc/parser.
    // So we have to replace them...
    // TODO, can do better
    return string.replaceAll("&nbsp;"," ");
}
// serialize a prosemirror document fragment to a DOM String
export const getDomOuterString = (fragment, schema, rootTagName) => {
    const domSerializer = DOMSerializer.fromSchema(schema)
    const $target = document.createElement(rootTagName)
    const dom = domSerializer.serializeFragment(fragment, {
        "document": window.document
    }, $target)    
    return replaceEntitiesByUnicodeEquivalent(dom.outerHTML)
}