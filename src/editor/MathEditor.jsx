import './MathEditor.scss'
import { useId, useRef, useState, useEffect, useContext } from "react"
import { postStarMathToMathMLTranscription } from "../serverio/transcription"
import { OptionsContext } from "../context/optionsContext"
import { UI_OPTION_MATH_EDITOR_MATHML_FONT_SIZE, getUIOption } from "../context/uiOptions"
import { useDebouncedCallback } from 'use-debounce';
import katex from 'katex'

const ANNOTATION_FORMAT_LATEX = "application/x-tex"
const ANNOTATION_FORMAT_STARMATH5 = 'StarMath 5.0'
const ANNOTATION_FORMATS = [ANNOTATION_FORMAT_STARMATH5, ANNOTATION_FORMAT_LATEX]
const formatLabels = {
  [ANNOTATION_FORMAT_LATEX]: "(La)TeX",
  [ANNOTATION_FORMAT_STARMATH5]: "StarMath 5.0 (LibreOffice)"
}

// debounce call to server startmath -> mathml
const STARMATH_TO_MATHML_DEBOUNCE_DELAY_MS = 400;

const starMath5ToMathElement = async (starMath5String) => {
  if (starMath5String) {
    const mathMLString = await postStarMathToMathMLTranscription(starMath5String)
    if (mathMLString) {
      const domParser = new window.DOMParser()
      //const $startDocWhole = domParser.parseFromString(mathMLString, "application/xml")
      const $doc = domParser.parseFromString(mathMLString, "text/html")
      const $mathML = $doc.getElementsByTagName("math")[0];
      return $mathML
    }
  }
}
const latexToMathElement = (latexString) => {
  if (latexString) {
    try {
      const mathMLString = katex.renderToString(latexString, {
        //throwOnError: false,
        //output: "mathml"
      });

      if (mathMLString) {
        const domParser = new window.DOMParser()
        const $doc = domParser.parseFromString(mathMLString, "text/html")
        const $mathML = $doc.getElementsByTagName("math")[0];
        return { $mathElement: $mathML }
      }
    } catch (e) {
      if (e instanceof katex.ParseError) {
        const $htmlError = ("Error in LaTeX '" + latexString + "': " + e.message).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        return { $error: $htmlError }
      }
    }
  }
}

const Labels = {
  'equation-editor': "Éditeur d'équation",
  'ok': 'valider',
  'cancel': 'annuler',
  'starmath-equation-placeholder': "saisissez l'équation au format starmath (libreoffice)",
  'latex-equation-placeholder': "saisissez l'équation au format latex",
  'denotes-chemistry': 'chimie',
  'denotes-math': 'mathématiques',
  'updating preview':'mise à jour de la prévisualisation...'
}


export const MathEditor = ({ finishEditing, mathEditorFinishedPromiseAccept, mathEditorFinishedPromiseReject, currentStarMathString, currentLatexString, currentIsChemistry, mathEditorModalOpenButtonRef }) => {

  const { uiOptions } = useContext(OptionsContext) // does not need to be reactive as editor will be reloaded when option page closes

  // note : a solution using mathmlPreviewRef.current.innerHTML = mathmlPreviewString
  // is alway a keypress late (mathml preview display previous state)
  // but dangerouslySetInnerHTML works fine 

  const modalLabelId = useId()
  const mathEditorModalId = useId()

  const modalCloseRef = useRef(null)
  //const mathEditorModalRef = useRef()

  const mathmlPreviewRef = useRef(null)

  const [localStarMathString, setLocalStarMathString] = useState(currentStarMathString)
  const [localLatexString, setLocalLatexString] = useState(currentLatexString)

  const [localEquationIsChemistry, setLocalEquationIsChemistry] = useState(currentIsChemistry)
  const [mathmlPreviewString, setMathmlPreviewString] = useState('')

  const [format, setFormat] = useState(ANNOTATION_FORMAT_STARMATH5)

  const [typesettingFromStarMath, setTypesettingFromStarMath] = useState(false)
  //const typesettingFromStarMath = useRef(false)

  useEffect(() => {
    if (currentStarMathString?.length > 0) {
      console.log("UE--", "currentStarMathString", currentStarMathString)
      setFormat(ANNOTATION_FORMAT_STARMATH5)
    }
    setLocalStarMathString(currentStarMathString);
  }, [currentStarMathString]);

  useEffect(() => {
    if (currentLatexString?.length > 0) {
      console.log("UE--", "currentLatexString", currentLatexString)
      setFormat(ANNOTATION_FORMAT_LATEX)
    }
    setLocalLatexString(currentLatexString);
  }, [currentLatexString]);

  useEffect(() => {
    setLocalEquationIsChemistry(currentIsChemistry);
  }, [currentIsChemistry]);

  const doUpdateFromStarMath = useDebouncedCallback(async (localStarMathString) => {
    //setTypesettingFromStarMath(true)
    //typesettingFromStarMath.current = true
    const maybeDom = await starMath5ToMathElement(localStarMathString)
    //typesettingFromStarMath.current = false
    setTypesettingFromStarMath(false)
    if (maybeDom) {
      setMathmlPreviewString(maybeDom.outerHTML)
    } else {
      /* setMathmlPreviewString(`<p style="font-style:italic">${Labels['starmath-equation-placeholder']}</p>`)*/
      setMathmlPreviewString(`<p></p>`)
    }
  }, STARMATH_TO_MATHML_DEBOUNCE_DELAY_MS)

  useEffect(() => {
    //console.log("do update from starmath A")
    setTypesettingFromStarMath(true)
    doUpdateFromStarMath(localStarMathString)
    //setTypesettingFromStarMath(false)
    //console.log("do update from starmath B")
  }, [localStarMathString])

  useEffect(() => {
    if (typesettingFromStarMath) {

      // Handle side effects of starting typesetting
    } else {
      // Handle side effects of finishing typesetting
    }
  }, [typesettingFromStarMath]);

  const starMathStringUserChanged = (e) => {
    setLocalStarMathString(e.target.value)
  }

  const doUpdateFromLatex = (localLatexString) => {
    const maybeDom = latexToMathElement(localLatexString)
    if (maybeDom) {
      if (maybeDom.$mathElement) {
        setMathmlPreviewString(maybeDom.$mathElement.outerHTML)
      } else if (maybeDom.$error) {
        setMathmlPreviewString(`<p style="font-style:italic">${maybeDom.$error}</p>`)
      }
    } else {
      /*setMathmlPreviewString(`<p style="font-style:italic">${Labels['latex-equation-placeholder']}</p>`)*/
      setMathmlPreviewString(`<p></p>`)
    }
  }

  useEffect(() => {
    doUpdateFromLatex(localLatexString)
  }, [localLatexString])

  const latexStringUserChanged = (e) => {
    setLocalLatexString(e.target.value)
  }
  const equationIsChemistryChanged = (e) => {
    setLocalEquationIsChemistry(e.target.value === 'true')
  }
  const editFinished = () => {
    if (mathEditorFinishedPromiseAccept) {
      mathEditorFinishedPromiseAccept({
        starMath5: (format === ANNOTATION_FORMAT_STARMATH5) ? localStarMathString : "",
        latex: (format === ANNOTATION_FORMAT_LATEX) ? localLatexString : "",
        isChemistry: localEquationIsChemistry
      })
    }

    modalCloseRef.current.click()
    finishEditing()
  }
  const editCancelled = () => {
    if (mathEditorFinishedPromiseReject) {
      mathEditorFinishedPromiseReject()
    }
    finishEditing()
  }

  const formatUpdated = newFormat => {
    setFormat(newFormat);
  }

  const $formatSelector = <select className="form-select mb-2 w-auto" value={format} onChange={(e) => formatUpdated(e.target.value)}>
    {ANNOTATION_FORMATS.map((fmt, index) => (
      <option key={index} value={fmt}>
        {formatLabels[fmt]}
      </option>
    ))}
  </select>

  const inputTextAreaRef = useRef()
  const isOpening = () => {
    // put focus on input
    document.body.inputTextAreaRef = inputTextAreaRef
    // TODO: this is ugly but the Editor and EditorSplit are rendered after
    // openMathEditorModal (which clicks to open the modal editor) is called, putting the focus 
    // back in the editor.
    // Timeout is set to not seem to lag too much and might work, or not, depending on the machine.
    if (inputTextAreaRef.current) {
      setTimeout( () => {
        inputTextAreaRef.current.focus();
      },500)
    } 
    
  }

  return <>

    <button onClick={isOpening} ref={mathEditorModalOpenButtonRef} style={{ display: "none" }} type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target={`#${mathEditorModalId}`}>
      Launch static backdrop modal
    </button>

    <div className="modal fade modal-lg" id={mathEditorModalId} data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby={modalLabelId} aria-hidden="true">
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id={modalLabelId}>{Labels['equation-editor']}</h1>

            <div className="form-check ms-4 mt-2">
              <label className="form-check-label">
                <input className="form-check-input"
                  type="radio"
                  name="eqType"
                  value="false"
                  checked={!localEquationIsChemistry}
                  onChange={equationIsChemistryChanged}
                />
                {Labels['denotes-math']}
              </label>
            </div>

            <div className="form-check ms-4 mt-2">
              <label className="form-check-label">
                <input className="form-check-input"
                  type="radio"
                  name="eqType"
                  value="true"
                  checked={localEquationIsChemistry}
                  onChange={equationIsChemistryChanged}
                />
                {Labels['denotes-chemistry']}
              </label>
            </div>

            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div className="modal-body">
            <style>{`.mathml-preview math { font-size:${getUIOption(uiOptions)(UI_OPTION_MATH_EDITOR_MATHML_FONT_SIZE)}px; }`}</style>
            <div className={"preview-spinner "+(typesettingFromStarMath ? "waiting" : "not-waiting")}>              
              <div className={"spinner-grow spinner-grow-sm text-dark "+(typesettingFromStarMath ? "waiting" : "not-waiting")}role="status">
              <span className="visually-hidden">{Labels["updating preview"]}</span>
              </div>
            </div>
            <div className="mathml-preview" ref={mathmlPreviewRef} dangerouslySetInnerHTML={{ __html: mathmlPreviewString }}></div>
            <div>{$formatSelector}</div>
            {(format == ANNOTATION_FORMAT_STARMATH5) && (
              <textarea ref={inputTextAreaRef} className="math-text-input starmath-input" value={localStarMathString} onChange={starMathStringUserChanged} />
            )}
            {(format == ANNOTATION_FORMAT_LATEX) && (
              <textarea ref={inputTextAreaRef} className="math-text-input latex-input" value={localLatexString} onChange={latexStringUserChanged} />
            )}
          </div>
          <div className="modal-body">
            {(format == ANNOTATION_FORMAT_STARMATH5) && (
              <a href="http://natbraille.free.fr/server-doc/starmath.html" target="_blank">Ouvrir la documentation du format starmath</a>
            )}
            {(format == ANNOTATION_FORMAT_LATEX) && (
              <a href="https://katex.org/docs/supported" target="_blank">Ouvrir la documentation du format (La)TeX pris en charge</a>
            )}
          </div>
          <div className="modal-footer">
            <button ref={modalCloseRef} type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={editCancelled}>{Labels['cancel']}</button>
            <button type="button" className="btn btn-primary" onClick={editFinished}>{Labels['ok']}</button>
          </div>
        </div>
      </div>
    </div>
  </>
}
