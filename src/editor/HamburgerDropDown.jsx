export const HamburgerDropDown = ({ content }) => {
    return <div className="natbraille-editor-menubar-subbar dropdown">
        <button className="bi bi-list toolbar-item natbraille-editor-menuicon" data-bs-toggle="dropdown"></button>
        <div className="dropdown-menu">
            {content}
        </div>
    </div>
}