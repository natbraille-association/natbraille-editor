import { Plugin } from "prosemirror-state";
import { reactPropsKey } from "./reactPropKey";
import { UI_OPTION_PERKINS_KEYS, UI_OPTION_USE_PERKINS_INPUT_IN_BRAILLE_BLOCK, getUIOption } from "../context/uiOptions";
import { isMarkActive } from "./editor";

const registerPerkinsInput = (view) => {

    console.log('registerPerkinsInput')
    let brailleCharCode = 0x2800; // Base Unicode for Braille
    let downCount = 0;

    //const uiOptions = view.state.tr.getMeta(reactPropsKey).uiOptions;
    const reactPropsState = reactPropsKey.getState(view.state);
    const { uiOptions } = reactPropsState


    const pk = getUIOption(uiOptions)(UI_OPTION_PERKINS_KEYS)
    const dots = Object.keys(pk.codes).sort().map(k => pk.codes[k])
    const usePerkins = getUIOption(uiOptions)(UI_OPTION_USE_PERKINS_INPUT_IN_BRAILLE_BLOCK)

    const getDot = (e) => dots.indexOf(e.code);

    const perkinsEditIsInAllowedBlock = (view) => {

        if (!usePerkins) return false;
        const { state, dispatch } = view;
        const { selection } = state;
        const { $from } = selection;
        const node = $from.node();
        const allowedBlockTypes = ["braille_block"];
        
        
        if (allowedBlockTypes.includes(node.type.name)) {
            return true;
        }
        if (isMarkActive(state, state.schema.marks.grade, { grade: 'g0' })){
            return true;
        }
        return false;

    }
    const resetCell = () => {
        brailleCharCode = 0x2800
        downCount = 0
    }
    const keydownHandler = (event) => {

        if (!perkinsEditIsInAllowedBlock(view)) {
            resetCell()
            return
        }

        const dot = getDot(event);
        if (dot !== -1) {
            event.preventDefault();
            if (event.repeat) return;

            brailleCharCode |= 1 << dot;
            downCount++;
        } else {

        }

    };

    const keyupHandler = (event) => {
        if (!perkinsEditIsInAllowedBlock(view)) {
            resetCell()
            return
        }

        if (getDot(event) !== -1) {
            downCount--;
            if (downCount === 0) {
                const { state, dispatch } = view;
                const brailleChar = String.fromCharCode(brailleCharCode);
                const tr = state.tr.insertText(brailleChar);
                dispatch(tr);
                // Reset for the next input
                resetCell();
            }

            event.preventDefault();
        }
    };

    // Attach event listeners
    view.dom.addEventListener("keydown", keydownHandler);
    view.dom.addEventListener("keyup", keyupHandler);

    return {
        destroy() {
            console.log('registerPerkinsInput')
            view.dom.removeEventListener("keydown", keydownHandler);
            view.dom.removeEventListener("keyup", keyupHandler);
        },
    };
};

export const perkinsInputPlugin = new Plugin({
    view(editorView) {
        return registerPerkinsInput(editorView);
    },
});
