import { useState, useRef, useEffect } from "react";

export const getSplitViewColumnsCss = (splitEditorRef, handleRef, handleLeft) => {

    // container
    const $container = splitEditorRef.current;
    const containerRect = $container.getBoundingClientRect();
    const containerLeft = containerRect.left;
    const containerRight = containerRect.right;
    const containerWidth = containerRect.width;

    const marginPx = 250;
    const min = containerLeft + marginPx
    const max = containerRight - marginPx

    // handle    
    //const editorColumnGap = window.getComputedStyle(document.body).getPropertyValue('--editor-column-gap'); // -> "0.5em"
    //const pixelValue = window.getComputedStyle(document.querySelector("#resize-handle")).width; // -> "8px"
    //const pixelMeasure = document.querySelector("#resize-handle").getBoundingClientRect().width; // -> 8
    const $handle = handleRef.current
    const handleRect = $handle.getBoundingClientRect()
    const handleWidth = handleRect.width
    //const handleLeft = handleRect.left

    //const pixelMeasure = handleRef.current.getBoundingClientRect().width; // -> 8
    // console.log({ containerLeft, containerWidth, handleLeft, handleWidth })
    // clamped handle left value
    const leftColumnWidth = Math.min(Math.max(min, handleLeft - containerLeft), max)

    // ratios
    const leftRatio = Math.min(parseFloat(100 * leftColumnWidth / (containerWidth - handleWidth)).toFixed(1), 100)
    const rightRatio = 100 - leftRatio

    const cssColumns = `calc(${leftRatio}% - var(--editor-column-gap) / 2 ) var(--editor-column-gap) calc(${rightRatio}% - var(--editor-column-gap) / 2 )`;
    // alternatively, do not set ratios but pixel values (no panles resize on window resize)
    // const cssColumns = `${leftColumnWidth}px ${editorColumnGap} calc(100% - ${editorColumnGap} - ${leftColumnWidth}px)`
    return cssColumns
}

/**
 * Draggable div
 */

export const DraggableDiv = ({ id, moved }) => {
    const [isDragging, setIsDragging] = useState(false);
    const [position, setPosition] = useState({ x: 0, y: 0 });
    const [offset, setOffset] = useState({ x: 0, y: 0 });
    const divRef = useRef(null);

    const handleMouseDown = (e) => {
        //console.log("handleMouseDown")
        setIsDragging(true);
        // Calculate the initial offset between mouse and element position
        const rect = divRef.current.getBoundingClientRect();
        setOffset({
            x: e.clientX - rect.left,
            y: e.clientY - rect.top,
        });
        document.body.classList.add("no-select-while-moving-resize-handle");
    };

    const handleMouseMove = (e) => {
        //console.log("handleMouseMove")
        if (isDragging) {
            // Update the element's position based on mouse movement
            const x = e.clientX - offset.x
            const y = e.clientY - offset.y
            setPosition({ x, y });
            moved({ handleLeft: x, handleRef: divRef })
        }
    };

    const handleMouseUp = () => {
        //console.log("handleMouseUp")
        setIsDragging(false);
        document.body.classList.remove("no-select-while-moving-resize-handle");
    };

    // Attach event listeners to the window
    useEffect(() => {
        if (isDragging) {
            window.addEventListener("mousemove", handleMouseMove);
            window.addEventListener("mouseup", handleMouseUp);
        } else {
            window.removeEventListener("mousemove", handleMouseMove);
            window.removeEventListener("mouseup", handleMouseUp);
        }

        // Cleanup event listeners on unmount
        return () => {
            window.removeEventListener("mousemove", handleMouseMove);
            window.removeEventListener("mouseup", handleMouseUp);
        };
    }, [isDragging]);

    return (
        <div
            id={id}
            ref={divRef}
            onMouseDown={handleMouseDown}
            className={isDragging ? 'is-dragging' : ''}
        />
    );
};

