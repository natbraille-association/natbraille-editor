import { saveOdt, saveXML } from "../localio/save"
import { postBlackToOdt } from "../serverio/export"
import { getUserNatbrailleDocumentList, postUserNatbrailleDocument } from "../serverio/userDocument"
import { printDate } from "./date"
import { getDomOuterString } from "./memo"
import { reactPropsKey } from "./reactPropKey"
import { SOURCE_DOCUMENT_ROOT_TAGNAME, SOURCE_SCHEMA } from "./prosemirror-model/schema"
import { createUniqueFilename } from "../documents/uniqueFilename"
import { navigateToEditorFile } from "../clientRoutes"

/*
const NewDocumentCommand = () => {
  return function deleteAll(state, dispatch) {
    if (dispatch) {
      const xmlString = getDomOuterString(state.doc, SOURCE_SCHEMA, SOURCE_DOCUMENT_ROOT_TAGNAME)
      dispatch(state.tr.delete(0, state.doc.content.size))
      return true
    }
    // can always delete
    return true
  }
}
*/


export const SaveCommand = () => {
    return function save(state, dispatch) {
        // console.log('SaveCommand')
        if (dispatch) {
            // won't use dispatch because all function is only an effect
            const xmlString = getDomOuterString(state.doc, SOURCE_SCHEMA, SOURCE_DOCUMENT_ROOT_TAGNAME)
            const encoder = new TextEncoder()
            const arrayBuffer = encoder.encode(xmlString)
            const { editedFileContext, navigate } = reactPropsKey.getState(state);
           
            
            const filename = editedFileContext.editedFile.filename

            const save = async () => {
                let realFilename;
                let created = false
                if (filename == null) {
                    const blacklist = await getUserNatbrailleDocumentList()
                    const newFilename = createUniqueFilename("Sans Titre.natbraille", blacklist || [])
                    realFilename = newFilename                    
                    created =true;
                }  else {
                    realFilename = filename
                }               
                await postUserNatbrailleDocument(realFilename, arrayBuffer)
                const { setLastSaved } = reactPropsKey.getState(state)                
                setLastSaved(printDate(new Date))
                editedFileContext.setEditedFileContextFilename(realFilename)
                if (created){
                    navigateToEditorFile(navigate)(realFilename);
                }
            }
            save();
        }
        // can always save
        return true

    }
}
export const ExportXMLCommand = () => {
    return function save(state, dispatch) {
        if (dispatch) {
            // won't use dispatch because all function is only an effect
            const xmlString = getDomOuterString(state.doc, SOURCE_SCHEMA, SOURCE_DOCUMENT_ROOT_TAGNAME)
            saveXML(xmlString, "document.xml");
        }
        // can always save
        return true

    }
}
export const OdtExportCommand = () => {
    return function save(state, dispatch) {
        if (dispatch) {
            // won't use dispatch because all function is only an effect
            const xmlString = getDomOuterString(state.doc, SOURCE_SCHEMA, SOURCE_DOCUMENT_ROOT_TAGNAME)
            postBlackToOdt(xmlString).then((arrayBuffer) => {
                saveOdt(arrayBuffer, "document.odt");
            })
        }
        // can always save
        return true

    }
}
export const ReplaceAllCommand = () => {

    return function replaceAll(state, dispatch) {
        if (dispatch) {
            // won't use dispatch because all function is only an effect
            const xmlString = getDomOuterString(state.doc, SOURCE_SCHEMA, SOURCE_DOCUMENT_ROOT_TAGNAME)
            dispatch(state.tr.delete(0, state.doc.content.size))
        }
        // can always replace
        return true
    }
}

export const FocusOnBraillePanel = (QUIRKY_PARSED) => {
    return function replaceAll(state, dispatch) {
        const $brailleElement = QUIRKY_PARSED?.brailleSelectedElement
        if (dispatch) {
            if ($brailleElement){
                $brailleElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" })
                $brailleElement.focus()
            }            
        }
        // can always focus
        return !!$brailleElement 
    }
}
/*
import { toggleMark } from "prosemirror-commands"

// Function to toggle the grade mark with a specific attribute
function toggleGradeMark(grade) {
    return (state, dispatch) => {
      // Access the grade mark type from the schema
      const markType = state.schema.marks.grade;
  
      // Create the mark with the specified grade attribute
      const mark = markType.create({ grade });
  
      // Use toggleMark to apply or remove the mark
      return toggleMark(mark)(state, dispatch);
    };
  }
export const toggleGradeMarkG0 = toggleGradeMark("g0")
export const toggleGradeMarkG1 = toggleGradeMark("g1")
export const toggleGradeMarkG2 = toggleGradeMark("g2")
*/