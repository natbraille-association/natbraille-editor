import { useId, useState } from "react"
import { NatbrailleBig } from "../logo/NatbrailleBig"
import { useEffect } from "react"
import { getClientVersion, getServerVersion } from "../serverio/version"
import { NavLink } from "react-router-dom"
import './About.scss'

const finance = [
    ["/finance/Logo_include_degrade_rvb-2-1.png", "Include"], 
    ["/finance/LogoLyon1Sig_CoulCmjnVecto.eps.png","Université Lyon 1"],
    ["/finance/logo-region.svg", "Region Auvergne-Rhône-Alpes"], 
    ["/finance/edu-up-514647_943844.webp", "Edu-Up ministrère de l'Éducation Nationale"], 
    ["/finance/logo-institut-forja.png", "Forja"],
    ["/finance/coexiscience-cropped-Logo-standard-1-1.png", "Coexiscience"],
    ["/finance/Logo_Sorbonne_Université.png", "Sorbonne Université"],
    ["/finance/Logo_Macif.svg", "Macif"],
    ["/finance/Ministère-Éducation-Nationale-Jeunesse.svg", "Ministère de l'Éducation Nationale et de la Jeunesse"],
]

export const About = () => {
    const [serverVersion, setServerVersion] = useState([])
    const [clientVersion, setClientVersion] = useState([])

    useEffect(() => {
        const fetchServerVersion = async () => {
            const v = await getServerVersion()
            setServerVersion(Object.entries(v))
        }
        fetchServerVersion()
    }, [])
    useEffect(() => {
        const fetchClientVersion = async () => {
            const v = await getClientVersion()
            setClientVersion(v)
        }
        fetchClientVersion()
    }, [])

    return <div id="welcome">
        <NatbrailleBig />
        <div className="credits">
            <h1>À propos de ce serveur</h1>

            <article className="about">

                <p>Nous sommes très preneurs de retours : suggestions, interrogations,
                    tests, remarques ou reproches à la fois sur la qualité du braille produit
                    et sur l'interface sont les bienvenues.</p>

                <p>Nous vous invitons à vous inscrire à la liste d'entraide nat-users pour toute demande ou
                    suggestion (ainsi, de notre côté, nous n'aurons ainsi pas à modérer vos
                    messages à nat-users qui seront publiés directement) : <a href="http://listes.univ-lyon1.fr/wws/subscribe/nat-users">http://listes.univ-lyon1.fr/wws/subscribe/nat-users</a></p>

                <p>Vous pouvez également nous faire part de vos remarques:</p>
                <ul>
                    <li>par mail à l'adresse <a href="mailto:natbraille@univ-lyon1.fr">natbraille@univ-lyon1.fr</a></li>
                    <li>en utilisant le <NavLink to="/contact">formulaire de contact</NavLink></li>
                </ul>

                Quelques remarques :
                <ol>
                    <li>Nous attirons votre attention sur le fait que, même si nous n'allons
                        évidemment pas exploiter commercialement ce que vous confierez au
                        serveur web, nous ne pouvons pas garantir actuellement la
                        confidentialité des données en cas d'accès malveillant au serveur.
                    </li>
                    <li>Tous les documents que vous soumettez au serveur et  que vous ne supprimez pas explicitement
                        sont actuellement
                        conservés sans limite de temps (pour nous permettre de comprendre les
                        dysfonctionnements éventuels du serveur). Aussi, ne transcrivez aucun
                        document qui contiendrait un mot de passe, des coordonnées bancaires ou
                        quelque chose dans ce goût.
                    </li>
                    <li>Vous pouvez à tout moment nous demander la suppression de vos données
                        ainsi que celle de votre compte.
                    </li>
                    <li>Par ailleurs, nous vous prions de faire un usage "raisonnable" du
                        serveur : ainsi, si vous constatez des lenteurs inhabituelles, peut être
                        beaucoup de personnes sont elles connectées... et il faudra alors
                        attendre un peu avant de réessayer plus tard (sans oublier de nous
                        prévenir du dysfonctionnement).
                    </li>
                </ol>


            </article>
            <h1>Partenaires</h1>
            <article className="logos">
                {
                    finance.map(([url, name]) =>
                        <div key={url}>
                            <img src={url} alt={name} />
                        </div>
                    )
                }
            </article>

            <h1>version du serveur</h1>
            <article>
                <table className="version">
                    <tbody>
                        {
                            serverVersion.map(([k, v]) =>
                                <tr key={k} kiki={k} className="version-attribute">
                                    <th className="version-attribute-key">{k}</th>
                                    <td className="version-attribute-value">{v}</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </article>

            <h1>version du client</h1>
            <article>
                <table className="version">
                    <tbody>
                        {
                            clientVersion.map(([k, v]) =>
                                <tr key={k} className="version-attribute">
                                    <th className="version-attribute-key">{k}</th>
                                    <td className="version-attribute-value">{v}</td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </article>


        </div>
    </div>

}