export const protectPageChange = () => {
    window.onbeforeunload = () => 'Voulez-vous fermer natbraille ?'
}
export const unprotectPageChange = () => {
    window.onbeforeunload = undefined
}