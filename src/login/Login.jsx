import "./Login.css"
import { useEffect, useState } from "react"
import { NatbrailleBig } from "../logo/NatbrailleBig.jsx"
import { getLoginEndpoint, getLogoutEndpoint, getRegisterEndpoint, getSendRegistrationEmailEndpoint, getSendRegistrationChallengeEndpoint, getRequestPasswordResetEndpoint, getPasswordResetEndpoint } from "../serverio/endpoints.js";
import { useLocation, useNavigate } from "react-router";
import { getNatbrailleTokenFromCookie, getNatbrailleUsernameFromCookie, getNatbrailleRegisterEmailSend, getNatbrailleBadLogin } from "../serverio/cookies.js";
import { NavLink } from "react-router-dom";


const challengeData = (() => {
    const p = new URL(window.location).searchParams
    const username = p.get('username');
    const challenge = p.get('challenge');
    if (username?.length && challenge?.length) {
        //decode ?
        return { username, challenge }
    }
})()

const requestPasswordResetData = (() => {
    const p = new URL(window.location).searchParams
    const username = p.get('username');
    const email = p.get('email');
    const d = {}
    if ((username == null) || (email == null)) {
        // no parameters in the url: password reset no asked yet.        
        return undefined
    } else {
        // parameters in the url, but might be empty if username/email not found
        if (username?.length) d.username = username;
        if (email?.length) d.email = email;
        return d;
    }
})()


/*
const loginNetworkMockup = ({ username, password }) => new Promise((accept, reject) => {
    setTimeout(() => {
        accept({ username, password, logged: true })
    }, 500)
})
const logoutNetworkMockup = ({ username, password }) => new Promise((accept, reject) => {
    setTimeout(() => {
        accept({ username, password, logged: false })
    }, 500)
})
*/
const ConnexionStates = {
    0: 'disconnected',
    1: 'connecting',
    2: 'connected',
    3: 'disconnecting',
}

// https://stackoverflow.com/questions/2382329/how-can-i-get-browser-to-prompt-to-save-password

export const Login = () => {

    const labels = {
        "Connexion": "Connexion",
        "Signup": "Inscription",
        "RenewPassword": "Mot de passe oublié",
        "login": "Login",
        "logout": "Logout",
        "registrationMailSend": "Pour confirmer votre inscription, vous devez valider votre adresse de courriel et suivre le lien qui vous sera envoyé à l'adresse : ",
        "sendRegistrationEmail": "M'envoyer le courriel de confirmation",
        "almostDone": "Votre inscription est presque terminée ! Cliquez sur le bouton ci-dessous et vous pourrez ensuite vous connecter avec vos indentifiants et mot de passe.",
        "confirmRegistration": "Confirmer l'inscription",
        "badLogin": "La connexion a échoué",
        "password-forgotten": "Mot de passe oublié",
        "reset-password": "Réinitialiser le mot de passe",
        "reset-password-again": "Envoyer un nouveau courriel de réinitialisation",
        "no email for user": "Impossible de trouver l'utilisateur nommé",
        "no such user": "Impossible de trouver l'utilisateur",
        "password reset mail sent to": "Un email de réinitialisation de votre mot de passe a été envoyé à l'adresse suivante: ",
        "enter-new-password": "Veuillez saisir votre nouveau mot de passe",
        "cannot process password reset challenge": "Impossible de traiter la réinitialisation du mot de passe.",
        "back-to-reset-password": "recommencer la procédure de réinitialisation du mot de passe"

    }

    const navigate = useNavigate();

    const [connexionState, setConnexionState] = useState(
        (getNatbrailleTokenFromCookie()) ? 2 : 0
    )
    const [username, setUsername] = useState(getNatbrailleUsernameFromCookie() || '')
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const [rememberMe, setRememberMe] = useState(false)

    const clientValidate = () => {
        return (username.length > 0) && (password.length > 0)
    }
    let location = useLocation();
    useEffect(() => {
        console.log('::::', location.pathname)
        if (location.pathname === '/register') {

            //navigate("/logout")  
        }

        //navigate("/logout")//, { state: partialUser });
    })
    const isRegisterPathname = (location.pathname === '/register')
    const isRequestPasswordResetPathname = (location.pathname === '/request-password-reset')
    const isRequestPasswordResetPathnameErrorNoSuchUser = isRequestPasswordResetPathname && requestPasswordResetData && !(requestPasswordResetData.email)
    const isRequestPasswordResetPathnameEmailSent = isRequestPasswordResetPathname && requestPasswordResetData && (requestPasswordResetData.email)

    const isPasswordResetPathname = (location.pathname === '/password-reset')

    /*
    not used since the form is a real POST FORM
    const login = async () => {
        setConnexionState(1)
        const c = await loginNetworkMockup({ username, password })
        if (c.logged) {
            setConnexionState(2)
        } else {
            setConnexionState(0)
        }
        return false
    }
    
    const logout = async () => {
        setConnexionState(3)
        await logoutNetworkMockup(({ username, password }))
        setConnexionState(0)

    }
    */
    const positionClass = (isFirst, isLast) => {
        return `${isFirst ? '':'notfirst' } ${isLast ? '':'notlast' }`
    }

    const InputUsername = ({ autocomplete = "off", isFirst = false, isLast = false } = {}) => {
        const classes = `form-control ${positionClass(isFirst, isLast)}`
        return (
            <div className="form-floating">
                <input id="username" name="username" required disabled={connexionState !== 0} type="text" className={classes} placeholder="Username"
                    aria-label="Username" onChange={({ target }) => setUsername(target.value)} value={username} autoComplete={autocomplete} />
                <label htmlFor="username">username</label>
            </div>
        )
    }
    const InputEmail = ({ autoComplete = "on", isFirst = false, isLast = false } = {}) => {
        const classes = `form-control ${positionClass(isFirst, isLast)}`
        return (
            <div className="form-floating">
                <input id="email" name="email" required disabled={connexionState !== 0} type="email" className={classes} placeholder="Email"
                    aria-label="Email" onChange={({ target }) => setEmail(target.value)} value={email} autoComplete="email"/>
                <label htmlFor="email">email</label>
            </div>
        )
    }
    const InputPassword = ({ autocomplete="current-password", isFirst = false, isLast = false } = {}) => {
        const classes = `form-control ${positionClass(isFirst, isLast)}`
        return (
            <div className="form-floating">
                <input id="password" name="password" required disabled={connexionState !== 0} type="password" className={classes} placeholder="Password" autoComplete={autocomplete}
                    aria-label="Password" onChange={({ target }) => setPassword(target.value)} value={password} />
                <label htmlFor="password">password</label>
            </div>
        )
    }
    const HiddenInput = ({ type, name, value }) => (
        <input hidden type={type} name={name} value={value} />
    )
    /*
    const InputRememberMe = () => (
        <div className="m-2 form-check">
            <input value="remember-me" disabled={connexionState !== 0} className="form-check-input" type="checkbox" id="flexSwitchCheckChecked" onChange={({ target }) => setRememberMe(target.checked)} checked={rememberMe} />
            <label className="form-check-label" htmlFor="flexSwitchCheckChecked" >Rester connecté</label>
        </div>
    )
        */
    const InputSubmit = ({ disabled = false, label }) => (
        <div className="m-2 text-center">
            <button type="submit" disabled={disabled} className="btn btn-primary">{label}</button>
        </div>
    )

    const Message = kind => content => (
        <div className={`my-3 alert alert-${kind} alert-dismissible fade show`} role="alert">
            {content}
            <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    )
    const ErrorMessage = Message("warning")
    const SuccessMessage = Message("success")

    return (<div id="login">

        <NatbrailleBig />

        <div className="mx-auto p-2 form-signin-max-width">
            {
                isRequestPasswordResetPathname ? (
                    <form action={getRequestPasswordResetEndpoint()} method="POST" className="form-signin">
                        {isRequestPasswordResetPathnameErrorNoSuchUser && (
                            ErrorMessage(
                                requestPasswordResetData.username ? (
                                    labels["no email for user"] + " " + requestPasswordResetData.username
                                ) : (
                                    labels["no such user"]
                                )
                            )
                        )}
                        {
                            isRequestPasswordResetPathnameEmailSent && (
                                SuccessMessage(labels["password reset mail sent to"] + " " + requestPasswordResetData.email)
                            )
                        }
                        {InputUsername({isFirst:true,isLast:true,autocomplete:"username"})}
                        {
                            /* display reset password button only if message is not sent */
                            isRequestPasswordResetPathnameEmailSent ?
                                (
                                    InputSubmit({ label: labels["reset-password-again"] })
                                ) : (
                                    InputSubmit({ label: labels["reset-password"] })
                                )
                        }
                    </form>
                ) : isPasswordResetPathname ? (
                    challengeData ? (
                        <form action={getPasswordResetEndpoint()} className="form-signin" method="POST">
                            <p>{labels["enter-new-password"]}</p>
                            {HiddenInput({ type: "text", name: "username", value: challengeData.username })}
                            {HiddenInput({ type: "text", name: "challenge", value: challengeData.challenge })}
                            {InputPassword({autocomplete:"new-password",isFirst:true,isLast:true})}
                            {InputSubmit({ label: labels["reset-password"] })}
                        </form>
                    ) : (
                        <>
                            {ErrorMessage(labels['cannot process password reset challenge'])}
                            <NavLink to="/request-password-reset" className="">{labels['back-to-reset-password']}</NavLink>
                        </>

                    )
                ) : challengeData ? (

                    <>
                        <p>{labels.almostDone}</p>
                        <form action={getSendRegistrationChallengeEndpoint()} method="POST" className="form-signin">
                            <input name="username" value={challengeData.username} type="hidden" />
                            <input name="challenge" value={challengeData.challenge} type="hidden" />
                            {InputSubmit({ label: labels.confirmRegistration })}
                        </form>
                    </>

                ) : getNatbrailleRegisterEmailSend() ? (
                    <>
                        <p>{labels.registrationMailSend} <b>{getNatbrailleRegisterEmailSend()}</b></p>
                        <form action={getSendRegistrationEmailEndpoint()} method="POST" className="form-signin">
                            {InputSubmit({ label: labels.sendRegistrationEmail })}
                        </form>
                    </>
                ) : (<>

                    < ul className="nav nav-tabs" id="user-connection-tab" role="tablist">
                        <li className="nav-item" role="presentation">
                            <button className={"nav-link " + (isRegisterPathname ? "" : "active")} id="user-connection-login-tab" data-bs-toggle="tab" data-bs-target="#user-connection-login-tab-pane" type="button" role="tab"
                                aria-controls="user-connection-login-tab-pane" aria-selected="true">{labels.Connexion}</button>
                        </li>
                        <li className="nav-item" role="presentation">
                            <button disabled={2 === connexionState} className={"nav-link " + (isRegisterPathname ? "active" : "")} id="user-connection-register-tab" data-bs-toggle="tab" data-bs-target="#user-connection-register-tab-pane" type="button" role="tab"
                                aria-controls="user-connection-register-tab-pane" aria-selected="false">{labels.Signup}</button>
                        </li>
                    </ul>
                    <div className="tab-content" id="user-connection-tabContent">
                        <div className={"tab-pane fade show " + (isRegisterPathname ? "" : "active")} id="user-connection-login-tab-pane" role="tabpanel" aria-labelledby="user-connection-login-tab" tabIndex="0">
                            {getNatbrailleBadLogin() && (
                                ErrorMessage(labels.badLogin)
                                /*
                                <div className="my-3 alert alert-warning alert-dismissible fade show" role="alert">
                                    {labels.badLogin}
                                    <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                                */
                            )}
                            <form action={((2 === connexionState) ? getLogoutEndpoint : getLoginEndpoint)()} method="POST" className="form-signin">
                                {InputUsername({isFirst:true,autocomplete:"username"})}
                                {[0, 1].includes(connexionState) && (
                                    <>
                                        {InputPassword({autocomplete:"current-password",isLast:true})}
                                        <NavLink to="/request-password-reset" className="">{labels['password-forgotten']}</NavLink>
                                    </>
                                )}
                                {[0, 1].includes(connexionState) && (
                                    InputSubmit({ disabled: ((connexionState !== 0) || !(clientValidate())), label: labels.login })
                                )}
                                {(2 === connexionState) && (
                                    InputSubmit({ label: labels.logout })
                                )}
                            </form>
                        </div>
                        <div className={"tab-pane fade show " + (isRegisterPathname ? "active" : "")} id="user-connection-register-tab-pane" role="tabpanel" aria-labelledby="user-connection-register-tab" tabIndex="0">
                            <form action={getRegisterEndpoint()} method="POST" className="form-signin">
                                {InputUsername({ isFirst:true })}
                                {InputEmail({ autoComplete: "off" })}
                                {InputPassword({ autocomplete:"new-password", isLast:true,  autoComplete: "off" })}
                                {InputSubmit({ label: labels.Signup })}
                            </form>
                        </div>
                        {/*
                    <div className={"tab-pane fade show " + (isRenewPasswordPathname ? "active" : "")} id="user-connection-renew-password-tab-pane" role="tabpanel" aria-labelledby="user-connection-renew-password-tab" tabIndex="0">
                        <form action={getRegisterEndpoint()} method="POST" className="form-signin">
                            {InputPassword({ autoComplete: "off" })}
                            {InputSubmit({ label: labels.Signup })}
                        </form>
                    </div>
                     */}
                    </div>
                </>)}
        </div >


        {
            /*           
            <div className="m-2 text-center">
                <pre>
                    {JSON.stringify({ username, password, stay: rememberMe, connexion: connexionState })}
                </pre>
         
            </div>
            */
        }
    </div>

    )
}