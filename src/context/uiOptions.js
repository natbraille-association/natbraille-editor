import { saveUIOptions } from "../localstorage/store"

export const UIOptionsVersion = 1

export const UI_OPTION_BRAILLE_FONT_SIZE = 'fontSize'
export const UI_OPTION_COLORED_BRAILLE_OUTPUT = 'tinted'
export const UI_OPTION_COLORED_BRAILLE_COLORS = 'braille-colors'
export const UI_OPTION_MATH_EDITOR_MATHML_FONT_SIZE = 'math-editor-mathml-font-size'
export const UI_OPTION_PERKINS_KEYS = "perkins-keys"
export const UI_OPTION_USE_PERKINS_INPUT_IN_BRAILLE_BLOCK = "perkins-keys-input-in-braille-block"

/*
pt1: f

pt2: d

pt3: s

pt4:j

pt5: k

pt6: l
*/
const UIOptions = [
    { name: UI_OPTION_BRAILLE_FONT_SIZE, defaultValue: "24" },
    { name: UI_OPTION_COLORED_BRAILLE_OUTPUT, defaultValue: true },
    { name: UI_OPTION_MATH_EDITOR_MATHML_FONT_SIZE, defaultValue: "30" },
    { name: UI_OPTION_PERKINS_KEYS, defaultValue: { codes: { 1: 'KeyF', 2: 'KeyD', 3: 'KeyS', 4: 'KeyJ', 5: 'KeyK', 6: 'KeyL' }, keys: {} } },
    {
        name: UI_OPTION_COLORED_BRAILLE_COLORS, defaultValue: {
            'letters': { text: '#000' },
            'digits': { text: '#049f95' },
            'punctuation': { text: '#cc8a00' },
            'list-bullet': { text: '#8141b4' },
            'math': { text: '#039952' },
            'math-prefix': { text: '#4169b4' },
            'uppercase-prefix': { text: '#8aa80c' },
        }
    },
    { name: UI_OPTION_USE_PERKINS_INPUT_IN_BRAILLE_BLOCK, defaultValue: true}

]

export const DefaultUIOptions = Object.fromEntries(UIOptions.map(({ name, defaultValue }) => [name, defaultValue]))

export const getUIOption = (uiOptions) => (name) => {
    // uiOptions is the react ui option context value
    if (uiOptions.hasOwnProperty(name)) {
        return uiOptions[name]
    } else if (DefaultUIOptions.hasOwnProperty(name)) {
        return DefaultUIOptions[name]
    } else {
        throw new Error("cannot get ui option : no ui option named", name)
    }
}
export const setUIOption = (setUIOptions, uiOptions) => (name, value) => {
    if (!DefaultUIOptions.hasOwnProperty(name)) {
        throw new Error("cannot set ui option : no ui option named", name)
    }
    // setUIOptions is the react ui option context setter
    // uiOptions is the react ui option context value
    const o = { ...uiOptions }
    o[name] = value
    setUIOptions(o)
    // save to localStorage
    saveUIOptions(o)
}
export const resetAllUIOptions = (setUIOptions) => {    
    const o = {}
    setUIOptions(o)
    saveUIOptions(o)
}