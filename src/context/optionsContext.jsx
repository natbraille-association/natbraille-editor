import { createContext, useState } from "react"
import { loadUIOptions, loadNatFilterOptions } from "../localstorage/store"

export const OptionsContext = createContext({
    options: {},
    setOptions: () => {},
    uiOptions: {},
    setUIOptions: () => {},
    brailleSource: '',
    setBrailleSource: () => {},
    noirSource: {},
    setNoirSource: () => {}
});

export const OptionsContextProvider = (props) => {
    const [options, setOptions] = useState(loadNatFilterOptions());
    const [uiOptions, setUIOptions] = useState(loadUIOptions());
    const [brailleSource, setBrailleSource] = useState('');
    const [noirSource, setNoirSource] = useState({});

    const contextValue = {
        options,
        setOptions,
        uiOptions,
        setUIOptions,
        brailleSource,
        setBrailleSource,
        noirSource,
        setNoirSource
    };

    return (
        <OptionsContext.Provider value={contextValue}>
            {props.children}
        </OptionsContext.Provider>
    );
}

const useOptionsContext = () => useContext(OptionsContext);

