import filterOptionsMetadata from "../formats/filterOptionsMetadata.json"
import { saveNatFilterOptions } from "../localstorage/store"

/*
 * gets Option value as string, or default value if undefined
 */
export const getNatFilterOptionValue = options => name => {
    if (!(filterOptionsMetadata.hasOwnProperty(name))){    
        throw new Error("cannot get nat filter option : no nat filter option named", name)
    }
    if (options[name] !== undefined) {
        return options[name]
    } else {
        return filterOptionsMetadata[name].defaultValue
    }
}
export const getNatFilterOptionDefaultValue = name => {
    if (!(filterOptionsMetadata.hasOwnProperty(name))){
        throw new Error("cannot get nat filter default option value : no nat filter option named", name)
    }
    return filterOptionsMetadata[name].defaultValue
}

//
// todo : context has setOptions which should be called setNatFilterOptions
// so we should either prefix context setOptions (ctxSetOptions) or just not
// destructure useContecxt(OptionContext) i.e. optionContext.setNatFilterOptions
// to avoid name clashes.
//
// same is true for ui options, because ui options are changed one at a time, so
// ui setter is called setUiOption, and context is called setUiOptions but there is no
// rationnale to that naming.
// 
export const setNatFilterOptions = (setOptions, options) => (names_values) => {

    const o = {...options};
    Object.entries(names_values).forEach( ([name,value]) => {
        if (!(filterOptionsMetadata.hasOwnProperty(name))){    
            throw new Error("cannot set nat filter option : no nat filter option named", name)
        }
        o[name] = value

        console.log('seti seta',name,value)
    })
    // set the is the react ui option context setter
    setOptions(o)
    
    // save in localstorage
    saveNatFilterOptions(o)
    
    
}