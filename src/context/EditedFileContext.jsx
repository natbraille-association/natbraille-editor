//
// A context to keep the current edited file.
// Is does not follow the same approach as the Options Context (for no particular reason)
// this time, we put individual setters in the context rather than having independant 
// functions to which the "set" (here setEditedFile) would be passed.
//

import { createContext, useContext, useEffect, useState } from "react";
import { loadEditedFile, saveEditedFile } from "../localstorage/store";

const EditedFileContext = createContext();

// Define the provider component
export const EditedFileProvider = ({ children }) => {

    // Load the initial state from localStorage or use default values
    const [editedFile, setEditedFile] = useState(() => loadEditedFile({
        filename: null,
        lastEdited: null,
        lastSaved: null,
        type: null,
        content: null,
    }));

    useEffect(() => {
        if (editedFile.filename) {
            document.title = `Natbraille - ${editedFile.filename}`;
        } else {
            document.title = `Natbraille`;
        }
    }, [editedFile.filename]);


    // Helper function to update the editedFile state and save it to localStorage
    const updateEditedFile = (newState) => {
        setEditedFile((prevState) => {
            const updatedState = { ...prevState, ...newState };
            saveEditedFile(updatedState);  // Save updated state to localStorage
            return updatedState;
        });
    };

    // Define setter functions for each field of the editedFile object
    const setEditedFileContextFilename = (filename) => { updateEditedFile({ filename }) };
    const setEditedFileContextLastEdited = (lastEdited) => updateEditedFile({ lastEdited });
    const setEditedFileContextLastSaved = (lastSaved) => updateEditedFile({ lastSaved });
    const setEditedFileContextType = (type) => updateEditedFile({ type });
    const setEditedFileContextContent = (content) => updateEditedFile({ content });

    return (
        <EditedFileContext.Provider
            value={{
                editedFile,
                setEditedFileContextFilename,
                setEditedFileContextLastEdited,
                setEditedFileContextLastSaved,
                setEditedFileContextType,
                setEditedFileContextContent
            }}
        >
            {children}
        </EditedFileContext.Provider>
    );
};


export const useEditedFileContext = () => useContext(EditedFileContext);

