export const saveBlob = (function () {
    const a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    return function (blob, fileName) {
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);
    };
}());

export const savePlainText = (text, name) => {
    const blob = new Blob([text], { type: "text/plain" });
    saveBlob(blob, name)
}
export const saveXML = (text, name) => {
    const blob = new Blob([text], { type: "text/xml" });
    saveBlob(blob, name)
}
export const saveZip = (bytes, name) => {
    const blob = new Blob([bytes], { type: "application/zip" });
    saveBlob(blob, name)
}
export const saveOdt = (bytes, name) => {
    const blob = new Blob([bytes], { type: "application/vnd.oasis.opendocument.text" });
    saveBlob(blob, name)
}