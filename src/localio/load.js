export const loadPlainText = (file, encoding) => new Promise((accept, reject) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => accept(reader.result), false)
    reader.readAsText(file, encoding)
})
export const loadArrayBuffer = (file, encoding) => new Promise((accept, reject) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => accept(reader.result), false)
    reader.readAsArrayBuffer(file)
})

// also modern Blob method .text(), only with UTF-8
// const text = await file.text()
