/**
 * "shamelessly produced by ChatGPT 3.5".
 *  write a react 18.2 function named useModalPrompt. This react 18.2 function returns a modalPrompt async function. This modalPrompt function has a title, a label, and a default value and  displays a bootstrap 5.3 modal window with corresponding title, a label, input field with default value, and cancel and ok buttons. When the cancel button is pressed on the modal window, the modal window disapears and modalPrompt is resolved with the value undefined. When the ok button is pressed on the modal window, the modal window disappears and modalPrompt is resolved with the value of the modal window input field.
 */

// The useModalPrompt hook provides a way to display a modal prompt with a title, label, and default value, 
// allowing users to input a value. It returns a modalPrompt function to trigger the modal and a 
// ModalComponent to render the modal UI.

//
// usage : 
//
// const { modalPrompt, ModalComponent } = useModalPrompt();
// 
// ...
//
// const handleClick = async () => {
//     const result = await modalPrompt('Enter Name', 'Name:', 'John');
//     console.log('Modal result:', result);
// };
//
// ...
//
// return <div className="mx-2 my-2">
//     <div className="App">
//         <button onClick={handleClick}>Open Modal</button>
//         <ModalComponent />
//     </div>
// 

import React, { useState, useEffect } from 'react';

function useModalPrompt() {
    const [showModal, setShowModal] = useState(false);
    const [title, setTitle] = useState('');
    const [label, setLabel] = useState('');
    const [defaultValue, setDefaultValue] = useState('');
    const [inputValue, setInputValue] = useState('');
    const [resolveFunc, setResolveFunc] = useState(null);

    const modalPrompt = async (title, label, defaultValue) => {
        setTitle(title);
        setLabel(label);
        setDefaultValue(defaultValue);
        setShowModal(true);

        return new Promise((resolve) => {
            setResolveFunc(() => resolve);
        });
    };

    const handleCancel = () => {
        setShowModal(false);
        setInputValue('');
        if (resolveFunc) {
            resolveFunc(undefined);
            setResolveFunc(null);
        }
    };

    const handleOk = () => {
        setShowModal(false);
        if (resolveFunc) {
            resolveFunc(inputValue);
            setInputValue('');
            setResolveFunc(null);
        }
    };

    useEffect(() => {
        if (showModal) {
            setInputValue(defaultValue);
        }
    }, [showModal, defaultValue]);

    return {
        modalPrompt,
        ModalComponent: () => (
            showModal && (
                <div>
                    <div className="modal-backdrop show"></div>
                    <div className="modal" tabIndex="-1" style={{ display: 'block' }}>
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">{title}</h5>
                                    <button type="button" className="btn-close" onClick={handleCancel}></button>
                                </div>
                                <div className="modal-body">
                                    <label htmlFor="modalInput" className="form-label">{label}</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="modalInput"
                                        value={inputValue}
                                        onChange={(e) => setInputValue(e.target.value)}
                                    />
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" onClick={handleCancel}>Cancel</button>
                                    <button type="button" className="btn btn-primary" onClick={handleOk}>Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        ),
    };
}

export default useModalPrompt;
