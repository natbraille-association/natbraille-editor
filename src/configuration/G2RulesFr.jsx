import { useEffect, useId, useState } from "react"
import { SelectOption } from "./BasicComponents"
import { parseFrG2Rules, exportFrG2RulesXml } from "../formats/fr-g2-rules.js"
import { saveXML } from "../localio/save"
import { loadPlainText } from "../localio/load"
import { getUserDocumentKindList, postUserDocument } from "../serverio/userDocument"
const fakeNatbrailleTranslates = source => new Promise((accept, reject) => {
    setTimeout(() => {
        accept({ braille: "⠏⠁⠎⠀⠧⠗⠁⠊⠍⠑⠝⠞⠂⠀⠓⠑⠊⠝" + source })
    }, 200 + Math.random() * 500)
})

const rulesFilters = [
    "Toutes",
    "Locutions",
    "Signes",
    "Symboles",
    "Règles sur les signes",
    "Règles sur les symboles",
    "Règles génériques",
    "Règles du cas général"
]
const rulesFilterOptions = rulesFilters.map((label, idx) => ({ value: idx, label }));
export const RuleList = ({ parsedRules, setData }) => {
    const [rulesFilterIdx, setRulesFilterIdx] = useState(0)
    const ruleFilterChanged = value => {
        setRulesFilterIdx(parseInt(value, 10))
    }
    const ruleLabel = ({ ruleType, actif, ruleFor, desc, ref, pluriel, invariant, composable, noir, braille }) => [
        ((noir && braille) && (`${noir} est transcrit par ${braille}`)),
        desc,
        (pluriel && " ; pluriel possible"),
        (invariant && " (pluriel possible)")
    ].filter(x => x).join(' ')

    const ruleChecked = ({ actif }) => actif === 'true'
    const ruleMatchesFilter = ({ ruleType, ruleFor }) => {
        return (rulesFilterIdx === 0)
            || ((rulesFilterIdx === 1) && (ruleType === 'locution'))
            || ((rulesFilterIdx === 2) && (ruleType === 'signe'))
            || ((rulesFilterIdx === 3) && (ruleType === 'symbole'))
            || ((rulesFilterIdx === 4) && (ruleType === 'rule') && (ruleFor === 'signe'))
            || ((rulesFilterIdx === 5) && (ruleType === 'rule') && (ruleFor === 'symbole'))
            || ((rulesFilterIdx === 6) && (ruleType === 'rule') && (ruleFor === 'all'))
            || ((rulesFilterIdx === 7) && (ruleType === 'rule') && (ruleFor === 'general'))
    }
    const changeChecked = rule => ({ target }) => {
        const checked = target.checked
        rule.actif = checked ? "true" : "false";
        setData([...parsedRules])
    }
    return <>
        <SelectOption label="Règles affichées" value={rulesFilterIdx} options={rulesFilterOptions} handleChange={ruleFilterChanged} />
        <div className="mb-2 overflow-y-scroll" style={{ 'maxHeight': '50vh' }}  >
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Règle</th><th>Actif</th>
                    </tr>
                </thead>
                <tbody>
                    {parsedRules.filter(ruleMatchesFilter).map((rule, idx) => (
                        <tr key={idx}>
                            <td>{ruleLabel(rule)}</td>
                            { /* <td><input className="form-check-input" type="checkbox" defaultChecked={ruleChecked(rule)} /></td> */}
                            <td><input className="form-check-input" type="checkbox" onChange={changeChecked(rule)} checked={ruleChecked(rule)} /></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    </>
}

const ServerUploadModal = () => {

    const [frG2RuleFiles, setFrG2RuleFiles] = useState([])
    const [selectedFile, setSelectedFile] = useState('')
    const [filename, setFilename] = useState('')
    const idSelectOption = useId()
    const idInputFilename = useId()
    const fetchList = async () => {
        console.log('fetch rules')
        const list = await getUserDocumentKindList("vivien", "fr-g2-rules")
        console.log('rules list fetched', list)
        setFrG2RuleFiles(list);
        setSelectedFile(filename)
    };
    useEffect(() => {
        fetchList();
    }, []);


    const labels = {
        title: "Sauver le fichier de règle sur le serveur",
        name: "Nom",
        save: "Enregistrer",
        replace: "Remplacer",
        close: "Fermer",
        ls: "Fichiers de règles"
    }
    const upload = async () => {
        const done = await postUserDocument("vivien", "fr-g2-rules", filename)
        fetchList();

    }
    const filenameEmpty = () => (filename.length === 0)
    const filenameExists = () => frG2RuleFiles.includes(filename)
    const filenameChanged = ({ target }) => {

        console.log('value', target.value)
        setFilename(target.value)
        const existsIndex = frG2RuleFiles.findIndex(v => v === target.value)
        if (existsIndex !== -1) {
            setSelectedFile(target.value)
            console.log(existsIndex)
        }

    }
    const selectedChanged = ({ target }) => {
        setSelectedFile(target.value)
        setFilename(target.value)
    }
    return (
        <div className="modal fade" id={"server-upload-modal-g2-rules"} tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="exampleModalLabel">{labels.title}</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body m-2">
                        <label className="mb-2" htmlFor={idSelectOption}>{labels.ls}</label>
                        <select value={selectedFile} onChange={selectedChanged} size={Math.max(3, Math.min(frG2RuleFiles.length, 5))} className="form-select" id={idSelectOption}>
                            {frG2RuleFiles.map(filename => (<option value={filename} key={filename}>{filename}</option>))}
                        </select>
                        <div className="input-group mt-3">
                            <label className="input-group-text" htmlFor={idInputFilename}>{labels.name}</label>
                            <input value={filename} onChange={filenameChanged} type="text" className="form-control" id={idInputFilename} aria-describedby="rule filename" />
                        </div>
                    </div>

                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" >{labels.close}</button>
                        <button disabled={filenameEmpty() || !filenameExists()} type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={upload}>{labels.replace}</button>
                        <button disabled={filenameEmpty() || filenameExists()} type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={upload}>{labels.save}</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
export const G2RulesFr = () => {

    const sourceInputId = useId()
    const transInputId = useId()
    const [sourceWord, setSourceWord] = useState('')
    const [isTranslating, setIsTranslating] = useState(false)
    const [transWord, setTransWord] = useState('')

    const [data, setData] = useState([])

    useEffect(() => {
        const dataFetch = async () => {
            console.log('fetch rules')
            const data = await (
                await fetch("assets/abrege1-12.xml")
            ).text();
            console.log('rules fetched')
            setData(parseFrG2Rules(data));
        };
        dataFetch();
    }, []);



    const wordChanged = async ({ target }) => {
        const noir = target.value
        setSourceWord(noir)
    }
    const testWord = async word => {
        setIsTranslating(true)
        const { braille } = await fakeNatbrailleTranslates(sourceWord)
        setIsTranslating(false)
        setTransWord(braille)
    }

    const download = () => {
        const xmlString = exportFrG2RulesXml(data)
        saveXML(xmlString, "fr-g2-rules-saved.xml")
    }

    const [importedXMLText, setImportedXMLText] = useState('')
    const uploadChange = async ({ target }) => {
        //updateImportPreview('')
        const curFiles = target.files
        for (const file of curFiles) {
            const text = await loadPlainText(file)
            setImportedXMLText(text)
        }
    }
    const upload = () => {
        setData(parseFrG2Rules(importedXMLText));
    }
    return <div className="m-1">
        <div className="m-5">
            <label className="form-label" labelfor={sourceInputId}>Mot en noir</label>
            <div className="input-group" >
                <input id={sourceInputId} disabled={isTranslating} className="form-control" onChange={wordChanged} placeholder="mot en noir" value={sourceWord} />
                <button disabled={isTranslating} type="button" className="btn btn-primary" onClick={testWord}>Tester</button>
            </div>
            <div className="mt-2">
                <label className="form-label" labelfor={transInputId}>Mot en braille</label>
                <input id={transInputId} disabled={true} className="form-control braille-text" placeholder="mot en braille" value={transWord} />
            </div>

            <div className="mt-4">
                <h3>Règles utilisées</h3>
                <RuleList parsedRules={data} setData={setData} />
            </div>

            <div className="mt-4">
                <div className="btn-toolbar" role="toolbar" aria-label="Buttons toolbar">
                    <button type="button" className="btn btn-primary bi bi-upload" data-bs-toggle="modal" data-bs-target={"#import-modal-g2-rules"}> Importer un fichier de règles...</button>
                    <button type="button" className="btn btn-primary ms-2 bi bi-download" onClick={download}> Télécharger...</button>
                    <button type="button" className="btn btn-primary ms-2 bi bi-cloud-upload" data-bs-toggle="modal" data-bs-target={"#server-upload-modal-g2-rules"}> Sauver sur le serveur...</button>
                </div>
                {/* import */}
                <div className="modal fade" id={"import-modal-g2-rules"} tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h1 className="modal-title fs-5" id="exampleModalLabel">Importer une liste de mots</h1>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <div className="m-3">
                                    <input type="file" className="form-control" onChange={uploadChange} />
                                    {/*<SelectOption options={WhatWGEncodingsOptions} value={importEncoding} handleChange={uploadEncodingChange} />*/}
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" >Close</button>
                                <button disabled={importedXMLText?.length === 0} type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={upload}>Import</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* server upload */}
                <ServerUploadModal modalId={'server-upload-modal-g2-rules'} />
            </div>
        </div>
    </div>
}
