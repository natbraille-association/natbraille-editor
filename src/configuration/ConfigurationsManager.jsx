import './ConfigurationsManager.scss'
import { useContext, useEffect, useState } from "react";
import { OptionsContext } from "../context/optionsContext"
import {
    getUserConfigurationList,
    getUserConfiguration,
    postUserConfiguration,
    deleteUserConfiguration
} from "../serverio/userDocument"

const Labels = {
    'configurations-manager': 'Gestion des configurations de transcription',
    'configuration-list-header': 'Liste des configurations',
    'configuration-summary-header': 'Résumé',
    'configuration-save-header': 'Enregistrement',
    'confirm-replace-configuration': 'Cette configuration existe déjà. Voulez-vous la remplacer ?',
    'show-configuration': 'résumé',
    'delete-configuration': 'supprimer',
    'rename-configuration': 'renommer',
    'confirm-delete-configuration': 'Voulez-vous vraiment supprimer cette configuration ?',
    'no-configurations': 'Aucune configuration sauvegardée',
    'no-summary': '-',
    'use-saved-configuration': 'charger',
    'confirm-use-saved-configuration': 'Voulez-vous replacer la configuration courante ?',
    'ask-configuration-name': 'Enregistrer la configuration sous le nom :',
    'save-current-configuration': 'Enregistrer la configuration courante',
    'ask-configuration-rename': 'Renommer la configuration sous le nom :',

    'configuration-new-header':'Créer une configuration vierge',
    'new-configuration':'nouvelle configuration',
    'confirm-configuration-new':'Voulez-vous replacer la configuration courante par une configuration vierge ?'
}

const makeDefaultConfigurationName = () => {
    const date = new Date()

    const pad = (i) => (i < 10) ? "0" + i : "" + i;
    const fmt = [[
        pad(date.getDate()),
        pad(1 + date.getMonth()),
        pad(date.getFullYear()),
    ].join('/'),
    [
        pad(date.getHours()),
        pad(date.getMinutes()),
        pad(date.getSeconds()),
    ].join(':')
    ].join(' ')
    return `options - ${fmt}`
}



import useModalPrompt from './useModalPrompt';

export const ConfigurationsManager = () => {

    const { options, setOptions } = useContext(OptionsContext)

    const [configurations, setConfigurations] = useState([])
    const [tmpOptions, setTmpOptions] = useState(undefined)

    const loadConfigurations = async () => {
        try {
            const cs = await getUserConfigurationList()
            setConfigurations(cs)
            setTmpOptions(undefined)
        } catch (e) {
            console.error(e)
        }
    }
    const showConfiguration = userConfiguration => async () => {
        try {
            const c = await getUserConfiguration(userConfiguration)
            setTmpOptions(c)
        } catch (e) {
            console.error(e)
        }
    }
    const useSavedConfiguration = userConfiguration => async () => {
        const confirmUseSaved = window.confirm(Labels['confirm-use-saved-configuration'])
        if (!confirmUseSaved) {
            return;
        }
        try {
            const c = await getUserConfiguration(userConfiguration)
            setOptions(c)
            setTmpOptions(undefined)
        } catch (e) {
            console.error(e)
        }
    }
    const saveConfiguration = async () => {
        const newConfigurationName = prompt(Labels['ask-configuration-name'], makeDefaultConfigurationName());
        const exists = configurations.some(configuration => configuration === newConfigurationName);
        if (exists) {
            const confirmReplace = window.confirm(Labels['confirm-replace-configuration'])
            if (!confirmReplace) {
                return;
            }
        }
        try {
            options['CONFIG_NAME'] = newConfigurationName
            await postUserConfiguration(newConfigurationName, options || {})
            await loadConfigurations()
        } catch (e) {
            console.error(e)
        }
    }
    const deleteConfiguration = userConfiguration => async () => {
        const confirmDelete = window.confirm(Labels['confirm-delete-configuration'])
        if (!confirmDelete) {
            return;
        }
        try {
            await deleteUserConfiguration(userConfiguration)
            await loadConfigurations()
        } catch (e) {
            console.error(e)
        }
    }
    const renameConfiguration = userConfiguration => async () => {
        const newConfigurationName = prompt(Labels['ask-configuration-rename'], userConfiguration);
        if ((newConfigurationName === null) || (newConfigurationName === '')){
            return
        }
        const os = await getUserConfiguration(userConfiguration)
        options['CONFIG_NAME'] = newConfigurationName
        await postUserConfiguration(newConfigurationName, os)
        await deleteUserConfiguration(userConfiguration)
        await loadConfigurations()
    }
    useEffect(() => {
        loadConfigurations()
    }, [])

    const newConfiguration = async () => {
        const newConfigurationConfirm =window.confirm(Labels['confirm-configuration-new'])
        if (!newConfigurationConfirm) {
            return;
        }
        setOptions({})
    }
    // setNatFilterOptions(setOptions, options)(newValues)
    const configList = <>
        {
            <table className="table table-striped configurations-list">
                <thead>
                    <tr><td></td><td></td><td></td><td></td></tr>
                </thead>
                <tbody>
                    {
                        configurations.map((userConfiguration, i) => {
                            return <tr key={i}>
                                <td className="configuration-name">{userConfiguration}</td>
                                <td><button className="btn btn-sm btn-primary" onClick={useSavedConfiguration(userConfiguration)}>
                                    {/*<i class="bi bi-download"></i> */}{Labels['use-saved-configuration']}
                                </button></td>
                                <td><button className="btn btn-sm btn-primary" onClick={showConfiguration(userConfiguration)}>
                                    {/*<i class="bi bi-search"></i> */}{Labels['show-configuration']}
                                </button></td>
                                <td><button className="btn btn-sm btn-primary" onClick={deleteConfiguration(userConfiguration)}>
                                    {/*<i class="bi bi-eraser"></i> */}{Labels['delete-configuration']}
                                </button></td>
                                <td><button className="btn btn-sm btn-primary" onClick={renameConfiguration(userConfiguration)}>
                                    {/*<i class="bi bi-search"></i> */}{Labels['rename-configuration']}
                                </button></td>
                            </tr>
                        })
                    }
                </tbody>
            </table>
        }
    </>;
    const emptyConfigList = <p>{Labels['no-configurations']}</p>

    const emptySummary = <p>{Labels['no-summary']}</p>
    const summary = <div>
        <ul>
            {
                tmpOptions && Object.entries(tmpOptions).map(([k, v]) => <li key={k}><span>{k}</span> : <span>{v}</span></li>)
            }
        </ul>
        {/*JSON.stringify(tmpOptions)*/}
    </div>

    const { modalPrompt, ModalComponent } = useModalPrompt();

    const handleClick = async () => {
        const result = await modalPrompt('Enter Name', 'Name:', 'John');
        console.log('Modal result:', result);
    };

    return <div className="mx-2 my-2">
        {/*
        <div className="App">
            <button onClick={handleClick}>Open Modal</button>
            <ModalComponent />
        </div>
        */}
        {/*<h3>{Labels['configurations-manager']}</h3>*/}
        <div className="my-4">
            <h4>{Labels['configuration-new-header']}</h4>
            <button className="btn btn-primary" onClick={newConfiguration}>{Labels['new-configuration']}</button>
        </div>
        <div className="my-4">
            <h4>{Labels['configuration-save-header']}</h4>
            <button className="btn btn-primary" onClick={saveConfiguration}>{Labels['save-current-configuration']}</button>
        </div>
        <div className="my-4">
            <h4>{Labels['configuration-list-header']}</h4>
            {(configurations && configurations.length) ? configList : emptyConfigList}
        </div>
        <div className="my-4">
            <h4>{Labels['configuration-summary-header']}</h4>
            {(tmpOptions) ? summary : emptySummary}
        </div>

    </div>
}