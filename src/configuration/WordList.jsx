const exemple_list = `aïeul true false
aïeule true true
aïeules true false
aïeuls false true
aïeux false false
allô true false
batz true false
bê true false
binz true false
bisaïeul true false
bisaïeule true false
bisaïeules true false
bisaïeuls true false
crû true false
fritz true false
führer true false`

import { parseWordListText, exportWordListText } from '../formats/wordlist.js'
import { savePlainText } from '../localio/save.js'
import { loadPlainText } from '../localio/load.js'
import { SelectOption } from './BasicComponents.jsx'
import { useState } from "react"

import { WhatWGEncodingsNames } from '../formats/whatwg-encodings.js'
const WhatWGEncodingsOptions = WhatWGEncodingsNames.map(name => ({ value: name, label: name }))

export const WordList = () => {

    const [wordList, updateWordList] = useState(parseWordListText(exemple_list))

    const onChange = (wordIdx, alwaysOrAsk) => ({ target }) => {
        const value = target.checked
        console.log('changed', wordIdx, alwaysOrAsk, value)
        wordList[wordIdx][alwaysOrAsk] = value
        updateWordList([...wordList])
    }
    const $worldLine = ({ word, always, ask }, wordIdx) => {
        return (<tr key={wordIdx}>
            <td>{word}</td>
            <td><input checked={always} onChange={onChange(wordIdx, 'always')} className="form-check-input" type="checkbox" /></td>
            <td><input checked={ask} onChange={onChange(wordIdx, 'ask')} className="form-check-input" type="checkbox" /></td>
        </tr>)
    }
    const download = () => savePlainText(exportWordListText(wordList), "word-list.txt")

    const [importPreview, updateImportPreview] = useState('')
    const [importEncoding, updateImportEncoding] = useState('UTF-8')

    const closeImport = () => {
        updateImportPreview('')
    }
    const doImport = () => {
        const newWordList = parseWordListText(importPreview)
        updateWordList(newWordList)
        updateImportPreview('')
    }
    const uploadChange = async ({ target }) => {
        updateImportPreview('')
        const curFiles = target.files
        for (const file of curFiles) {
            const text = await loadPlainText(file, importEncoding)
            updateImportPreview(text)
        }
    }
    const uploadEncodingChange = (value) => {
        console.log('changed', value)
        updateImportEncoding(value)
    }
    return (
        <div>

            <div className="ms-2 me-2">
                <h2>Conserver des mots en intégral</h2>
                <div className="mb-2 overflow-y-scroll" style={{ 'maxHeight': '50vh' }}  >
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>mot</th>
                                <th>toujours</th>
                                <th>demander</th>
                            </tr>
                        </thead>
                        <tbody>
                            {wordList.map((wordLine, wordIdx) => $worldLine(wordLine, wordIdx))}
                        </tbody>
                    </table>
                </div>
                <div className="btn-toolbar" role="toolbar" aria-label="Buttons toolbar">
                    <button type="button" className="btn btn-primary bi bi-upload" data-bs-toggle="modal" data-bs-target={"#import-modal-wordlist"}> Importer une liste de mots...</button>
                    <button type="button" className="btn btn-primary ms-2 bi bi-download" onClick={download}> Télécharger...</button>
                    <button type="button" className="btn btn-primary ms-2 bi bi-cloud-upload" data-bs-toggle="modal" data-bs-target={"#save-modal-wordlist"}> Enregistrer la liste actuelle...</button>
                </div>
            </div>

            {/* save to server */}
            <div className="modal fade" id={"save-modal-wordlist"} tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="exampleModalLabel">Modal title</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            ...
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

            {/* import */}
            <div className="modal fade" id={"import-modal-wordlist"} tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="exampleModalLabel">Importer une liste de mots</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <div className="m-3">
                                <input type="file" className="form-control" onChange={uploadChange} />
                                <SelectOption options={WhatWGEncodingsOptions} value={importEncoding} handleChange={uploadEncodingChange} />
                                <div className="m-3">
                                    <pre className="overflow-y-scroll" style={{ 'maxHeight': '20vh' }}  >
                                        {importPreview}
                                    </pre>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={closeImport}>Close</button>
                            <button disabled={importPreview.length === 0} type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={doImport}>Import</button>
                        </div>
                    </div>
                </div>
            </div>

            <pre>
                {JSON.stringify(wordList, null, 2)}
            </pre>
            <pre>
                {exportWordListText(wordList)}
            </pre>
        </div>
    )
}

