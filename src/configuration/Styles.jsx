const stylist_default_xml = `<?xml version="1.1" encoding="UTF-8"?>

<!-- Fichier de styles par défaut pour NatBraille
vous pouvez l'éditer mais "à vos risques et périls" -->

<styles>
     <style name="standard" mode="3-1" />
     <style name="integral" special="g1" mode="3-1" />
     <style name="poesie" mode="1-3" />
     <style name="noteTr" mode="7-5" prefixe="p6p23" suffixe="p56p3"/>
     <style name="noIndent" mode="1" />
     <style name="heading1" mode="title1" />
     <style name="heading" mode="title" />
     <style name="chemistry" special="chemistry" />
     <style name="chimie" special="chimie" />
     <style name="echap" special="g0" />
     <style name="envers" mode="1" upsidedown="yes" />
</styles>`

import { parseStylistXml, exportSylistXml, stylistAttributeNames } from '../formats/stylist.js'
import { useState } from 'react'
export function test() {
    const styles = parseStylistXml(stylist_default_xml)
    console.log('read', styles)
    const xml = exportSylistXml(styles)
    console.log('wrote', xml)

}

export const Stylist = () => {
    const [styles, updateStyles] = useState(parseStylistXml(stylist_default_xml))

    const valueChanged = (rowIdx, colIdx) => ({ target }) => {
        const value = target.value
        console.log(valueChanged, { rowIdx, colIdx, value, target })
        styles[rowIdx][stylistAttributeNames[colIdx]] = value
        updateStyles([...styles])
        console.log(styles)
    }
    const styleRemoved = (rowIdx) => () => {
        console.log('remove rowIdx', rowIdx)
        styles.splice(rowIdx, 1)
        updateStyles([...styles])
    }
    const styleAdded = () => {
        console.log('add row')
        updateStyles([
            ...styles,
            Object.fromEntries(stylistAttributeNames.map(aName => [aName, '']))
        ])
        //styles.splice(rowIdx, 1)
        //updateStyles([...styles])
    }
    const $style = (style, rowIdx) => <tr key={rowIdx}>{
        stylistAttributeNames.map((aName, colIdx) => {
            const value = style[aName]
            return <td key={colIdx}>
                <input className="form-control" onChange={valueChanged(rowIdx, colIdx)} value={value} aria-label={aName} />
            </td>
        })
    }<td><button type="button" className="bi bi-trash btn btn-primary btn-sm" aria-label="supprimer" onClick={styleRemoved(rowIdx)} /></td>
    </tr>

    return <div>
        <div className="mb-5">
            <h3>Styles personnalisés</h3>
        </div>
        <div className="mb-5">
            <table className="table table-striped">
                <thead>
                    <tr>
                        {stylistAttributeNames.map((aName, idx) => <th key={idx}>{aName}</th>)}
                        <th>supprimer</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        styles.map((style, idx) => $style(style, idx))
                    }
                </tbody>
            </table>
        </div>
        <div className="mb-5">
            <button type="button" className="btn btn-primary btn-sm bi bi-plus-circle" onClick={styleAdded}> Ajouter un nouveau style</button>
        </div>
        <div className="mb-5">
            <pre>
                {
                    exportSylistXml(styles)
                }
            </pre>
        </div>
    </div>




}
