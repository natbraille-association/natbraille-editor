import { useId } from "react"

const parseBoolean = s => JSON.parse(s)

export const CheckBox = ({ label, value, handleChange }) => {
    const id = useId()
    const onChange = ({ target }) => handleChange(target.checked)
    return (
        <div className="form-check">
            <input checked={parseBoolean(value)} onChange={onChange} className="form-check-input" type="checkbox" id={id} />
            <label className="form-check-label" htmlFor={id}>
                {label}
            </label>
        </div>
    )
}

export const Switch = ({ label, value, handleChange, disabled = false }) => {
    const id = useId()
    const onChange = ({ target }) => handleChange(target.checked)
    return (
        <div className="form-check form-switch">
            <input disabled={disabled} checked={parseBoolean(value)} onChange={onChange} className="form-check-input" type="checkbox" id={id} />
            <label className="form-check-label" htmlFor={id}>
                {label}
            </label>
        </div>
    )
}

export const SelectOption = ({ label, ariaLabel, value, options, handleChange }) => {
    const id = useId()
    const onChange = ({ target }) => handleChange(target.value)
    return (
        <div className="mb-3">
            <label className="form-label" htmlFor={id}>
                {label}
            </label>
            <select onChange={onChange} value={value} className="form-select" aria-label={ariaLabel} id={id} >
                {options.map(({ value, label }) => <option key={value} value={value}>{label}</option>)}
            </select>
        </div>
    )

}

export const NumberSpindle = ({ label, value, min = 0, max, handleChange, disabled = false }) => {
    const id = useId()
    const onChange = ({ target }) => handleChange(target.value)
    const haveLabel = ((label !== undefined) && (label.length > 0))
    const $label = haveLabel ? (<span className="input-group-text" htmlFor={id}>{label}</span >) : ('');
    const $input = <input style={{ maxWidth: '5em' }} aria-label={label} disabled={disabled} value={parseInt(value)} min={min} max={max} onChange={onChange} className="form-control" type="number" id={id} />
    return haveLabel ? (
        <div className="input-group my-2"  >
            {$label}
            {$input}
        </div>
    ) : $input;

}
export const SimpleText = ({ label, value, placeholder = "", handleChange, disabled = false }) => {
    const id = useId()
    const onChange = ({ target }) => handleChange(target.value)
    return <div className="form-floating mb-3">
        <input  disabled={disabled}  className="form-control" id={id} onChange={onChange}  placeholder={placeholder} />
        <label htmlFor={id}>{label}</label>
    </div>
}