import "./Options.scss"
import { useState, useEffect, useId, useContext } from "react"
import { CheckBox, NumberSpindle, Switch, SelectOption, SimpleText } from "./BasicComponents.jsx"
import filterOptionsMetadata from "../formats/filterOptionsMetadata.json"


const GenericOption = ({ handleChange, optionName, optionMetadata, value }) => {
    const valueIsUndefined = (value === undefined)
    const valueIsDefault = valueIsUndefined || (value === optionMetadata.defaultValue)
    const setOrDefaultValue = valueIsUndefined ? (optionMetadata.defaultValue) : value
    const restoreValue = <button className="btn btn-primary" disabled={valueIsDefault} onClick={() => handleChange(optionMetadata.defaultValue)}>restore default : {optionMetadata.defaultValue} </button>

    switch (optionMetadata.type) {
        //case "String": return <div>{optionName}</div>
        case "String": {
            return <div className="mb-5">
                <h4>{optionName}</h4>
                <SimpleText label={optionMetadata.comment} handleChange={handleChange} value={setOrDefaultValue} />
                {restoreValue}
            </div>
        }
        case "Number": {
            return <div className="mb-5">
                <h4>{optionName}</h4>
                <NumberSpindle label={optionMetadata.comment} handleChange={handleChange} min={0} value={setOrDefaultValue} />
                {restoreValue}
            </div>
        }
        case "Boolean": {
            return <div className="mb-5">
                <h4>{optionName}</h4>
                <Switch handleChange={handleChange} label={optionMetadata.comment} value={setOrDefaultValue} />
                {restoreValue}
            </div>
        }
        case "Url": return <div>{optionName} = {setOrDefaultValue}</div>
        default: throw new Error(`option ${optionName} type is unknown`)

    }

}
const pageNumberingChoices = [
    // LAYOUT_NUMBERING_MODE
    { value: "'nn'", label: "Pas de numérotation" },
    { value: "'bs'", label: "En bas de la page, sur une ligne vide" },
    { value: "'hs'", label: "En haut de la page, sur une ligne vide" },
    { value: "'bb'", label: "En bas de la page, après le texte " },
    { value: "'hb'", label: "En haut de la page, après le texte" },

]

const emptyLineOptionsGroup = ["LAYOUT_PAGE_EMPTY_LINES_MODE", "LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1", "LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2", "LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3"]
const pageEmptyLineModeChoices = [
    // LAYOUT_PAGE_EMPTY_LINES_MODE
    { value: '0', label: 'Comme dans le document d\'origine' },
    { value: '2', label: 'Pas de lignes vides' },
    { value: '3', label: 'Conforme à la norme Braille' },
    { value: '4', label: 'Conforme à la norme Braille "aérée"' },
    { value: '5', label: 'Interlignage double' },
    { value: '1', label: 'Personnalisé' }, //LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1 LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2 LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3
]

const brailleTableChoices = filterOptionsMetadata["FORMAT_OUTPUT_BRAILLE_TABLE"].possibleValues
    .map(brailleTable => ({ label: brailleTable, value: brailleTable }))


const NiveauxDeTitres = ({ optionString, stringChanged }) => {
    const existingLevels = Array(8).fill(0).map((_, i) => (1 + i))
    const minLevel = Math.min(...existingLevels)
    const maxLevel = Math.max(...existingLevels)
    const levelBrailles = optionString.split(",").map(x => parseInt(x, 10))
    const handleChange = levelNoir => (...v) => {
        // generate comma separated header levels numbers
        levelBrailles[levelNoir - 1] = v.toString(10)
        stringChanged(levelBrailles.join(","))
    }

    /*
    const levels = (levelNoir) => <div className="row text-start align-items-start" key={levelNoir}>
        <div className="col">Un titre de niveau {levelNoir} en Noir donne un titre Braille de niveau</div>
        <div className="col"><NumberSpindle handleChange={handleChange(levelNoir)} min={minLevel} max={maxLevel} value={levelBrailles[levelNoir - 1]} />
        </div>
    </div>
    */
    const levels = (levelNoir) => <tr className="text-start align-items-start" key={levelNoir}>
        <td >Titre {levelNoir}</td>
        <td ><NumberSpindle handleChange={handleChange(levelNoir)} min={minLevel} max={maxLevel} value={levelBrailles[levelNoir - 1]} />
        </td>
    </tr>
    return <table className="table table-striped" >
        <thead>
            <tr>
                <th scope="col">Niveau de titre en Noir</th>
                <th scope="col">Niveau de titre Braille</th>
            </tr>
        </thead>
        <tbody>
            {existingLevels.map(i => levels(i))}
        </tbody>
    </table>

    //    return <div className="container text-center" >{existingLevels.map(i => levels(i))}</div>
    return

}
// console.log( "ooooooooooooooooooooo", Object.keys(filterOptionsMetadata).filter( n => filterOptionsMetadata[n].possibleValues?.length))
// "LAYOUT_PAGE_EMPTY_LINES_MODE", "MODE_G2_HEADING_LEVEL", "LAYOUT_NUMBERING_MODE", "FORMAT_OUTPUT_BRAILLE_TABLE" ]

const GestionLignesVidesOptions = ({ values, handleChange }) => {
    /*
    * Empty lines is grouped in a single component because 
    * LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1,2,3 are used only if LAYOUT_PAGE_EMPTY_LINES_MODE === 1
    */
    const modeModified = (value) => {
        handleChange({ LAYOUT_PAGE_EMPTY_LINES_MODE: value })
    }
    const customModified = which => value => {
        switch (which) {
            case 1: handleChange({ LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1: value }); break;
            case 2: handleChange({ LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2: value }); break;
            case 3: handleChange({ LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3: value }); break;
        }
    }
    const customInactive = !(parseInt(values.LAYOUT_PAGE_EMPTY_LINES_MODE) === 1)
    return <div>
        <SelectOption label="Traitement des lignes vides" handleChange={modeModified} value={values.LAYOUT_PAGE_EMPTY_LINES_MODE} options={pageEmptyLineModeChoices} ariaLabel="gestion des lignes vides" />

        <table className="table table-striped" style={{ visibility: customInactive ? 'collapse' : 'visible' }}>
            <thead>
                <tr>
                    <th scope="col">Lignes vides dans le document d'origine</th>
                    <th scope="col">produisent dans le document Braille</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>une ligne vide</td>
                    <td><NumberSpindle disabled={customInactive} handleChange={customModified(1)} value={values.LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1} /></td>
                </tr>
                <tr>
                    <td>deux lignes vides</td>
                    <td><NumberSpindle disabled={customInactive} handleChange={customModified(2)} value={values.LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2} /></td>
                </tr>
                <tr>
                    <td>trois lignes vides</td>
                    <td><NumberSpindle disabled={customInactive} handleChange={customModified(3)} value={values.LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3} /></td>
                </tr>
            </tbody>
        </table>

    </div>


}


const DoneOptions = [
    "CONFIG_NAME",
    "CONFIG_DESCRIPTION",
    "MODE_MATH_ALWAYS_PREFIX",
    "MODE_MATH_SPECIFIC_NOTATION",
    "MODE_G2",
    "MODE_MATH",
    "MODE_LIT",
    "MODE_CHEMISTRY",
    "MODE_LIT_FR_MAJ_DOUBLE",
    "MODE_LIT_FR_MAJ_MELANGE",
    "MODE_LIT_FR_MAJ_PASSAGE",
    "MODE_LIT_EMPH",
    "MODE_LIT_FR_EMPH_IN_WORD",
    "MODE_LIT_FR_EMPH_IN_PASSAGE",
    "MODE_G2_HEADING_LEVEL",
    "IvbMajSeule",
    "MODE_ARRAY_2D_MIN_CELL_NUM",
    "MODE_ARRAY_LINEARIZE_ALWAYS",
    "LAYOUT",
    "LAYOUT_LIT_HYPHENATION",
    "LAYOUT_DIRTY_HYPHENATION",
    "LAYOUT_NUMBERING_MODE",
    "LAYOUT_NUMBERING_START_PAGE",
    "LAYOUT_PAGE_EMPTY_LINES_MODE",
    "LAYOUT_PAGE_EMPTY_LINES_CUSTOM_1",
    "LAYOUT_PAGE_EMPTY_LINES_CUSTOM_2",
    "LAYOUT_PAGE_EMPTY_LINES_CUSTOM_3",
    "LAYOUT_PAGE_BREAKS",
    "LAYOUT_PAGE_BREAKS_MIN_LINES",
    "LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL",
    "LAYOUT_PAGE_HEADING_LEVEL",
    "LAYOUT_PAGE_TOC",
    "LAYOUT_PAGE_TOC_NAME",
    "LAYOUT_NOLAYOUT_OUTPUT_TAGS",
    "FORMAT_OUTPUT_BRAILLE_TABLE",
    "FORMAT_OUTPUT_ENCODING",
    "FORMAT_LINE_LENGTH",
    "FORMAT_PAGE_LENGTH",
    ///
    /// NOT DISPLAYED
    /// 
    // ignored
    "imageMagickDir",
    "MODE_IMAGES",
    "MODE_MUSIC",
    // really not an option
    "XSL_musique",
    "XSL_maths",
    "XSL_chimie",
    "XSL_g1",
    "XSL_g2",
    // what is it ?
    "XSL_FR_HYPH_RULES",
    // meta, fixed
    "CONFIG_VERSION",
    // not part of user options
    "MODE_DETRANS"

]

const PrefixesSuffixesElements = ({ value, handleChange }) => {
    //
    // ajout avant-après
    //
    const managedElements = ["document", "paragraphe", "ligne", "mathématique", "littéraire", "musique"]
    const optionStringToArray = s => {
        const a = s.split(",").map(s => s.substring(1, s.length - 1))
        return managedElements.flatMap(() => ['', '']).map((v, i) =>
            (a[i] === undefined) ? '' : a[i]
        )
    }
    const arrayToOptionString = a => {
        return a.map(v => `'${v}'`).join(",")
    }
    //const valuesArray = optionStringToArray(getOptionValue("LAYOUT_NOLAYOUT_OUTPUT_TAGS"))
    const valuesArray = optionStringToArray(value)

    const onArrayCellChanged = index => ({ target }) => {
        const value = target.value
        console.log("changed", index, value)
        valuesArray[index] = value
        // stringOptionChanged("LAYOUT_NOLAYOUT_OUTPUT_TAGS")(arrayToOptionString(valuesArray))
        handleChange(arrayToOptionString(valuesArray))
    }
    const $input = ({ type, pre }) => {
        const id = useId()
        const index = 2 * managedElements.indexOf(type) + (pre ? 0 : 1);
        const label = `${pre ? "Avant" : "Après"} ${type}` + index
        return <td>
            <input key={id} aria-label={label} className="form-control" onChange={onArrayCellChanged(index)} value={valuesArray[index]} />
            {/*            <label htmlFor={id}>{label}</label> */}
        </td>
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Avant</th>
                    <th scope="col">Élément</th>
                    <th scope="col">Après</th>
                </tr>
            </thead>
            <tbody>
                {
                    managedElements.map((type, idx) => {
                        return (
                            <tr key={idx}>
                                {$input({ type, pre: true })}
                                <td key={idx}>{type}</td>
                                {$input({ type, pre: false })}
                            </tr>
                        )
                    })
                }
            </tbody>
        </table>
    )
}

import { Stylist } from './Styles.jsx'
//import { TopNavBar } from "../ui/TopNavbar.jsx"

import { OptionsContext } from "../context/optionsContext.jsx"
import { getNatFilterOptionDefaultValue, getNatFilterOptionValue, setNatFilterOptions } from "../context/natfilterOptions.js"
import useBrailleTables from "../braille-tables/useBrailleTables.js"
import { UnicodeBraille6DotsCharacters } from "../braille-tables/brailleTable.js"
import { ConfigurationsManager } from "./ConfigurationsManager.jsx"
import { StaticSystemG2Rules } from "./static-system-g2-rules.js"
import { UIOptions } from "./UIOptions.jsx"
import ResetOptions from "./ResetOptions.jsx"


const Options = () => {
    const { options, setOptions } = useContext(OptionsContext)


    const booleanOptionChanged = (natFilterOptionName) => newValue => {
        setNatFilterOptions(setOptions, options)({ [natFilterOptionName]: newValue.toString() })
        // options[natFilterOptionName] = newValue.toString()
        // setOptions({ ...options })
    }
    const stringOptionChanged = (natFilterOptionName) => newValue => {
        setNatFilterOptions(setOptions, options)({ [natFilterOptionName]: newValue })
        // console.log(natFilterOptionName, newValue)
        // options[natFilterOptionName] = newValue
        // setOptions({ ...options })
    }
    const numberOptionChanged = (natFilterOptionName) => newValue => {
        setNatFilterOptions(setOptions, options)({ [natFilterOptionName]: newValue.toString() })
        // options[natFilterOptionName] = newValue.toString()
        // setOptions({ ...options })
    }
    const optionChangeHandlers = {
        String: stringOptionChanged,
        Number: numberOptionChanged,
        Boolean: booleanOptionChanged,
        Url: stringOptionChanged
    }
    const multipleStringOptionsChanged = (natFilterOptionNames) => newValues => {
        // // ensure only natFilterOptionNames are modified
        // natFilterOptionNames.forEach(natFilterOptionName => {
        //     const newValue = newValues[natFilterOptionName]
        //     // change only changed newValues
        //     if (newValue !== undefined)
        //         options[natFilterOptionName] = newValues[natFilterOptionName]
        // })
        // setOptions({ ...options })
        setNatFilterOptions(setOptions, options)(newValues)
    }
    const getOptionValue = getNatFilterOptionValue(options)

    const getG2MethodFromG2Dict = g2dict => {
        const g2rule = StaticSystemG2Rules.list.find(({ rulefile }) => rulefile === g2dict)
        const method = g2rule?.method
        return (method || StaticSystemG2Rules.filters[0])
    }

    const [g2RulesFilter, setG2RulesFilters] = useState(getG2MethodFromG2Dict(getOptionValue("XSL_G2_DICT")))

    useEffect(() => {
        setG2RulesFilters(getG2MethodFromG2Dict(getOptionValue("XSL_G2_DICT")))
    }, [options])

    const EcrituresBraille = () => (
        <>
            {/* 
            <Switch label="Traiter les écritures littéraires" handleChange={booleanOptionChanged("MODE_LIT")} value={getOptionValue("MODE_LIT")} />
    */}
            <Switch label="Traiter les écritures mathématiques" handleChange={booleanOptionChanged("MODE_MATH")} value={getOptionValue("MODE_MATH")} />
            <div className="ms-2">
                <Switch disabled={getOptionValue("MODE_MATH") === 'false'} label="Préfixer tous les contenus mathématiques" handleChange={booleanOptionChanged("MODE_MATH_ALWAYS_PREFIX")} value={getOptionValue("MODE_MATH_ALWAYS_PREFIX")} />
                <Switch disabled={getOptionValue("MODE_MATH") === 'false'} label="Notation trigonométrique spécifique" handleChange={booleanOptionChanged("MODE_MATH_SPECIFIC_NOTATION")} value={getOptionValue("MODE_MATH_SPECIFIC_NOTATION")} />
            </div>


            <Switch label="Utiliser le Braille abrégé" handleChange={booleanOptionChanged("MODE_G2")} value={getOptionValue("MODE_G2")} />
            {/*
            <Switch label="Traiter les notations de physique et chimie" handleChange={booleanOptionChanged("MODE_CHEMISTRY")} value={getOptionValue("MODE_CHEMISTRY")} />
        */}
        </>
    )
    const Majuscules = () => (
        <>
            <Switch label="Double préfixe pour les mots en majuscule" handleChange={booleanOptionChanged("MODE_LIT_FR_MAJ_DOUBLE")} value={getOptionValue("MODE_LIT_FR_MAJ_DOUBLE")} />
            <Switch label="Traiter le mélange de majuscules et minuscules dans un mot" handleChange={booleanOptionChanged("MODE_LIT_FR_MAJ_MELANGE")} value={getOptionValue("MODE_LIT_FR_MAJ_MELANGE")} />
            <Switch label="Traiter les parties de texte en majuscules" handleChange={booleanOptionChanged("MODE_LIT_FR_MAJ_PASSAGE")} value={getOptionValue("MODE_LIT_FR_MAJ_PASSAGE")} />

        </>
    )
    const MisesEnEvidence = () => (
        <>
            <Switch label="Traiter les mises en évidence" handleChange={booleanOptionChanged("MODE_LIT_EMPH")} value={getOptionValue("MODE_LIT_EMPH")} />
            <Switch disabled={getOptionValue("MODE_LIT_EMPH") === 'false'} label="À l'intérieur du mot" handleChange={booleanOptionChanged("MODE_LIT_FR_EMPH_IN_WORD")} value={getOptionValue("MODE_LIT_FR_EMPH_IN_WORD")} />
            <Switch disabled={getOptionValue("MODE_LIT_EMPH") === 'false'} label="De parties de texte" handleChange={booleanOptionChanged("MODE_LIT_FR_EMPH_IN_PASSAGE")} value={getOptionValue("MODE_LIT_FR_EMPH_IN_PASSAGE")} />
        </>
    )

    const abregeMethodsChoice = StaticSystemG2Rules.filters.map(filter => ({
        label: filter, value: filter
    }))
    const abregeMethodChanged = (e) => {
        if (e !== g2RulesFilter) {
            const firstLessonOfMethod = StaticSystemG2Rules.list.find(({ method }) => method === e)
            if (firstLessonOfMethod) {
                // set first lesson of method
                stringOptionChanged("XSL_G2_DICT")(firstLessonOfMethod.rulefile)
            }
        }
    }
    const abregeRulesChoice = StaticSystemG2Rules.list.filter(({ method }) => method === g2RulesFilter).map(({ label, rulefile }) => ({
        label,
        value: rulefile,
    }))

    const Abrege = () => (
        <>
            <NumberSpindle label="Abrégér les titres à partir du niveau" handleChange={numberOptionChanged("MODE_G2_HEADING_LEVEL")} min={0} /*max={maxLevel}*/ value={getOptionValue("MODE_G2_HEADING_LEVEL")} />
        </>
    )
    const AbregeRules = () => (
        <>
            <SelectOption label="Méthode" handleChange={abregeMethodChanged} value={g2RulesFilter} options={abregeMethodsChoice} ariaLabel="Méthode d'abrégé" />

            <div className={(abregeRulesChoice?.length === 1) ? "useless-because-only-one-choice" : ""}>
                <div className="ms-4 me-1">
                    <SelectOption disabled={true} label="Règles d'abrégé" handleChange={stringOptionChanged("XSL_G2_DICT")} value={getOptionValue("XSL_G2_DICT")} options={abregeRulesChoice} ariaLabel="Règles d'abrégé" />
                </div>
            </div>

            {/* TODO clarfier */}
            {/*
           <Switch label="IVB sur une majuscule isolée" handleChange={booleanOptionChanged("IvbMajSeule")} value={getOptionValue("IvbMajSeule")} />
    */}
        </>
    )
    const Tableaux = () => (
        <>
            <Switch label="Toujours linérariser les tableaux" handleChange={booleanOptionChanged("MODE_ARRAY_LINEARIZE_ALWAYS")} value={getOptionValue("MODE_ARRAY_LINEARIZE_ALWAYS")} />
            <NumberSpindle disabled={getOptionValue("MODE_ARRAY_LINEARIZE_ALWAYS") === 'true'} label="Linéariser les tableaux à partir de combien de cellules ?" handleChange={numberOptionChanged("MODE_ARRAY_2D_MIN_CELL_NUM")} min={0} /*max={maxLevel}*/ value={getOptionValue("MODE_ARRAY_2D_MIN_CELL_NUM")} />
        </>
    )
    const ActiverMiseEnPage = () => (
        <Switch label="Activer la mise en page" handleChange={booleanOptionChanged("LAYOUT")} value={getOptionValue("LAYOUT")} />
    )
    const Coupure = () => (
        <>
            <Switch label="Activer la coupure littéraire" handleChange={booleanOptionChanged("LAYOUT_LIT_HYPHENATION")} value={getOptionValue("LAYOUT_LIT_HYPHENATION")} />
            <Switch label="Coupure brute des mots en fin de ligne" handleChange={booleanOptionChanged("LAYOUT_DIRTY_HYPHENATION")} value={getOptionValue("LAYOUT_DIRTY_HYPHENATION")} />
        </>
    )
    const NumerotationPage = () => (
        <>
            <SelectOption label="Numérotation des pages" handleChange={stringOptionChanged("LAYOUT_NUMBERING_MODE")} value={getOptionValue("LAYOUT_NUMBERING_MODE")} options={pageNumberingChoices} ariaLabel="numérotation des pages" />

            <NumberSpindle disabled={getOptionValue("LAYOUT_NUMBERING_MODE") === "'nn'"} label="Numéroter à partir de la page" handleChange={numberOptionChanged("LAYOUT_NUMBERING_START_PAGE")} min={0} /*max={maxLevel}*/ value={getOptionValue("LAYOUT_NUMBERING_START_PAGE")} />
        </>
    )
    const LignesVides = () => (
        <GestionLignesVidesOptions
            handleChange={multipleStringOptionsChanged(emptyLineOptionsGroup)}
            values={Object.fromEntries(emptyLineOptionsGroup.map(name => [name, getOptionValue(name)]))} />
    )
    const SautsDePages = () => (
        <>
            <Switch label="Générer des sauts de page" handleChange={booleanOptionChanged("LAYOUT_PAGE_BREAKS")} value={getOptionValue("LAYOUT_PAGE_BREAKS")} />
            <Switch label="Identiques à l'original en Noir" handleChange={booleanOptionChanged("LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL")} value={getOptionValue("LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL")} />
            <NumberSpindle disabled={getOptionValue("LAYOUT_PAGE_BREAKS_KEEP_ORIGINAL") === "true"} label="Générer un saut de page à partir de combien de lignes vides ?" handleChange={numberOptionChanged("LAYOUT_PAGE_BREAKS_MIN_LINES")} min={0} /*max={maxLevel}*/ value={getOptionValue("LAYOUT_PAGE_BREAKS_MIN_LINES")} />
        </>
    )
    const NiveauxTitres = () => (
        <>
            <NiveauxDeTitres stringChanged={stringOptionChanged("LAYOUT_PAGE_HEADING_LEVEL")} optionString={getOptionValue("LAYOUT_PAGE_HEADING_LEVEL")} />
            <div className="ms-2">
                <button className="btn btn-primary" onClick={() => stringOptionChanged("LAYOUT_PAGE_HEADING_LEVEL")(getNatFilterOptionDefaultValue("LAYOUT_PAGE_HEADING_LEVEL"))}>Rétablir comme dans la norme Braille</button>
            </div>
        </>
    )
    const TableDesMatieres = () => (
        <>
            <div className="mb-2">
                <Switch label="Ajouter une table des matières" handleChange={booleanOptionChanged("LAYOUT_PAGE_TOC")} value={getOptionValue("LAYOUT_PAGE_TOC")} />
            </div>
            <div className="mb-5">
                <SimpleText disabled={getOptionValue("LAYOUT_PAGE_TOC") === "false"} label="Titre de la table" handleChange={booleanOptionChanged("LAYOUT_PAGE_TOC_NAME")} value={getOptionValue("LAYOUT_PAGE_TOC_NAME")} />
            </div>
        </>
    )
    const PasDeMiseEnPage = () => (
        <>
            <h3>Suffixes et prefixes</h3>
            <p>Ces suffixes et prefixes sont utilisés par exemple lorsque la mise en page est déléguée à un logiciel tiers</p>
            <PrefixesSuffixesElements value={getOptionValue("LAYOUT_NOLAYOUT_OUTPUT_TAGS")} handleChange={stringOptionChanged("LAYOUT_NOLAYOUT_OUTPUT_TAGS")} />

        </>
    )
    const DimensionsPage = () => (
        <>
            <NumberSpindle label="Nombre de caractères par ligne" handleChange={numberOptionChanged("FORMAT_LINE_LENGTH")} min={10} /*max={maxLevel}*/ value={getOptionValue("FORMAT_LINE_LENGTH")} />
            <NumberSpindle label="Nombre de lignes par page" handleChange={numberOptionChanged("FORMAT_PAGE_LENGTH")} min={10} /*max={maxLevel}*/ value={getOptionValue("FORMAT_PAGE_LENGTH")} />
        </>
    )


    const brailleTables = useBrailleTables()
    const tableSplits = 4;
    const TableEtEncodage = () => (
        <>
            <SelectOption label="Table Braille" handleChange={stringOptionChanged("FORMAT_OUTPUT_BRAILLE_TABLE")} value={getOptionValue("FORMAT_OUTPUT_BRAILLE_TABLE")} options={brailleTableChoices} ariaLabel="table Braille" />
            {/*
                (brailleTables && brailleTables.names.map(name => {
                    return <span key={name}>{name}</span>
                }))
            */}
            {

                brailleTables ? (
                    Array(tableSplits).fill(0).map((_, subTableIndex) => {
                        const oneRowLength = Math.floor(UnicodeBraille6DotsCharacters.length / tableSplits)
                        const oneRowChars = UnicodeBraille6DotsCharacters.slice(oneRowLength * subTableIndex, oneRowLength * (subTableIndex + 1))
                        //return <table className="braille-table-show" >
                        return <table key={subTableIndex} className="table table-bordered" >
                            <tbody>
                                <tr><th>unicode</th>{oneRowChars.map(c => <td key={c}>{c}</td>)}</tr>
                                <tr><th>{getOptionValue("FORMAT_OUTPUT_BRAILLE_TABLE")}</th>{
                                    oneRowChars.map((c, i) => <td key={i}>{brailleTables.get(getOptionValue("FORMAT_OUTPUT_BRAILLE_TABLE")).fromUnicode(c)}</td>)
                                }</tr>
                            </tbody>
                        </table>
                    })
                ) : ('')
            }
            {/* TODO */}
            {/*
            <p>Encodage de texte Braille</p>
            <SimpleText label="Encodage du Braille" handleChange={booleanOptionChanged("FORMAT_OUTPUT_ENCODING")} value={getOptionValue("FORMAT_OUTPUT_ENCODING")} />
    */}
        </>
    )
    const Styles = () => (
        <>
            <Stylist />
        </>
    )
    const Debug = () => (
        <>
            {
                Object.entries(filterOptionsMetadata).filter(([optionName]) => optionName.startsWith('debug_')).map(([optionName, optionMetadata]) => {
                    const handleChange = optionChangeHandlers[optionMetadata.type](optionName)
                    return <GenericOption handleChange={handleChange} key={optionName} optionName={optionName} optionMetadata={optionMetadata} value={getOptionValue(optionName)} />
                })
            }
        </>
    )
    const RemainingOptions = () => (
        <>
            {
                Object.entries(filterOptionsMetadata).map(([optionName, optionMetadata]) => {

                    if (DoneOptions.includes(optionName) || optionName.startsWith('debug_')) {
                        return ''
                    } else {
                        const handleChange = optionChangeHandlers[optionMetadata.type](optionName)
                        return <GenericOption handleChange={handleChange} key={optionName} optionName={optionName} optionMetadata={optionMetadata} value={getOptionValue(optionName)} />
                    }
                })
            }
        </>
    )
    const NameAndDescription = () => <>
        <SimpleText label="Nom" handleChange={booleanOptionChanged("CONFIG_NAME")} value={getOptionValue("CONFIG_NAME")} />
        <SimpleText label="Description" handleChange={booleanOptionChanged("CONFIG_DESCRIPTION")} value={getOptionValue("CONFIG_DESCRIPTION")} />
    </>

    const UIOptionsSetter = () => <>
        <UIOptions />
    </>
    const ConfigurationsManagerSetter = () => <>
        <ConfigurationsManager />
    </>
    const oStructure = {
        title: "ROOT", childs: [
            {
                title: "Transcription", childs: [
                    // {  title: "", setter : NameAndDescription  },
                    {

                        title: "Mode de Transcription", childs: [
                            { title: "Écritures", setter: EcrituresBraille },
                            {
                                title: "Règles", childs: [
                                    /*              {
                                                      title: "Braille intégral", childs: [
                                                          { title: "Majuscules", setter: Majuscules },
                                                          { title: "Mises en évidence", setter: MisesEnEvidence },
                                                      ]
                                                  },*/
                                    /*{ title: "Braille Abrégé", setter: Abrege }*/
                                    {
                                        title: "Braille Abrégé", childs: [
                                            { title: 'Titres', setter: Abrege },
                                            { title: 'Règles (abrégé progressif)', setter: AbregeRules }
                                        ]
                                    }
                                ]
                            },
                            //{ title: "Tableaux", setter: Tableaux }
                        ]
                    },
                    {
                        title: "Mise en page",
                        childs: [
                            //{ title: "Activer la mise en page", setter: ActiverMiseEnPage },
                            //{ title: "Coupure", setter: Coupure },
                            {
                                title: "Propriétés de la page",
                                childs: [
                                    { title: "Dimensions", setter: DimensionsPage },
                                    { title: "Numérotation", setter: NumerotationPage },
                                    { title: "Gestion des lignes vides", setter: LignesVides },
                                    //{ title: "Sauts de page", setter: SautsDePages }
                                ]
                            },
                            /* { title: "Niveaux de titres", setter: NiveauxTitres },
                             { title: "Table des matières", setter: TableDesMatieres },
                             { title: "Pas de mise en page", setter: PasDeMiseEnPage }*/
                        ]
                    },
                    {
                        title: "Formats de Documents",
                        childs: [
                            /*{ title: "Styles", setter: Styles },*/
                            { title: "Encodage", setter: TableEtEncodage },
                        ]
                    },

                    // { title: "Options de debug", setter: Debug },
                    // { title: "Dev", setter: RemainingOptions }
                ]
            },
            {
                title: "Gestion", setter: ConfigurationsManagerSetter
            },            
            {
                title: "Interface", setter: UIOptionsSetter
            },
            {
                title: "Réinitialisation", setter: () => (<ResetOptions/>)
            },
        ]
    }


    const maxNavDepth = 2
    function buildNavigation(p, depth = 0, childIndex = 0) {
        const showChildren = (p.childs && p.childs.length && depth < maxNavDepth)
        const href = `#${p.title.replace(/\s/g, '_')}`
        const classes = [
            `ms-${depth + 1}`,
            'nav-link',
            'option-menu-item',
            (showChildren ? ('option-menu-item-having-children') : ('option-menu-item-not-having-children')),
            'text-dark'
        ].join(' ')
        const item = (depth === 0) ? ('') : (<a href={href} className={classes} >{p.title}</a>)
        return (
            <>
                {item}
                {
                    showChildren && (
                        <nav className="nav nav-pills flex-column" onClick={({ target }) => target.classList.toggle('option-menu-item-open')}>
                            {
                                p.childs.map((child, childIndex) => {
                                    return (
                                        <div key={`cc${p.title}${childIndex}`}>
                                            {buildNavigation(child, depth + 1, childIndex)}
                                        </div>
                                    )
                                })
                            }
                        </nav>
                    )
                }
            </>
        )
    }
    function buildSetters(p, depth = 0) {
        const HeaderN = `h${depth + 1}`
        const href = `${p.title.replace(/\s/g, '_')}`
        const hasChilds = (p.childs && p.childs.length)
        //const marginStart = `ms-${depth} mt-${Math.max(1, 6 - depth)}`
        //const marginStart = `ms-2 mt-${Math.max(1, 6 - depth)}`
        const marginStart = ""
        return (
            <div key={href} id={href} className={marginStart}>
                {(depth > 0) && (<HeaderN className="my-2">{p.title}</HeaderN>)}
                {p.setter && p.setter()}
                {
                    p.childs?.map(child => {
                        return buildSetters(child, depth + 1)
                    })
                }
                {(depth == 1) && (<hr className="mb-5 mt-5" />)}

            </div>
        )
    }



    function content() {
        return <>


            <nav className="text-decoration-none  natbraille-side-options-navigation" >
                {buildNavigation(oStructure)}
            </nav>
            <div className="natbraille-options-setters ms-4 me-4" >
                {buildSetters(oStructure)}
                {/*
                <div className="mb-5">
                    <h2>Résumé</h2>
                    <pre>
                        {JSON.stringify(options, null, 2)}
                    </pre>
                </div>
                 */}
            </div>
        </>
    }
    return <div className="natbraille-options">{content()}</div>
}
export default Options