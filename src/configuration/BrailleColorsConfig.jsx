import { useEffect, useState } from 'react';
import "./BrailleColorsConfig.scss"

const Labels = {
    'text-header': 'Texte',
    'background-header': 'Fond',
    'element-header': 'Élement',
    'defined-subheader': 'Actif',
    'color-subheader': 'Couleur',
    'active-text-color-checkbox-aria': "utiliser une couleur de texte pour les éléments de type",
    'active-background-color-checkbox-aria': "utiliser une couleur de fond pour les éléments de type",
    'text-color-color-picker-aria': "couleur de texte pour les éléments de type",
    'background-color-color-picker-aria': "couleur de fond pour les éléments de type",
}
const TypesLabels = {
    'letters': "lettres",
    'digits': "chiffres",
    'punctuation': "ponctuation",
    'list-bullet': "puce de liste",
    'math': "mathématiques",
    'math-prefix': "préfixe mathématique",
    'uppercase-prefix': "préfixe majuscule"
}

const BrailleColorsConfig = ({ initialColorsConfiguration, onColorsConfigurationChange }) => {
    const [colorsConfiguration, setColorsConfiguration] = useState(initialColorsConfiguration);
    
    useEffect(() => {
        // ensure redraw on initialColorsConfiguration change
        setColorsConfiguration(initialColorsConfiguration)
    }, [initialColorsConfiguration])

    const handleColorChange = (type, colorType, value) => {
        const updatedConfig = {
            ...colorsConfiguration,
            [type]: {
                ...colorsConfiguration[type],
                [colorType]: value
            }
        };
        setColorsConfiguration(updatedConfig);
        onColorsConfigurationChange(updatedConfig);
    };

    const handleCheckboxChange = (type, colorType, checked) => {
        const updatedConfig = {
            ...colorsConfiguration,
            [type]: {
                ...colorsConfiguration[type],
                [colorType]: checked ? colorsConfiguration[type][colorType] : undefined
            }
        };
        setColorsConfiguration(updatedConfig);
        onColorsConfigurationChange(updatedConfig);
    };
    return (
        <table className="table table-striped">
            <colgroup span="1"></colgroup>
            <colgroup span="2"></colgroup>
            <colgroup span="2"></colgroup>
            <thead>
                <tr>
                    <td rowSpan="1"></td>
                    <th colSpan="2" scope="colgroup">{Labels['text-header']}</th>
                    <th colSpan="2" scope="colgroup">{Labels['background-header']}</th>
                </tr>
                <tr>
                    <th scope="col">{Labels['element-header']}</th>
                    <th scope="col">{Labels['defined-subheader']}</th>
                    <th scope="col">{Labels['color-subheader']}</th>
                    <th scope="col">{Labels['defined-subheader']}</th>
                    <th scope="col">{Labels['color-subheader']}</th>
                </tr>
            </thead>
            <tbody>
                {Object.entries(colorsConfiguration).map(([type, colors]) => {
                    const typeLabel = TypesLabels[type] || type
                    return (
                        <tr key={typeLabel}>
                            <th scope="row">{typeLabel}</th>
                            <td>

                                <input
                                    type="checkbox"
                                    checked={colors.text !== undefined}
                                    onChange={(e) => handleCheckboxChange(type, 'text', e.target.checked)}
                                    aria-label={`${Labels['active-text-color-checkbox-aria']} ${typeLabel}`}
                                />
                            </td>
                            <td>
                                <input
                                    type="color"
                                    value={colors.text || '#000000'}
                                    onChange={(e) => handleColorChange(type, 'text', e.target.value)}
                                    aria-label={`${Labels['text-color-color-picker-aria']} ${typeLabel}`}
                                />

                            </td>
                            <td>

                                <input
                                    type="checkbox"
                                    checked={colors.background !== undefined}
                                    onChange={(e) => handleCheckboxChange(type, 'background', e.target.checked)}
                                    aria-label={`${Labels['active-background-color-checkbox-aria']} ${typeLabel}`} />
                            </td>
                            <td>
                                <input
                                    type="color"
                                    value={colors.background || '#ffffff'}
                                    onChange={(e) => handleColorChange(type, 'background', e.target.value)}
                                    aria-label={`${Labels['background-color-color-picker-aria']} ${typeLabel}`}
                                />
                            </td>

                        </tr>
                    )
                })}
            </tbody>
        </table>
    );
};

export default BrailleColorsConfig;
