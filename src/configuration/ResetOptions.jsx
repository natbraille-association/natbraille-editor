import { useContext } from "react"
import { OptionsContext } from "../context/optionsContext"
import { resetAllUIOptions } from "../context/uiOptions"
import './ResetOptions.scss'

const Labels = {
    'reset-all-options': 'réinitialiser toutes les options',
    'reset-filter-options': 'réinitialiser les options de transcription',
    'reset-ui-options': 'réinitialiser les options d\'interface',
    'confirm-reset-all-options': 'voulez-vous vraiment réinitialiser toutes les options ?',
    'confirm-reset-filter-options': 'voulez-vous vraiment réinitialiser les options de transcription ?',
    'confirm-reset-ui-options': 'voulez-vous vraiment réinitialiser les options d\'interface ?',
}
const ResetOptions = () => {
    const { setUIOptions, setOptions } = useContext(OptionsContext)
    const onResetUiOptionsClick = () => {
        if (window.confirm(Labels['confirm-reset-ui-options'])) {
            console.log('yes')
            resetAllUIOptions(setUIOptions)
        }
    }
    const onResetFilterOptionsClick = () => {
        if (window.confirm(Labels['confirm-reset-filter-options'])) {
            setOptions({})
        }
    }
    const onResetAllOptionsClick = () => {
        if (window.confirm(Labels['confirm-reset-all-options'])) {
            setUIOptions({})
            setOptions({})
        }
    }
    return <div className="reset-options-buttons">
        <button className="btn btn-primary" aria-label={Labels['reset-all-options']} onClick={onResetAllOptionsClick}>{Labels['reset-all-options']}</button>
        <button className="btn btn-primary" aria-label={Labels['reset-filter-options']} onClick={onResetFilterOptionsClick}>{Labels['reset-filter-options']}</button>
        <button className="btn btn-primary" aria-label={Labels['reset-ui-options']} onClick={onResetUiOptionsClick}>{Labels['reset-ui-options']}</button>
    </div>
}
export default ResetOptions
