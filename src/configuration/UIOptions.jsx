import { useContext, useState, useEffect } from "react"
import { OptionsContext } from "../context/optionsContext"
import { NumberSpindle, Switch } from "./BasicComponents"
import { UI_OPTION_BRAILLE_FONT_SIZE, UI_OPTION_COLORED_BRAILLE_OUTPUT, UI_OPTION_MATH_EDITOR_MATHML_FONT_SIZE, setUIOption, getUIOption, UI_OPTION_PERKINS_KEYS, UI_OPTION_COLORED_BRAILLE_COLORS, UI_OPTION_USE_PERKINS_INPUT_IN_BRAILLE_BLOCK } from "../context/uiOptions"

const Labels = {
    [UI_OPTION_BRAILLE_FONT_SIZE]: 'taille de la police Braille',
    [UI_OPTION_COLORED_BRAILLE_OUTPUT]: 'Braille en couleur',
    [UI_OPTION_MATH_EDITOR_MATHML_FONT_SIZE]: "taille de la police mathématique de l'éditeur d'équation",
    [UI_OPTION_USE_PERKINS_INPUT_IN_BRAILLE_BLOCK] : 'Utiliser la saisie perkins dans les blocs braille',
    'title-noir': "Noir",
    'title-braille': "Braille",
    'title-braille-colors': "Couleurs de repérage",
    'title-perkins-input': "Saisie Perkins",
    'perkins-instructions': "Utilisez les boutons pour régler le point braille correspondant",
}

import { PerkinsKeysConfig } from "./PerkinsKeysConfig"
import BrailleColorsConfig from "./BrailleColorsConfig"
import { disabledStyle } from "../style/disabledStyle"

export const UIOptions = () => {
    const { uiOptions, setUIOptions } = useContext(OptionsContext)

    const stringOptionChanged = name => value => {
        setUIOption(setUIOptions, uiOptions)(name, value)
    }
    const booleanOptionChanged = name => value => {
        setUIOption(setUIOptions, uiOptions)(name, value ? true : false)
    }
    const getOptionValue = name => {
        return getUIOption(uiOptions)(name)
    }

    const handleDotsConfigurationChange = (newDotsConfiguration) => {
        setUIOption(setUIOptions, uiOptions)(UI_OPTION_PERKINS_KEYS, newDotsConfiguration)
    };
    const handleColorsConfigurationChange = (newBrailleColorsConfiguration) => {
        console.log('!', newBrailleColorsConfiguration)
        setUIOption(setUIOptions, uiOptions)(UI_OPTION_COLORED_BRAILLE_COLORS, newBrailleColorsConfiguration)
    }
    
    return <div className="m-2 mb-5">
        {/*<h3>Options d'interface</h3>*/}
        <div>
            <h4>{Labels['title-noir']}</h4>
            <NumberSpindle label={Labels[UI_OPTION_MATH_EDITOR_MATHML_FONT_SIZE]} min={5} handleChange={stringOptionChanged(UI_OPTION_MATH_EDITOR_MATHML_FONT_SIZE)} value={getOptionValue(UI_OPTION_MATH_EDITOR_MATHML_FONT_SIZE)} />

            <h4>{Labels['title-braille']}</h4>
            <NumberSpindle label={Labels[UI_OPTION_BRAILLE_FONT_SIZE]} min={5} handleChange={stringOptionChanged(UI_OPTION_BRAILLE_FONT_SIZE)} value={getOptionValue(UI_OPTION_BRAILLE_FONT_SIZE)} />

            <h5>{Labels['title-braille-colors']}</h5>
            <Switch label={Labels[UI_OPTION_COLORED_BRAILLE_OUTPUT]} handleChange={booleanOptionChanged(UI_OPTION_COLORED_BRAILLE_OUTPUT)} value={getOptionValue(UI_OPTION_COLORED_BRAILLE_OUTPUT)} />
            <div style={getOptionValue(UI_OPTION_COLORED_BRAILLE_OUTPUT) ? {} : disabledStyle}>
                <BrailleColorsConfig initialColorsConfiguration={getOptionValue(UI_OPTION_COLORED_BRAILLE_COLORS)} onColorsConfigurationChange={handleColorsConfigurationChange} /*firstHeadingLevel={6}*/></BrailleColorsConfig>
            </div>
            <h5>{Labels['title-perkins-input']}</h5>
            <p>{Labels['perkins-instructions']}</p>
            <PerkinsKeysConfig initialDotsConfiguration={getOptionValue(UI_OPTION_PERKINS_KEYS)} onDotsConfigurationChange={handleDotsConfigurationChange} />

            <h4>{Labels['title-perkins-input-in-braille-block']}</h4>
            <div className="mt-4">
            <Switch label={Labels[UI_OPTION_USE_PERKINS_INPUT_IN_BRAILLE_BLOCK]} handleChange={booleanOptionChanged(UI_OPTION_USE_PERKINS_INPUT_IN_BRAILLE_BLOCK)} value={getOptionValue(UI_OPTION_USE_PERKINS_INPUT_IN_BRAILLE_BLOCK)} />
            </div>
        </div>
    </div>
}