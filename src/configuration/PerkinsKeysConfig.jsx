import './PerkinsKeysConfig.scss'
import { useState, useEffect } from "react"

const Labels = {
    'dot': 'pt',
    'unset': 'non défini',
    'press-a-key': 'appuyez sur la touche'
}
export const PerkinsKeysConfig = ({ initialDotsConfiguration, onDotsConfigurationChange }) => {
    const [dotsConfiguration, setDotsConfiguration] = useState(initialDotsConfiguration);
    const [waitingForKeypress, setWaitingForKeypress] = useState(null);
    
    useEffect(() => {
        // ensure redraw on initialDotsConfiguration change
        setDotsConfiguration(initialDotsConfiguration)
    }, [initialDotsConfiguration])
   
    useEffect(() => {
        // Update parent with initial configuration
        onDotsConfigurationChange(dotsConfiguration);

        const handleKeyDown = (event) => {
            if (waitingForKeypress) {
                if (event.key === 'Escape') {
                    // Cancel action if Escape key is pressed
                    setWaitingForKeypress(null);
                    return;
                }
                handleKeyPress(event, waitingForKeypress);
            }
        };

        // Add event listener for keydown
        window.addEventListener('keydown', handleKeyDown);

        // Cleanup function
        return () => {
            // Remove event listener
            window.removeEventListener('keydown', handleKeyDown);
        };
    }, [waitingForKeypress]); // eslint-disable-line react-hooks/exhaustive-deps

    const handleKeyPress = (event, dotName) => {
        const newDotsConfiguration = { codes : { ...dotsConfiguration.codes  }, keys : { ...dotsConfiguration.keys } }
        newDotsConfiguration.codes[dotName] = event.code;
        newDotsConfiguration.keys[dotName] = event.key;
        setDotsConfiguration(newDotsConfiguration);
        setWaitingForKeypress(null);
        onDotsConfigurationChange(newDotsConfiguration);
    };

    const startKeypress = (dotName) => {
        setWaitingForKeypress(dotName);
    };

    const renderButtons = () => {
        return Object.keys(dotsConfiguration.codes).map((dotName) => {
            const dotLabel = `${Labels['dot']}${dotName}`
            const dotValue = dotsConfiguration.keys[dotName] || dotsConfiguration.codes[dotName] || Labels['unset']
            const valueLabel = `${dotLabel} : ${dotValue}`
            return <button key={dotName} className="btn btn-sm btn-primary" onClick={() => startKeypress(dotName)}>
                {waitingForKeypress === dotName ? `${dotLabel} : ${Labels['press-a-key']}` : valueLabel}
            </button>
        });
    };

    return <div className="perkins-keys-config">
        {renderButtons()}
    </div>
};
