//
// cd  Nat-v2.2rc1/configurations
// grep abrege*/*cfg  -e fi-litt-fr-abbreg-rules-filename-perso -e fi-infos
//
const lines = `
abrege1/1-scen01.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 1
abrege1/1-scen01.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-01.xml
abrege1/1-scen02.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 2
abrege1/1-scen02.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-02.xml
abrege1/1-scen03.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 3
abrege1/1-scen03.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-03.xml
abrege1/1-scen04.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 4
abrege1/1-scen04.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-04.xml
abrege1/1-scen05.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 5
abrege1/1-scen05.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-05.xml
abrege1/1-scen06.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 6
abrege1/1-scen06.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-06.xml
abrege1/1-scen07.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 7
abrege1/1-scen07.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-07.xml
abrege1/1-scen08.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 8
abrege1/1-scen08.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-08.xml
abrege1/1-scen09.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 9
abrege1/1-scen09.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-09.xml
abrege1/1-scen10.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 10
abrege1/1-scen10.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-10.xml
abrege1/1-scen11.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 11
abrege1/1-scen11.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-11.xml
abrege1/1-scen12.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 12
abrege1/1-scen12.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-12.xml
abrege1/1-scen13.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 13
abrege1/1-scen13.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-13.xml
abrege1/1-scen14.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 14 s\u00E9rie 1
abrege1/1-scen14.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-14.xml
abrege1/1-scen15.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 14 s\u00E9rie 2
abrege1/1-scen15.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-15.xml
abrege1/1-scen16.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 14 s\u00E9rie 3
abrege1/1-scen16.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-16.xml
abrege1/1-scen17.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 14 s\u00E9rie 4
abrege1/1-scen17.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-17.xml
abrege1/1-scen18.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 15 s\u00E9rie 1
abrege1/1-scen18.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-18.xml
abrege1/1-scen19.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 15 s\u00E9rie 2
abrege1/1-scen19.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-19.xml
abrege1/1-scen20.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 15 s\u00E9rie 3
abrege1/1-scen20.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-20.xml
abrege1/1-scen21.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 15 s\u00E9rie 4
abrege1/1-scen21.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-21.xml
abrege1/1-scen22.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 15 s\u00E9rie 5
abrege1/1-scen22.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-22.xml
abrege1/1-scen23.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 15 s\u00E9rie 6
abrege1/1-scen23.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-23.xml
abrege1/1-scen24.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 15 s\u00E9rie 7
abrege1/1-scen24.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-24.xml
abrege1/1-scen25.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 15 s\u00E9rie 8
abrege1/1-scen25.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-25.xml
abrege1/1-scen26.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 16
abrege1/1-scen26.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-26.xml
abrege1/1-scen27.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 17  s\u00E9rie 1
abrege1/1-scen27.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-27.xml
abrege1/1-scen28.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 17  s\u00E9rie 2
abrege1/1-scen28.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-28.xml
abrege1/1-scen29.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 17  s\u00E9rie 3
abrege1/1-scen29.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-29.xml
abrege1/1-scen30.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 17  s\u00E9rie 4
abrege1/1-scen30.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-30.xml
abrege1/1-scen31.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 17  s\u00E9rie 5
abrege1/1-scen31.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-31.xml
abrege1/1-scen32.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 17  s\u00E9rie 6
abrege1/1-scen32.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-32.xml
abrege1/1-scen33.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 17  s\u00E9rie 7
abrege1/1-scen33.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-33.xml
abrege1/1-scen34.cfg:fi-infos=\u00C9tudions l'abr\u00E9g\u00E9 (Le Reste, Perdoux, 2006), le\u00E7on 17  s\u00E9rie 8
abrege1/1-scen34.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege1-34.xml
abrege2/2-scen01.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 1
abrege2/2-scen01.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-01.xml
abrege2/2-scen02.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 2
abrege2/2-scen02.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-02.xml
abrege2/2-scen03.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 3
abrege2/2-scen03.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-03.xml
abrege2/2-scen04.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 4
abrege2/2-scen04.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-04.xml
abrege2/2-scen05.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 5
abrege2/2-scen05.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-05.xml
abrege2/2-scen06.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 6
abrege2/2-scen06.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-06.xml
abrege2/2-scen07.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 7
abrege2/2-scen07.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-07.xml
abrege2/2-scen08.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 8
abrege2/2-scen08.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-08.xml
abrege2/2-scen09.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 9
abrege2/2-scen09.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-09.xml
abrege2/2-scen10.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 10
abrege2/2-scen10.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-10.xml
abrege2/2-scen11.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 11
abrege2/2-scen11.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-11.xml
abrege2/2-scen12.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 12
abrege2/2-scen12.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-12.xml
abrege2/2-scen13.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 13
abrege2/2-scen13.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-13.xml
abrege2/2-scen14.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 14
abrege2/2-scen14.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-14.xml
abrege2/2-scen15.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 15
abrege2/2-scen15.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-15.xml
abrege2/2-scen16.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 16
abrege2/2-scen16.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-16.xml
abrege2/2-scen17.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 17
abrege2/2-scen17.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-17.xml
abrege2/2-scen18.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 18
abrege2/2-scen18.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-18.xml
abrege2/2-scen19.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 19
abrege2/2-scen19.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-19.xml
abrege2/2-scen20.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 20
abrege2/2-scen20.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-20.xml
abrege2/2-scen21.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 21
abrege2/2-scen21.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-21.xml
abrege2/2-scen22.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 22
abrege2/2-scen22.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-22.xml
abrege2/2-scen23.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 23
abrege2/2-scen23.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-23.xml
abrege2/2-scen24.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 24
abrege2/2-scen24.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-24.xml
abrege2/2-scen25.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 25
abrege2/2-scen25.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-25.xml
abrege2/2-scen26.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 26
abrege2/2-scen26.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-26.xml
abrege2/2-scen27.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 27
abrege2/2-scen27.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-27.xml
abrege2/2-scen28.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 28
abrege2/2-scen28.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-28.xml
abrege2/2-scen29.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 29
abrege2/2-scen29.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-29.xml
abrege2/2-scen30.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 30
abrege2/2-scen30.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-30.xml
abrege2/2-scen31.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 31
abrege2/2-scen31.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-31.xml
abrege2/2-scen32.cfg:fi-infos=L'abr\u00E9g\u00E9 progressif (Kommer, 1977), le\u00E7on 32
abrege2/2-scen32.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege2-32.xml
abrege3/3-scen01.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 1
abrege3/3-scen01.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-01.xml
abrege3/3-scen02.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 2
abrege3/3-scen02.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-02.xml
abrege3/3-scen03.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 3
abrege3/3-scen03.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-03.xml
abrege3/3-scen04.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 4
abrege3/3-scen04.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-04.xml
abrege3/3-scen05.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 5
abrege3/3-scen05.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-05.xml
abrege3/3-scen06.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 6
abrege3/3-scen06.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-06.xml
abrege3/3-scen07.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 7
abrege3/3-scen07.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-07.xml
abrege3/3-scen08.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 8
abrege3/3-scen08.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-08.xml
abrege3/3-scen09.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 9
abrege3/3-scen09.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-09.xml
abrege3/3-scen10.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 10
abrege3/3-scen10.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-10.xml
abrege3/3-scen11.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 11
abrege3/3-scen11.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-11.xml
abrege3/3-scen12.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 12
abrege3/3-scen12.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-12.xml
abrege3/3-scen13.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 13
abrege3/3-scen13.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-13.xml
abrege3/3-scen14.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 14
abrege3/3-scen14.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-14.xml
abrege3/3-scen15.cfg:fi-infos=Je donne ma langue au chat  (Kommer, 1993), le\u00E7on 15
abrege3/3-scen15.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-15.xml
abrege3/3-scen16.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 16
abrege3/3-scen16.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-16.xml
abrege3/3-scen17.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 17
abrege3/3-scen17.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-17.xml
abrege3/3-scen18.cfg:fi-infos=Je donne ma langue au chat  (Kommer, 1993), le\u00E7on 18
abrege3/3-scen18.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-18.xml
abrege3/3-scen19.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 19
abrege3/3-scen19.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-19.xml
abrege3/3-scen20.cfg:fi-infos=Je donne ma langue au chat (Kommer, 1993), le\u00E7on 20
abrege3/3-scen20.cfg:fi-litt-fr-abbreg-rules-filename-perso=xsl/dicts/abrege3-20.xml
`
const PROP_EQ_RULEFILE = "fi-litt-fr-abbreg-rules-filename-perso="
const PROP_EQ_INFO = "fi-infos="

const r = lines.split("\n").map(l => l.trim()).filter(l => l && l.length).map(l => {
    const [filename, property_value] = l.split(':')
    if (property_value.startsWith(PROP_EQ_INFO)) {
        return { filename, label: property_value.replace(PROP_EQ_INFO, '').replace(/\s+/g, ' ') }
    } else if (property_value.startsWith(PROP_EQ_RULEFILE)) {
        return { filename, rulefile: property_value.replace(PROP_EQ_RULEFILE, '').replace(/\s+/g, ' ') }
    }
}).reduce((r, { filename, rulefile, label }) => {
    if (r[filename] === undefined) {
        r[filename] = {}
    }
    if (rulefile) {
        const systemRulefile = `nat://system/${rulefile}`
        r[filename] = { ...r[filename], rulefile: systemRulefile }
    } else if (label) {
        const [method] = label.split(", leçon")
        r[filename] = { ...r[filename], method, label }
    }
    return r
}, {})
/*
'abrege3/3-scen20.cfg': {
    method: 'Je donne ma langue au chat (Kommer, 1993)',
    label: 'Je donne ma langue au chat (Kommer, 1993), leçon 20',
    rulefile: 'nat://system/xsl/dicts/abrege3-20.xml'
  }

*/
const list = [
    {
        method: 'Toute la norme',
        label: 'Toute la norme',
        rulefile: 'nat://system/xsl/dicts/fr-g2.xml'
    },
    ...Object.values(r).sort((a, b) => a.rulefile.localeCompare(b.rulefile))
]
const method_list = list.map(({ method }) => method)
const filters = method_list.filter((x, i) => method_list.indexOf(x) === i)

export const StaticSystemG2Rules = { list, filters }

console.log(StaticSystemG2Rules)