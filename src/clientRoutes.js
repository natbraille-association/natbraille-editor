export const navigateToEditorFile = navigate => filename => navigate(`/editor?f=${encodeURI(filename)}`)
export const navigateToReverseEditor = navigate => navigate(`/reverse-editor`)
export const editorGetFileSearchParam = searchParams => searchParams.get('f')