import { getAccountRemovalEndpoint } from "../serverio/endpoints";
import "./RGPDProfile.css"


const getChallengeData = () => {
    const p = new URL(window.location).searchParams
    const username = p.get('username');
    const challenge = p.get('challenge');
    if (username?.length && challenge?.length) {
        return { username, challenge }
    }
}

export const RemoveAccount = () => {
    // retrieve getChallenge data then display a button that will send
    // the url params in line
    console.log('o')
    const challengeData = getChallengeData();

    if (!challengeData) {
        return <div>No challenge data found.</div>;
    }
    console.log(getAccountRemovalEndpoint())
    return <div id="rgpd-profile">
        <h1>Suppression définitive de votre compte</h1>
        {(challengeData === undefined)
            ? (<p>La Demande de suppression est invalide</p>)
            : (<><p>En cliquant sur le bouton suivant, votre compte et toutes les données associées seront définitivement supprimées.</p>
                <form action={getAccountRemovalEndpoint()} method="POST">
                    <input type="hidden" name="username" value={challengeData.username} />
                    <input type="hidden" name="challenge" value={challengeData.challenge} />
                    <p><button className="btn btn-danger" >Supprimer mon compte</button></p>
                </form>
            </>)
        }
    </div>

}