import { useEffect, useState } from "react"
import "./RGPDProfile.css"
import { getCheckLiveTokenIsValid } from "../serverio/checkLiveToken"
import { getNatbrailleUsernameFromCookie } from "../serverio/cookies"
import { getUserArchiveEndpoint } from "../serverio/endpoints"
import { getUserEmail, postAccountRemovalRequest } from "../serverio/user"
import { NavLink } from "react-router-dom"

function formatDate(date) {
    const day = String(date.getDate()).padStart(2, '0')
    const month = String(date.getMonth() + 1).padStart(2, '0')
    const year = date.getFullYear()
    const hours = String(date.getHours()).padStart(2, '0')
    const minutes = String(date.getMinutes()).padStart(2, '0')
    const seconds = String(date.getSeconds()).padStart(2, '0')
    return `${year}.${month}.${day}-${hours}.${minutes}.${seconds}`;
}

const AccountRemoval = () => {
    const [requestSuccessMessage, setRequestSuccessMessage] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const [isButtonDisabled, setIsButtonDisabled] = useState(false);

    const requestRemoval1 = async () => {

        try {
            // Disable the button to prevent multiple clicks
            setIsButtonDisabled(true);

            // Send a POST request
            console.log('me')
            const { email, username, error } = await postAccountRemovalRequest();
            if (error) {
                throw new Error(error);
            }
            setRequestSuccessMessage(`Un message de confirmation de suppression votre compte '${username}' a été envoyé à l'adresse '${email}'. Si vous suivez le lien donné dans ce message, votre compte sera définitivement supprimé.`);
        } catch (error) {
            setErrorMessage('Une erreur est advenue.');
        } finally {
            // Re-enable the button if needed
            // setIsButtonDisabled(false); // Uncomment this if you want to re-enable the button after the request
        }
    }
    return (
        <div>
            <p><button className="btn btn-danger" disabled={isButtonDisabled} onClick={requestRemoval1}>Supprimer mon compte</button></p>
            { requestSuccessMessage && <div className="alert alert-success" role="alert">{requestSuccessMessage}</div> }
            { errorMessage && <div className="alert alert-warning" role="alert">{errorMessage}</div> }

        </div>
    )
}

export const RGPDProfile = () => {

    const [liveTokenIsValid, setLiveTokenIsValid] = useState(false)
    const [username, setUsername] = useState('')
    const [archiveUrl, setArchiveUrl] = useState('')
    const [userEmail, setUserEmail] = useState('')
    useEffect(() => {
        const update = async () => {
            const valid = await getCheckLiveTokenIsValid()
            setLiveTokenIsValid(valid)
            const email = await getUserEmail()
            setUserEmail(email.email)
            const username = getNatbrailleUsernameFromCookie()
            setUsername(username)
            setArchiveUrl(getUserArchiveEndpoint(username))
        }
        update()
    }, null)

    //getUserDocumentArchive()
    const archiveName = (username) => `${formatDate(new Date())}.zip`

    const downloadFile = async (url, filename) => {
        // this is needed because in debug, document and page have cross-origin        
        // so setting "download" attribute on an <a> won't change the name
        // and will redirect to page.
        try {
            const response = await fetch(url);
            const blob = await response.blob();
            const link = document.createElement('a');
            link.href = URL.createObjectURL(blob);
            link.download = filename; // Set the custom filename
            link.click();
            URL.revokeObjectURL(link.href);
        } catch (error) {
            console.error('Error downloading file:', error);
        }
    };

    return <div id="rgpd-profile">
        <h1>Gestion de vos données personnelles</h1>
        <h2>Identité et mot de passe</h2>
        <ul>
            <li><p>Vous êtes inscrit sur ce serveur sous le nom «&nbsp;<span className="user-data">{username}</span>&nbsp;»</p> </li>
            <li><p>Vous avez utilisé l'adresse de courriel «&nbsp;<span className="user-data">{userEmail}</span>&nbsp;» pour vous inscrire.</p> </li>
        </ul>
        <p>Si vous avez oublié votre mot de passe ou souhaitez en changer, suivez ce lien: <NavLink to="/request-password-reset" >réinitialiser le mot de passe</NavLink></p>
        <h2>Accéder à mes données personnelles</h2>
        <ul>
            <li><p>Vous pouvez à tout moment télécharger une archive des données enregistrées sur ce serveur, qui comprennent vos documents et vos configurations de transcription.</p>
                <p><button className="btn btn-primary" onClick={() => downloadFile(archiveUrl, archiveName(username))}>télécharger l'archive</button></p>
            </li>
            <li><p>Pour supprimer des documents, utilisez la page <NavLink to="/document-management">gestion des documents</NavLink>.</p></li>
            <li><p>Pour supprimer des configurations de transcription, utilisez l'interface de <NavLink to="/options#Gestion">gestion des configurations</NavLink>.</p></li>
        </ul>
        <h2>Supprimer mon compte</h2>
        <p>Pour recevoir par email un lien de suppression de votre compte, cliquez sur le bouton suivant:</p>
        <AccountRemoval></AccountRemoval>        
    </div>
}
