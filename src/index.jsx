import './logo/console-logo.js'
import './logo/rot.js'
import './style/braille-font.css'
import './style/bootstrap-import.scss'
import './style/global.scss'

// bootstrap's JS
import * as bootstrap from 'bootstrap'
import "bootstrap-icons/font/bootstrap-icons.css";

// react
import React, { createContext, useEffect, useState } from 'react'
import { createRoot } from 'react-dom/client'

// react router
import {
  createBrowserRouter,
  RouterProvider,
  NavLink,
  Outlet,
  Navigate
} from "react-router-dom";

import { getCheckLiveTokenIsValid } from './serverio/checkLiveToken.js'
import { getNatbrailleUsernameFromCookie, removeCookies, tokenSaysUserIsConnected } from './serverio/cookies'

// check live
window.getCheckLiveTokenIsValid = getCheckLiveTokenIsValid
window.tokenSaysUserIsConnected = tokenSaysUserIsConnected
window.removeCookies = removeCookies


if (tokenSaysUserIsConnected()) {
  getCheckLiveTokenIsValid().then(connected => {
    if (!connected) {
      alert("connexion expirée - vous allez être redirigé vers la page de connexion")
      removeCookies();
      window.location.reload()
    }
  })
}


import Options from './configuration/Options.jsx'
import { Login } from './login/Login.jsx';
import natbrailleLogo from '/assets/logoNat.svg'

const linkLabels = {
  documents: "documents",
  transcription: "transcription",
  backtranscription: "détranscription",
  options: "options",
  contact: "contact",
  about: "à propos",
  login: "connexion",
  logout: "déconnexion",
  register: "inscription",
  menuTitle: "plus",
  rgpd: "données personnelles"
}

const UndraggableNavLink = ({ children, className = '', ...props }) => {
  const classNamef = ({ isActive, isPending }) => {
    const activeState = isPending ? "pending" : isActive ? "active" : ""
    return [activeState, 'no-drag', className].filter(x => x).join(' ')
  }
  return (
    <NavLink {...props} draggable="false" className={classNamef}>
      {children}
    </NavLink>
  );
};

const PageHeader = () => {
  //const navLinkClassNameFromState = ({ isActive, isPending }) => isPending ? "pending" : isActive ? "active" : ""
  // show "logout" if user is connected, else "login"   
  const loginCaption = tokenSaysUserIsConnected() ? linkLabels["logout"] : linkLabels["login"]

  const $navBar = $menuItems => (
    <header className="natbraille-page-header">
      <span><img src={natbrailleLogo} className="natbraille-logo" alt="logo de natbraille"></img></span>
      {$menuItems}
    </header>
  )

  const $profileMenu = () => {
    return (
      <span className="natbraille-name nav-item-dropdown no-drag">
        <div draggable="false" className="nav-link no-drag" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          <ProfileAvatar name={getNatbrailleUsernameFromCookie()} />
        </div>
        <ul className="dropdown-menu">
          <li><UndraggableNavLink to="/logout" className="dropdown-item">{loginCaption}</UndraggableNavLink></li>
          <li><UndraggableNavLink to="/contact" className="dropdown-item">{linkLabels['contact']}</UndraggableNavLink></li>
          <li><UndraggableNavLink to="/rgpd" className="dropdown-item">{linkLabels['rgpd']}</UndraggableNavLink></li>
          <li><UndraggableNavLink to="/about" className="dropdown-item">{linkLabels['about']}</UndraggableNavLink></li>
        </ul>
      </span>
    )
  }

  if (tokenSaysUserIsConnected()) {
    return $navBar(
      <>
        <div className="full-content">
          <UndraggableNavLink to="/document-management" >{linkLabels['documents']}</UndraggableNavLink>
          <UndraggableNavLink to="/editor" >{linkLabels['transcription']}</UndraggableNavLink>
          <UndraggableNavLink to="/reverse-editor" >{linkLabels['backtranscription']}</UndraggableNavLink>
          <UndraggableNavLink to="/options" >{linkLabels['options']}</UndraggableNavLink>
          {/* 
          <span className="nav-item dropdown">
            <a draggable="false" className="nav-link dropdown-toggle no-drag" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              {linkLabels['menuTitle']}
            </a>
            <ul className="dropdown-menu">
              <li><UndraggableNavLink to="/logout" className="dropdown-item">{loginCaption}</UndraggableNavLink></li>
              <li><UndraggableNavLink to="/contact" className="dropdown-item">{linkLabels['contact']}</UndraggableNavLink></li>
              <li><UndraggableNavLink to="/about" className="dropdown-item">{linkLabels['about']}</UndraggableNavLink></li>
            </ul>
          </span>
          */}

        </div>
        <div className="reduced-content">
          <div className="nav-item dropdown">
            <button className="bi bi-list natbraille-editor-menuicon" data-bs-toggle="dropdown"></button>
            <ul className="dropdown-menu">
              <li><UndraggableNavLink to="/document-management" className="dropdown-item " >{linkLabels['documents']}</UndraggableNavLink></li>
              <li><UndraggableNavLink to="/editor" className="dropdown-item ">{linkLabels['transcription']}</UndraggableNavLink></li>
              <li><UndraggableNavLink to="/reverse-editor" className="dropdown-item ">{linkLabels['backtranscription']}</UndraggableNavLink></li>
              <li><UndraggableNavLink to="/options" className="dropdown-item ">{linkLabels['options']}</UndraggableNavLink></li>
              {/*
              <li><UndraggableNavLink to="/logout" className="dropdown-item">{loginCaption}</UndraggableNavLink></li>
              <li><UndraggableNavLink to="/contact" className="dropdown-item">{linkLabels['contact']}</UndraggableNavLink></li>
              <li><UndraggableNavLink to="/about" className="dropdown-item">{linkLabels['about']}</UndraggableNavLink></li>
 */}
            </ul>
          </div>
        </div>

        {$profileMenu()}
      </>
    )
  } else {
    return $navBar(
      <>
        <UndraggableNavLink to="/login" >{linkLabels['login']}</UndraggableNavLink>
        <UndraggableNavLink to="/register" >{linkLabels['register']}</UndraggableNavLink>
        <UndraggableNavLink to="/about" >{linkLabels['about']}</UndraggableNavLink>
        <span className="natbraille-name">natbraille</span>
      </>
    );
  }
}

const PageMain = () => {
  return <main className="natbraille-page-content-container" >
    <Outlet />
  </main>
}

const Root = () => {
  return <>
    <PageHeader />
    <PageMain />
  </>
}


export const ProtectedRoute = ({ children }) => {
  // const token = getNatbrailleTokenFromCookie()
  // const connected = (token?.length > 0)
  // console.log('connected?', connected)
  if (!tokenSaysUserIsConnected()) {
    //if (!connected) {
    return <Navigate to="/login" />;
  }
  protectPageChange()
  return children;
};

import { EditorSplit } from './editor/editor'
import { ReverseEditorSplit } from './reverse-editor/reverse-editor'
import { OptionsContextProvider } from './context/optionsContext.jsx'
import PerformanceMonitor from './monitor/PeformanceMonitor.jsx'
import { DocumentManagement } from './documents/DocumentsManagement';
import { protectPageChange } from './unload-protection.js'
import { About } from './about/About.jsx'
import { ContactForm } from './feedback/Feedback.jsx'
import ProfileAvatar from './ProfileAvatar.jsx'
import { RGPDProfile } from './profile/RGPDProfile.jsx'
import { RemoveAccount } from './profile/RemoveAccount.jsx'

import { EditedFileProvider } from './context/EditedFileContext.jsx'

// const ErrorPage = () => { return <div>Il y a eu une erreur</div> }

const router = createBrowserRouter([
  {
    element: <Root />,
    //errorElement: <ErrorPage />,
    path: "/",
    children: [
      {
        path: "",
        element: <About />
      },
      {
        path: "document-management",
        element: <ProtectedRoute><DocumentManagement /></ProtectedRoute>
      },
      {
        path: "editor",
        element: <ProtectedRoute><EditorSplit /></ProtectedRoute>
      },
      {
        path: "reverse-editor",
        element: <ProtectedRoute><ReverseEditorSplit /></ProtectedRoute>
      },
      {
        path: "options",
        element: <ProtectedRoute><Options /></ProtectedRoute>
      },
      {
        path: "rgpd",
        element: <ProtectedRoute><RGPDProfile /></ProtectedRoute>
      },
      {
        path: "login",
        element: <Login />
      },
      {
        path: "logout",
        element: <Login />
      },
      {
        path: "register",
        element: <Login />
      },
      {
        path: "request-password-reset",
        element: <Login />
      },
      {
        path: "password-reset",
        element: <Login />
      },
      {
        path: "account-removal",
        element: <ProtectedRoute><RemoveAccount /></ProtectedRoute>
      },
      {
        path: "contact",
        element: <ProtectedRoute><ContactForm /></ProtectedRoute>
      },
      {
        path: "about",
        element: <About />
      },
      {
        path: "monitor",
        element: <PerformanceMonitor />
      },

    ],
  },
]);

createRoot(document.getElementById("root")).render(
  <EditedFileProvider>
    <OptionsContextProvider>
      <RouterProvider router={router} />
    </OptionsContextProvider>
  </EditedFileProvider>
)
