import "./DocumentsManagement.scss"
import { useEffect, useRef, useState } from "react";
import { loadArrayBuffer } from "../localio/load";
// import { deleteUserDocument, getUserDocument, getUserDocumentKindList, postUserDocument } from "../serverio/userDocument";
// import { getNatbrailleUsernameFromCookie } from "../serverio/cookies";
import { getUserNatbrailleDocumentList, getUserNatbrailleDocument, postUserNatbrailleDocument, deleteUserNatbrailleDocument, getUserDocument } from "../serverio/userDocument"

import { createUniqueFilename, filenameStemExtension } from "./uniqueFilename";
import { postAnyToOdt, postOdtToBlack } from "../serverio/import";
import { useNavigate } from "react-router-dom";
import { navigateToEditorFile } from "../clientRoutes";
import { putNatbrailleDocumentToLocalStorage, removeEditorProgressFromLocalStorage } from "../localstorage/store";
import { useEditedFileContext } from "../context/EditedFileContext";
// import { getNatbrailleUsernameFromCookie } from "../serverio/cookies";

// import = to xml -> save
// + nouveau
// select old

// => copie dans current document 

const Labels = {
    'new document': "nouveau document",
    'create': "Créer",
    'import black': "importer un document noir local",
    'recent transcriptions': "transcriptions récentes",
    'open': "ouvrir",
    'select all': "tout sélectionner",
    'reverse selection': "inverser la sélection",
    'delete': "supprimer",
    'file exists':'Un fichier portant ce nom existe déjà'
}


const acceptedMimeTypes = [
    '.natbraille'
]

const acceptedImportMimeTypes = [
    '.odt', 'application/vnd.oasis.opendocument.text',
    ".txt", "text/plain",
    ".rtf", "application/rtf",
    ".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    ".doc", "application/msword",
]

const IMPORT_STATE_EMPTY = 0;
const IMPORT_STATE_FILE_UPLOAD_TO_BROWSER_STARTED = 1;
const IMPORT_STATE_FILE_UPLOAD_TO_BROWSER_DONE = 2;
const IMPORT_STATE_IMPORT_STARTED = 3;
const IMPORT_STATE_IMPORT_FAILED = 4;
const IMPORT_STATE_IMPORT_DONE = 5;
const IMPORT_STATE_IMPORT_UPLOADED_TO_SERVER = 6;

const ImportStateMessage = new Map([
    [IMPORT_STATE_EMPTY, ""],
    [IMPORT_STATE_FILE_UPLOAD_TO_BROWSER_STARTED, "le chargement du fichier commencé"],
    [IMPORT_STATE_FILE_UPLOAD_TO_BROWSER_DONE, "chargement du fichier terminé"],
    [IMPORT_STATE_IMPORT_STARTED, "import en cours"],
    [IMPORT_STATE_IMPORT_FAILED, "échec de l'import"],
    [IMPORT_STATE_IMPORT_DONE, "import terminé"],
    [IMPORT_STATE_IMPORT_UPLOADED_TO_SERVER, "ok"]
])

export const DocumentManagement = () => {


    const inputFileRef = useRef()
    const inputUploadFileRef = useRef()
    const [importState, setImportState] = useState(IMPORT_STATE_EMPTY)
    const [userDocumentsList, setUserDocumentsList] = useState([]);

    const editedFileContext = useEditedFileContext()

    const navigate = useNavigate();

    const addToDocumentList = document => {
        setUserDocumentsList([...new Set([document, ...userDocumentsList])])
    }
    const fetchUserDocumentList = async () => {
        // const docs = await getUserDocumentKindList(getNatbrailleUsernameFromCookie(), "natbraille")
        const docs = await getUserNatbrailleDocumentList()
        setUserDocumentsList(docs)
    }
    useEffect(() => {
        fetchUserDocumentList()
        return () => { }
    }, [])

    // const filename = createUniqueFilename(file.name, userDocumentsList)
    // await postUserDocument(getNatbrailleUsernameFromCookie(), "odt", filename, arrayBuffer)

    const [newDocumentFilename, setNewDocumentFilename] = useState('')
    const documentUserFilenameChanged = (e) => {
        const userFilename = e.target.value || ''
        if (userFilename.length > 0) {
            let filename = userFilename
            if (!(filename.endsWith(".natbraille"))) {
                filename = `${filename}.natbraille`
            }
            setNewDocumentFilename(filename)
        } else {
            setNewDocumentFilename('')
        }
    }
    const createNewDocument = async () => {
        if (newDocumentFilename.length === 0)
            return

        console.log('create', newDocumentFilename)

        // get filename and clear input 
        const filename = newDocumentFilename
        setNewDocumentFilename('')

        // new document from xml string
        const documentString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><html><body><p></p></body></html>"
        const textEncoder = new TextEncoder()
        const arrayBuffer = textEncoder.encode(documentString);

        // save document and load in editor
        await postUserNatbrailleDocument(filename, arrayBuffer)
        await loadInEditor(filename)
    }

    const loadInEditor = async filename => {
        // load document        
        //const arrayBuffer = await getUserDocument(getNatbrailleUsernameFromCookie(), 'natbraille', filename)
        const arrayBuffer = await getUserNatbrailleDocument(filename)
        const decoder = new TextDecoder('utf-8')
        const documentString = decoder.decode(arrayBuffer)

        // put in local storage
        console.log("documentString", documentString)
        putNatbrailleDocumentToLocalStorage(filename, documentString)

        // remove previous work document
        removeEditorProgressFromLocalStorage()

        //
        editedFileContext.setEditedFileContextFilename(filename)

        // navigate
        navigateToEditorFile(navigate)(filename)
    }


    const fileUploaded = async file => {
        // get file bytes
        const arrayBuffer = await loadArrayBuffer(file);

        // update black document list
        await fetchUserDocumentList()

        // get unique name
        const blackFilename = createUniqueFilename(`${file.name}`, userDocumentsList)

        // upload black document to server
        // await postUserDocument(getNatbrailleUsernameFromCookie(), "natbraille", blackFilename, arrayBuffer)
        await postUserNatbrailleDocument(blackFilename, arrayBuffer)

        // update document list
        addToDocumentList(blackFilename)

        // load to editor
        loadInEditor(blackFilename)
    }
    const fileImportUploaded = async file => {

        setImportState(IMPORT_STATE_FILE_UPLOAD_TO_BROWSER_STARTED)
        const arrayBuffer = await loadArrayBuffer(file);
        setImportState(IMPORT_STATE_FILE_UPLOAD_TO_BROWSER_DONE)

        try {
            setImportState(IMPORT_STATE_IMPORT_STARTED)

            // update black document list
            await fetchUserDocumentList()

            // convert to black document
            const blackFilename = createUniqueFilename(`${file.name}.natbraille`, userDocumentsList)
            console.log(blackFilename);

            let odtArrayBuffer;
            if (file.type == 'application/vnd.oasis.opendocument.text') {
                odtArrayBuffer = arrayBuffer
            } else {
                // convert * to odt
                odtArrayBuffer = await postAnyToOdt(arrayBuffer)
            }
            const black = await postOdtToBlack(odtArrayBuffer)
            console.log(blackFilename, black);

            setImportState(IMPORT_STATE_IMPORT_DONE)

            // upload black document to server
            // await postUserDocument(getNatbrailleUsernameFromCookie(), "natbraille", blackFilename, black)
            await postUserNatbrailleDocument(blackFilename, black)
            setImportState(IMPORT_STATE_IMPORT_UPLOADED_TO_SERVER)

            // update document list
            addToDocumentList(blackFilename)

            // load to editor
            loadInEditor(blackFilename)
        } catch (e) {
            console.error(e)
            // import error
            setImportState(IMPORT_STATE_IMPORT_FAILED)
        }

        // reset file input element
        inputUploadFileRef.current.value = null;

    }
    const [checkedDocuments, setCheckedDocuments] = useState([])
    const checkboxChanged = name => e => {
        console.log('change', name, e.target.checked)
        if (checkedDocuments.includes(name)) {
            setCheckedDocuments(checkedDocuments.filter(x => x !== name))
        } else {
            setCheckedDocuments([...checkedDocuments, name])
        }
    }
    const selectAll = () => {
        setCheckedDocuments([...userDocumentsList])
    }
    const reverseSelection = () => {
        setCheckedDocuments(userDocumentsList.filter(x => !(checkedDocuments.includes(x))))
    }
    const batchRemove = async () => {
        console.log("removes")
        //const deletes = checkedDocuments.map(filename => deleteUserDocument(getNatbrailleUsernameFromCookie(), 'natbraille', filename))
        const deletes = checkedDocuments.map(filename => deleteUserNatbrailleDocument(filename))
        await Promise.all(deletes)
        fetchUserDocumentList();
    }
    return <div id="document-management">
        <div className="document-file-new document-management-block">
            <h1><i className="bi bi-file-earmark"></i>{Labels['new document']}</h1>
            <div className="document-management-block-body">
                <div className="input-group">
                    <input type="text" className="form-control" placeholder="nouveau document.natbraille" aria-label="nouveau document"
                        onChange={documentUserFilenameChanged}
                    />
                    <div className="input-group-append">
                        <button className="btn btn-primary" type="button" onClick={createNewDocument}
                            disabled={(newDocumentFilename.length === 0) || userDocumentsList.includes(newDocumentFilename)}>
                            {Labels["create"]}
                        </button>
                    </div>
                </div>
                <div className="file-exists-message">
                    {userDocumentsList.includes(newDocumentFilename) && (
                        <div class="alert alert-warning" role="alert">{Labels['file exists']}</div>
                        
                    )}
                </div>
            </div>
        </div>
        <div className="document-file-import document-management-block">
            <h1><i className="bi bi-upload"></i>{Labels["import black"]}</h1>
            <div className="document-file-input document-management-block-body">
                <label >
                    {/*
                    <p>(Seuls les fichiers odt sont pris en compte pour le moment)</p>
                    <p>Cliquez pour choisir le fichier à importer</p>
*/}
                    <input
                        ref={inputUploadFileRef}
                        type="file"
                        accept={acceptedImportMimeTypes.join(", ")}
                        multiple
                        onChange={e => fileImportUploaded(e.target.files[0])}
                    />
                </label>
                {/*              <p>État de l'import: <span className="document-file-import-state">{ImportStateMessage.get(importState)}</span></p>
   */}
            </div>
        </div>
        {/*
        <div className="document-file-open document-management-block">
            <h1><i className="bi bi-file-earmark-plus"></i>ouvrir une transcription locale</h1>
            <div className="document-file-input document-management-block-body">
                <label >
                    <input
                        ref={inputFileRef}
                        type="file"
                        accept={acceptedMimeTypes.join(", ")}
                        multiple
                        onChange={e => fileUploaded(e.target.files[0])}
                    />
                </label>
       
            </div>
        </div>
    */}
        <div className="document-recently-used document-management-block">
            <h1><i className="bi bi-files"></i>{Labels["recent transcriptions"]}</h1>
            <div className="document-management-block-body">
                <table className="document-recently-used-list table table-striped">
                    <thead>
                        <tr>
                            <td></td><td></td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            userDocumentsList.map(name => {
                                return <tr className="document-recently-used-item" key={name} >

                                    <td>
                                        <input type="checkbox" checked={checkedDocuments.includes(name)} onChange={checkboxChanged(name)}></input>
                                    </td>
                                    <td>
                                        <span role="link" onClick={() => loadInEditor(name)}>{name}</span>
                                    </td>
                                    <td>
                                        <button className="btn btn-small btn-primary document-open-button" role="link" onClick={() => loadInEditor(name)}>{Labels["open"]}</button>
                                    </td>

                                </tr>
                            })
                        }
                    </tbody>
                </table>
                {/*
            <ul className="document-recently-used-list">
                {
                    userDocumentsList.map(name => {
                        return <li className="document-recently-used-item" key={name} >
                            <label>
                                <input type="checkbox" checked={checkedDocuments.includes(name)} onChange={checkboxChanged(name)}></input>
                                <span role="link" onClick={() => loadInEditor(name)}>{name}</span>
                            </label>
                        </li>
                    })
                }
            </ul>
             */}
                <div className="document-recently-used-batch-actions">
                    <button className="btn btn-primary btn-sm" onClick={selectAll}>{Labels["select all"]}</button>
                    <button className="btn btn-primary btn-sm" onClick={reverseSelection}>{Labels["reverse selection"]}</button>
                    <button className="btn btn-primary btn-sm" onClick={batchRemove}>{Labels["delete"]}</button>
                </div>
            </div>
        </div>
    </div>
}