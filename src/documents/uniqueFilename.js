const filenameStemExtensionRegexp = /((?<stem>.+?)(\.(?<extension>[^.]+))?)$/
export const filenameStemExtension = filename => filename.match(filenameStemExtensionRegexp)?.groups

const filenameStemCounterRegexp = /(?<nocounter>.*?)(\((?<counter>\d+)\))?$/
const filenameStemCounter = filename => filename.match(filenameStemCounterRegexp)?.groups

export const createUniqueFilename = (baseFilename = "sans nom", blackList) => {
    //
    // given a file of the form "file(22).odt"
    // return first increment of (22), i.e. "file(23).odt", "file(24).odt"...
    // that is not in the blacklist
    // 
    let uniqueFilename = baseFilename

    if (blackList.includes(uniqueFilename)) {
        const { stem, extension } = filenameStemExtension(baseFilename) || {}
        const { nocounter, counter } = filenameStemCounter(stem)
        console.log({ stem, extension }, { nocounter, counter })
        let intCounter = parseInt(counter) || 0
        do {
            intCounter++;
            uniqueFilename = `${nocounter}(${intCounter.toString(10)})`
            if (extension)
                uniqueFilename += `.${extension}`
        } while (blackList.includes(uniqueFilename))
    }
    return uniqueFilename
}
