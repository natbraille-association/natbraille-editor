import { getBrailleTables } from "../serverio/brailleTables"

export const UnicodeBraille6DotsCharacters = Array(0x283f - 0x2800 + 1).fill(0).map((_, i) => String.fromCharCode(0x2800 + i))

export class BrailleTable {
    #toUnicodeMap = new Map()
    #fromUnicodeMap = new Map()
    constructor(name, codeList) {
        this.name = name
        for (let i = 0x2800; i <= 0x283f; i++) {
            const unicodeChar = String.fromCharCode(i)
            const tableChar = String.fromCharCode(codeList[i - 0x2800])
            this.#toUnicodeMap.set(tableChar, unicodeChar)
            this.#fromUnicodeMap.set(unicodeChar, tableChar)
        }
    }
    toUnicode(from) {
        let to = ''
        for (let i = 0; i < from.length; i++) {
            to += this.#toUnicodeMap.get(from[i])
        }
        return to
    }
    fromUnicode(from) {
        let to = ''
        for (let i = 0; i < from.length; i++) {
            to += this.#fromUnicodeMap.get(from[i])
        }
        return to
    }
}

export class BrailleTables {
    tables = new Map()
    add(brailleTable) {
        this.tables.set(brailleTable.name, brailleTable)
    }
    get(name) {
        return this.tables.get(name)
    }
    get names() {
        return [...this.tables.keys()]
    }
}

export const loadBrailleTables = async () => {
    const brailleTables = new BrailleTables()
    const data = await getBrailleTables() // load from server
    Object.entries(data).forEach(([name, codeList]) => {
        brailleTables.add(new BrailleTable(name, codeList))
    })
    console.log('I am the braille tables', brailleTables)
    return brailleTables
}