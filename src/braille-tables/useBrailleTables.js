import { useState, useEffect } from 'react';
import { loadBrailleTables } from './brailleTable';

const useBrailleTables = () => {
  const [brailleTables, setBrailleTables] = useState(null);
  useEffect(() => {   
    const fetchBrailleTable = async () => {
      try {
        const brailleTables = await loadBrailleTables()
        setBrailleTables(brailleTables);
      } catch (error) {
        console.error('Error fetching braille tables', error);
      }
    };
    fetchBrailleTable();
    return () => {};
  }, []); 
  return brailleTables;
};

export default useBrailleTables;