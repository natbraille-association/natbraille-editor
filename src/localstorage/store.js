/**
 * editor progress
 */
const EditorProgressKey = "natbraille-editor-progress"
export const saveEditorProgressToLocalStorage = o => {
    localStorage.setItem(EditorProgressKey, JSON.stringify(o))
}
export const loadEditorProgressFromLocalStorage = () => {
    console.log('load from local storage')
    const o = localStorage.getItem(EditorProgressKey)
    if (o === null) {
        return undefined
    } else {
        return JSON.parse(o)
    }
}
export const removeEditorProgressFromLocalStorage = () => {
    console.log('remove from local storage')
    localStorage.removeItem(EditorProgressKey)
}


/**
 * editor xml files
 */
import unique from 'uniq'
import { DefaultUIOptions, UIOptionsVersion } from '../context/uiOptions'
const EditorFilesDirectoryKey = `natbraille-editor-files-directory`
export const editorFilesDirectoryAdd = fileKey => {
    const o = localStorage.getItem(EditorFilesDirectoryKey);
    let list;
    if (o === null) {
        list = []
    } else {
        list = JSON.parse(o)
    }
    const newList = unique([fileKey, ...list])
    localStorage.setItem(EditorFilesDirectoryKey, JSON.stringify(newList));
}
export const editorFilesDirectoryRemove = fileKey => {
    const o = localStorage.getItem(EditorFilesDirectoryKey);
    let list;
    if (o === null) {
        list = []
    } else {
        list = JSON.parse(o)
    }
    const newList = list.filter(x => x !== fileKey)
    localStorage.setItem(EditorFilesDirectoryKey, JSON.stringify(newList));
}

const EditorFileKey = filename => `natbraille-editor-file-${filename}`

export const putNatbrailleDocumentToLocalStorage = (filename, arrayBuffer) => {
    const key = EditorFileKey(filename)
    editorFilesDirectoryAdd(key)
    localStorage.setItem(key, JSON.stringify(arrayBuffer))
}

export const getNatbrailleDocumentFromLocalStorage = (filename) => {
    const key = EditorFileKey(filename)
    const o = localStorage.getItem(key)
    if (o === null) {
        console.error("no such document " + filename)
        throw new Error("no such document")
    } else {
        return JSON.parse(o)
    }
}

/**
 *  Options
 */

const restrictToExistingNonDefaultOptions = (options, defaultOptions) => {
    console.log({ options, defaultOptions })
    const nonDefaultExistingOptions = {}
    Object.keys(defaultOptions).forEach(k => {
        if (options.hasOwnProperty(k)) {
            const v = options[k]
            if (v !== defaultOptions[k]) {
                nonDefaultExistingOptions[k] = v
            }
        }
    })
    return nonDefaultExistingOptions
}

/**
 * UI options
 */

const UIOptionsKey = version => `natbraille-ui-options-${version}`
export const loadUIOptions = () => {
    // agglomerate all option values from previous versions, 
    // keeping most recent
    let agglomerate = {}
    for (let v = 1; v <= UIOptionsVersion; v++) {
        const key = UIOptionsKey(v)
        const os = localStorage.getItem(key)
        if (os !== null) {
            try {
                const o = JSON.parse(os)
                agglomerate = { ...agglomerate, ...o }
            } catch (e) {
                console.error(e)
            }
        }
    }
    const options = {}
    // set only non default option value amongst 
    // last-version options
    Object.keys(DefaultUIOptions).forEach(k => {
        if (agglomerate.hasOwnProperty(k)) {
            options[k] = agglomerate[k]
        }
    })
    return options;
}

export const saveUIOptions = (uiOptions) => {
    const nonDefaultExistingOptions = restrictToExistingNonDefaultOptions(uiOptions, DefaultUIOptions)
    console.log('save ui options to localstorage', nonDefaultExistingOptions)
    localStorage.setItem(UIOptionsKey(UIOptionsVersion), JSON.stringify(nonDefaultExistingOptions))
}

/**
 * Nat Filter Options
 */

const NatFilterOptionsKey = `natbraille-nat-filter-options`

import filterOptionsMetadata from "../formats/filterOptionsMetadata.json"
const DefaultNatFilterOptions = Object.fromEntries(Object.entries(filterOptionsMetadata).map(([name, { defaultValue }]) => {
    return [name, defaultValue]
}))

export const loadNatFilterOptions = () => {
    const os = localStorage.getItem(NatFilterOptionsKey)
    if (os !== null) {
        try {
            return JSON.parse(os)
        } catch (e) {
            console.error(e)
        }
    }
    return {}
}
export const saveNatFilterOptions = (natFilterOptions) => {
    const nonDefaultExistingOptions = restrictToExistingNonDefaultOptions(natFilterOptions, DefaultNatFilterOptions)
    localStorage.setItem(NatFilterOptionsKey, JSON.stringify(nonDefaultExistingOptions))
}

/**
 * Edited File
 */

const EditedFileKey = `natbraille-edited-file`

// DefaultEditedFile are  defined at the level of the Context
const restrictToReplaceOnly = (defaultEditedFile, values) => {
    return Object.fromEntries(Object.keys(defaultEditedFile).map(key => {
        if (values.hasOwnProperty(key)) {
            return [key, values[key]]
        } else {
            return [key, defaultEditedFile[key]]
        }
    }))
}
export const loadEditedFile = (defaultEditedFile) => {
    const os = localStorage.getItem(EditedFileKey)
    if (os !== null) {
        try {
            return restrictToReplaceOnly(defaultEditedFile, JSON.parse(os))
        } catch (e) {
            console.error(e)
        }
    }
    return defaultEditedFile
}
export const saveEditedFile = (editedFile) => {
    localStorage.setItem(EditedFileKey, JSON.stringify(editedFile))
}