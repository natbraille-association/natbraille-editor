// see https://developer.mozilla.org/fr/docs/Glossary/Base64
export function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str));
}
export function UnicodeDecodeB64(str) {
    return decodeURIComponent(atob(str));
}

