import { getRestEndpoint, getWsEndpoint } from './endpoints.js'
import { v4 as uuidv4 } from 'uuid';

const waitingTasks = []
const pending = []

/*
 * TranscriptionService implements a request queue over a websocket
 *
 * Each transcription request is mapped to a promise
 */

export const TranscriptionService = () => {

    const wsEndpoint = getWsEndpoint('transcription')
    const socket = new WebSocket(wsEndpoint)
    socket.addEventListener('open', e => {
        maybeDo()
    })
    socket.addEventListener('close', e => {
        reset()
    })
    socket.addEventListener('message', e => {
        const reply = e.data
        readResult(JSON.parse(e.data))
    });

    const readResult = (result) => {
        const [currentlyPending] = pending
        if (currentlyPending?.transcriptionId === result?.transcriptionTask?.transcriptionId) {
            const task = pending.shift()
            task.taskAccept(result)
        } else {
            console.error('this answer answers no question', result)
        }
        maybeDo()
    }
    const doNow = () => {
        const [task] = pending
        if (task) {
            const { transcriptionId, transcription } = task
            const msg = { transcriptionId, transcription }
            socket.send(JSON.stringify(msg))
        }
    }
    const maybeDo = () => {
        if (socket.readyState !== 1) {
            // not OPEN
        } else if (pending.length) {
            // later
        } else {
            const task = waitingTasks.shift()
            if (task) {
                pending.push(task)
                doNow()
            }
        }
    }
    const enqueue = async transcription => {
        const transcriptionId = uuidv4()
        let taskAccept, taskReject
        const promise = new Promise((accept, reject) => {
            taskAccept = accept
            taskReject = reject
        })
        const task = { transcriptionId, transcription, taskAccept, taskReject }
        waitingTasks.push(task)
        console.log({ waitingTasks })
        maybeDo()
        return promise
    }
    const reset = () => {
        waitingTasks.forEach(({ taskReject }) => taskReject('canceled'))
        waitingTasks.length = 0
        pending.forEach(({ taskReject }) => taskReject('canceled'))
        pending.length = 0
    }

    return { enqueue, reset }
}


export const testTranscriptionService = async () => {
    const t = TranscriptionService()
    const [$add, $reset] = ['add', 'reset'].map(t => {
        const $b = document.createElement('button')
        $b.textContent = t
        $b.classList.add('btn')
        $b.classList.add('btn-primary')
        document.body.append($b)
        return $b
    })
    $reset.onclick = () => t.reset()
    $add.onclick = async () => {
        makeOne()
    }
    let count = 0;
    const makeOne = async () => {
        count++
        const thisCount = count
        const letters = 'abcdefghijklmnopqrstuvwxyz'.split('')
        const text = `[${thisCount}]` + Array(10).fill(0).map(i => letters[Math.floor(Math.random() * letters.length)]).join("")
        try {
            const result = await t.enqueue({ text })
            console.log(JSON.stringify(result, null, 2))
            const id = result.transcriptionTask.transcriptionId
            const from = result.transcriptionTask.transcription.text
            const to = result.result
            console.log('got resuslt', id, from, to)
        } catch (e) {
            console.log('there was an e', e, "in", thisCount)
        }
    }
}