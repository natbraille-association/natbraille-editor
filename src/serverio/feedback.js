import { getBlackToOdtEndpoint, getFeedbackFormEndpoint } from "./endpoints"
import { getHeaderApiToken } from "./apiTokenHeaders"

export const postFeedback = async (feedbackObject) => {
    const url = getFeedbackFormEndpoint()
    const payload = feedbackObject
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/vnd.oasis.opendocument.text',
            'Content-Type': 'application/json',
            ...getHeaderApiToken()
        },
        method: "POST",
        body: JSON.stringify(payload)
    })        
    console.log("fetched.status",fetched.status);
    return fetched.status == 200;
}
