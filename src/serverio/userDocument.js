import { getUserArchiveEndpoint, getUserDocumentEndpoint } from '../serverio/endpoints.js'
import { getNatbrailleUsernameFromCookie } from './cookies.js'

//
// POST a user document as a byte array
//
export const postUserDocument = async (username, kind, filename, data) => {
    const url = getUserDocumentEndpoint(username, kind, filename)
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'text/plain',
            'Content-Type': 'application/octet-stream'
        },
        method: "POST",
        body: data
    })
    const result = await fetched.text()
    return result === 'true'
}
export const postUserDocumentJSON = async (username, kind, filename, data) => {
    const url = getUserDocumentEndpoint(username, kind, filename)
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'text/plain',
            'Content-Type': 'application/octet-stream' // server will read the json data as octet stream
        },
        method: "POST",
        body: JSON.stringify(data || {})
    })
    const result = await fetched.text()
    return result === 'true'
}

//
// GET a user document as a byte array
//
// decode to text string using : new TextDecoder("utf-8")).decode(fetched) 
//
export const getUserDocument = async (username, kind, filename) => {
    const url = getUserDocumentEndpoint(username, kind, filename)
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/octet-stream',
        },
    })
    const result = await fetched.arrayBuffer()
    return result
}
export const getUserDocumentJSON = async (username, kind, filename) => {
    const url = getUserDocumentEndpoint(username, kind, filename)
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/octet-stream',
        },
    })
    const result = await fetched.json() // the octect-stream is read as json
    return result
}

//
// DELETE a user document 
//
export const deleteUserDocument = async (username, kind, filename) => {
    const url = getUserDocumentEndpoint(username, kind, filename)
    const fetched = await fetch(url, {
        headers: {
        },
        method: "DELETE",
    })    
}

//
// GET user document kind list
//
export const getUserDocumentKindList = async (username, kind) => {
    const url = getUserDocumentEndpoint(username, kind)
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/json',
        },
    })
    const result = await fetched.json()
    return result
}

//
// GET user documents archive
//

export const getUserDocumentArchive = async username => {
    const url = getUserArchiveEndpoint(username)
    //const url = getUserDocumentEndpoint(username, "archive.zip")
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/zip',
        },
    })
    const result = await fetched.arrayBuffer();
    return result
}

//
// specialisations
//
const NATBRAILLE_DOCUMENT_KIND = "natbraille"
export const getUserNatbrailleDocumentList = () => getUserDocumentKindList(getNatbrailleUsernameFromCookie(), NATBRAILLE_DOCUMENT_KIND)
export const getUserNatbrailleDocument = filename => getUserDocument(getNatbrailleUsernameFromCookie(), NATBRAILLE_DOCUMENT_KIND, filename)
export const postUserNatbrailleDocument = (filename, arrayBuffer) => postUserDocument(getNatbrailleUsernameFromCookie(), NATBRAILLE_DOCUMENT_KIND, filename, arrayBuffer)
export const deleteUserNatbrailleDocument = filename => deleteUserDocument(getNatbrailleUsernameFromCookie(), NATBRAILLE_DOCUMENT_KIND, filename)

const NATBRAILLE_CONFIGURATION_KIND = "configurations"
export const getUserConfigurationList = () => getUserDocumentKindList(getNatbrailleUsernameFromCookie(), NATBRAILLE_CONFIGURATION_KIND)
export const getUserConfiguration = filename => getUserDocumentJSON(getNatbrailleUsernameFromCookie(), NATBRAILLE_CONFIGURATION_KIND, filename)
export const postUserConfiguration = (filename, o) => postUserDocumentJSON(getNatbrailleUsernameFromCookie(), NATBRAILLE_CONFIGURATION_KIND, filename, o)
export const deleteUserConfiguration = filename => deleteUserDocument(getNatbrailleUsernameFromCookie(), NATBRAILLE_CONFIGURATION_KIND, filename)

