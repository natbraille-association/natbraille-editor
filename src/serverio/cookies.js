import { UnicodeDecodeB64 } from "./urlCode";

export function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return UnicodeDecodeB64(parts.pop().split(';').shift());
}
export const getNatbrailleTokenFromCookie = () => getCookie("natbraille-token")
export const getNatbrailleUsernameFromCookie = () => getCookie("natbraille-username")
export const getNatbrailleRegisterEmailSend = () => getCookie("natbraille-register-email-send")
export const getNatbrailleBadLogin = () => getCookie("natbraille-bad-login")
export const tokenSaysUserIsConnected = () => {
    const token = getNatbrailleTokenFromCookie()
    const connected = (token?.length > 0)
    return connected
}

export function removeCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);

    if (parts.length === 2) {
        // Set the cookie with an expiration date in the past to remove it
        document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
    }
}
export function removeCookies() {
    ;[
        "natbraille-token",
        "natbraille-username",
        "natbraille-register-email-send",
        "natbraille-bad-login"
    ].forEach(removeCookie)
}