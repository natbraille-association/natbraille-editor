import { ClientConfig } from "../client-config.js"
import { b64EncodeUnicode } from './urlCode.js'

export const getRestEndpoint = pathname => {
    // because the server hostname and port can be different from
    // the one used in developpement, but not in production, we use 
    // the window.location URL and only change needed part
    const location = new URL(window.location) // clone
    if (ClientConfig?.server?.hostname) location.hostname = ClientConfig.server.hostname
    if (ClientConfig?.server?.port) location.port = ClientConfig.server.port
    location.pathname = ['api', pathname].join('/')
    return location.toString()
}
// document management 

// TODO no need to filter undefined. 
export const getUserDocumentEndpoint = (username, kind, filename) => getRestEndpoint(['users', ...[username, kind, filename].filter(x => x).map(b64EncodeUnicode)].join('/'))
export const getUserArchiveEndpoint = (username) => getRestEndpoint(['users', b64EncodeUnicode(username), "archive.zip"].join('/'))


// login / logout / register
export const getAuthEndpoint = action => getRestEndpoint('auth/' + action)

export const getLoginEndpoint = () => getAuthEndpoint('login')
export const getLogoutEndpoint = () => getAuthEndpoint('logout')
export const getRegisterEndpoint = () => getAuthEndpoint('register')
export const getSendRegistrationEmailEndpoint = () => getAuthEndpoint('register-send-email')
export const getSendRegistrationChallengeEndpoint = () => getAuthEndpoint('challenge')
export const getRequestPasswordResetEndpoint = () => getAuthEndpoint('request-password-reset')
export const getPasswordResetEndpoint = () => getAuthEndpoint('password-reset')
export const getUserEmailEndpoint = () => getAuthEndpoint('user-email')
export const getAccountRemovalRequestEndpoint = () => getAuthEndpoint('account-removal-request')
export const getAccountRemovalEndpoint = () => getAuthEndpoint('account-removal')

// check connection
export const getCheckLiveTokenEndpoint = () => getAuthEndpoint('check-live-token')

// transcription
export const getBlackToBrailleFullEndpoint = () => getRestEndpoint('to-braille')

// export
export const getBlackToOdtEndpoint = () => getRestEndpoint('to-odt')

// import
export const getOdtToBlackEndpoint = () => getRestEndpoint('odt-to-noir')
export const getAnyToOdtEndpoint = () => getRestEndpoint('any-to-odt')

// performance
export const getPerformanceEndpoint = () => getRestEndpoint('performance')
export const getPerUserPerformanceEndpoint = () => getRestEndpoint('user-performance')

// starmath to mathml
export const getStarMathToMathMLEndpoint = () => getRestEndpoint('starmath-to-mathml')

// braille + options to html (detranscription)
export const getBrailleToHtmlEndpoint = () => getRestEndpoint('braille-to-html')

// braille tables
export const getBrailleTablesEndpoint = () => getRestEndpoint('braille-tables')

// feedback form
export const getFeedbackFormEndpoint = () => getRestEndpoint('feedback-form')

// server version
export const getServerVersionEndpoint = () => getRestEndpoint('version')

// client version
export const getClientVersionEndpoint = () => '/git-version.json'


// webservice (unused)
export const getWsEndpoint = pathname => {
    // http -> ws
    // https -> wss
    return getRestEndpoint(pathname).replace(/^http/, 'ws')
}



