import { getBlackToOdtEndpoint } from "./endpoints"
import { getHeaderApiToken } from "./apiTokenHeaders"

export const postBlackToOdt = async (xmlString) => {
    const url = getBlackToOdtEndpoint()
    const payload = { xmlString: xmlString }
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/vnd.oasis.opendocument.text',
            'Content-Type': 'application/json',
            ...getHeaderApiToken()
        },
        method: "POST",
        body: JSON.stringify(payload)
    })
    const result = await fetched.arrayBuffer();
    return result
}
