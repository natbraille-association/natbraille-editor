import { getBlackToBrailleFullEndpoint, getBrailleToHtmlEndpoint, getStarMathToMathMLEndpoint } from "./endpoints"
import { getHeaderApiToken } from "./apiTokenHeaders"

export const postBlackToBrailleTranscription = async (xmlString, filterOptions) => {
    const url = getBlackToBrailleFullEndpoint()
    const payload = { xmlString: xmlString, filterOptions: filterOptions }
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...getHeaderApiToken()
        },
        method: "POST",
        body: JSON.stringify(payload)
    })
    const result = await fetched.json()
    return result
}

export const postStarMathToMathMLTranscription = async (starMathString) => {
    const url = getStarMathToMathMLEndpoint()
    const payload = starMathString
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/xml',
            'Content-Type': 'text/plain;charset=UTF-8',
            ...getHeaderApiToken()
        },
        method: "POST",
        body: payload
    })
    const result = await fetched.text()
    return result
}

export const postBrailleToHtmlTranscription = async (brailleString, filterOptions) => {
    const url = getBrailleToHtmlEndpoint()
    const payload = { brailleString: brailleString, filterOptions: filterOptions }
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            ...getHeaderApiToken()
        },
        method: "POST",
        body: JSON.stringify(payload)
    })
    const result = await fetched.json()
    return result
}


//window.postStarMathToMathMLTranscription = postStarMathToMathMLTranscription
window.postBrailleToHtmlTranscription = postBrailleToHtmlTranscription