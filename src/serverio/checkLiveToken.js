import { getHeaderApiToken } from "./apiTokenHeaders"
import { getCheckLiveTokenEndpoint } from "./endpoints"

export const getCheckLiveTokenIsValid = async (bytes) => {
    const url = getCheckLiveTokenEndpoint()
    const fetched = await fetch(url, {
        headers: {
            'Accept': ' application/json',
            ...getHeaderApiToken()
        },
        method: "GET",
        body: bytes
    })
    const result = await fetched.json();   
    const error = (!result) || (result?.error?.length)
    const valid = !error
    return valid
}
