import { getNatbrailleTokenFromCookie, getNatbrailleUsernameFromCookie } from "./cookies";

export const getHeaderApiToken = () => ({
    'X-Natbraille-Api-Token': getNatbrailleTokenFromCookie(),
    'X-Natbraille-Api-Username': getNatbrailleUsernameFromCookie()
})
