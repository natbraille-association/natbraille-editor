import { getHeaderApiToken } from "./apiTokenHeaders"
import { getAccountRemovalRequestEndpoint, getUserEmailEndpoint } from "./endpoints";

export const getUserEmail = async () => {
    const url = getUserEmailEndpoint()
    const fetched = await fetch(url, {
        headers: {
            'Accept': ' application/json',
            ...getHeaderApiToken()
        },
        method: "GET",
    })
    const result = await fetched.json();   
    return result
}

export const postAccountRemovalRequest = async () => {
    const url = getAccountRemovalRequestEndpoint()
    const fetched = await fetch(url, {
        headers: {
            'Accept': ' application/json',
            ...getHeaderApiToken()
        },
        method: "POST",
    })
    const result = await fetched.json();   
    return result
}
