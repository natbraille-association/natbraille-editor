import { getHeaderApiToken } from "./apiTokenHeaders"
import { getClientVersionEndpoint, getServerVersionEndpoint } from "./endpoints"

export const getServerVersion = async (bytes) => {
    const url = getServerVersionEndpoint()
    const fetched = await fetch(url, {
        headers: {
            'Accept': ' application/json',
            ...getHeaderApiToken()
        },
        method: "GET",
    })
    const result = await fetched.json();
    return result
}
export const getClientVersion = async (bytes) => {
    const url = getClientVersionEndpoint()
    const fetched = await fetch(url, {
        headers: {
            'Accept': ' application/json',
        },
        method: "GET",
    })
    const result = await fetched.json();
    return result
}