import { getHeaderApiToken } from "./apiTokenHeaders"
import { getBrailleTablesEndpoint } from "./endpoints"

export const getBrailleTables = async (bytes) => {
    const url = getBrailleTablesEndpoint()
    const fetched = await fetch(url, {
        headers: {
            'Accept': ' application/json',
            ...getHeaderApiToken()
        },
        method: "GET",
        body: bytes
    })
    const result = await fetched.json();
    return result
}