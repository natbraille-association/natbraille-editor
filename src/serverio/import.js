import { getHeaderApiToken } from "./apiTokenHeaders"
import { getAnyToOdtEndpoint, getOdtToBlackEndpoint } from "./endpoints"

export const postOdtToBlack = async (bytes) => {
    const url = getOdtToBlackEndpoint()
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/xml',
            'Content-Type': 'application/vnd.oasis.opendocument.text',
            ...getHeaderApiToken()
        },
        method: "POST",
        body: bytes
    })
    const result = await fetched.text();
    return result
}

export const postAnyToOdt = async (bytes, mimeType) => {
    const url = getAnyToOdtEndpoint()
    const fetched = await fetch(url, {
        headers: {
            'Accept': 'application/octet-stream',
            'Content-Type': mimeType,
            ...getHeaderApiToken()
        },
        method: "POST",
        body: bytes
    })
    const result = await fetched.arrayBuffer();
    return result
}
