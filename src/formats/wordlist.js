/*
const exemple_list = `aïeul true false
aïeule true true
aïeules true false
aïeuls false true
aïeux false false
allô true false
batz true false
bê true false
binz true false
bisaïeul true false
bisaïeule true false
bisaïeules true false
bisaïeuls true false
crû true false
fritz true false
führer true false
führers true false
glaïeul true false
glaïeuls true false
inouïe true false
inouïes true false
interviewer true false
jazz true false
klaxon true false
ô true false
ouïe true false
ouïes true false
quartz true false
us true false`
*/
export const parseWordListText = text => {
    const parseWorldListLine = line => {
        const [word, always, ask] = line.split(' ')
        const parseBool = s => (s === 'true')
        return ({
            word,
            always: parseBool(always),
            ask: parseBool(ask)
        })
    }
    return text.split(/\r|\n/)
        .map(l => l.trim())
        .filter(l => l && l.length)
        .map(l => parseWorldListLine(l));
}

export const exportWordListText = a => {
    const exportBool = b => b ? 'true' : 'false'
    const exportWordListLine = ({ word, always, ask }) => [word, exportBool(always), exportBool(ask)].join(" ")
    return a.map(exportWordListLine).join("\n")
}