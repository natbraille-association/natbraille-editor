const KnownMimeType = {
    'application/vnd.oasis.opendocument.text': { display: 'OpenDocument Text', extensions: ['odt'] },
    'text/plain': { display: 'Text', extensions: ['txt'] },
    'application/msword': { display: 'Microsoft Word', extensions: ['doc'] },
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': { display: 'Microsoft Word', extensions: ['docx'] }
}
export const getDisplayNameForMimeType = mimeType => {
    return KnownMimeType[mimeType]?.display || mimeType
}