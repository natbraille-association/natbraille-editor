/*
 * Rules are XML 1.0, not XML 1.1 so we can use browser DOMParser
*/

export const parseFrG2Rule = $rule => {
    const ruleType = $rule.nodeName; // locution / signe / symbole / rule
    const actif = $rule.getAttribute('actif')
    const base = { ruleType, actif, $rule }
    if (ruleType === 'rule') {
        // for : signe / symbole / general / all
        const [ruleFor, desc, ref] = ['for', 'desc', 'ref'].map(n => $rule.querySelector(n).textContent.trim())
        return { ...base, ruleFor, desc, ref }
    } else {
        const [noir, braille] = ['noir', 'braille'].map(n => $rule.querySelector(n).textContent.trim())
        if (ruleType === 'locution') {
            return { ...base, noir, braille }
        } else if (ruleType === 'signe') {
            const pluriel = $rule.getAttribute('pluriel') === 'true'
            return { ...base, noir, braille, pluriel }
        } else if (ruleType === 'symbole') {
            const invariant = $rule.getAttribute('invariant') === 'true'
            const composable = $rule.getAttribute('composable') === 'true'
            return { ...base, noir, braille, invariant, composable }
        } else {
            throw new Error('no such rule nodeName' + $rule.outerHTML)
        }
    }
}
export const parseFrG2Rules = xmlString => {

    const parser = new DOMParser();
    const doc = parser.parseFromString(xmlString, "application/xml");


    const $rulesList = doc.querySelectorAll("rules > *")
    const parsedRules = []
    for (let idx = 0; idx < $rulesList.length; idx++) {
        const $rule = $rulesList[idx]
        parsedRules.push(parseFrG2Rule($rule))
    }
    return parsedRules
}

const fr_g2_rules_skeleton_xml = `<?xml version='1.0' encoding='UTF-8'?>
<rules>
</rules>
`

export const exportFrG2RulesXml = parsedRules => {
    const parser = new DOMParser();
    const doc = parser.parseFromString(fr_g2_rules_skeleton_xml, "application/xml");
    const $rulesList = doc.querySelector("rules")
    parsedRules.forEach(({ $rule, actif }) => {
        const $r = doc.importNode($rule, true)
        $r.setAttribute("actif", actif)
        $rulesList.append("\t")
        $rulesList.append($r)
        $rulesList.append("\n")
    })
    var s = new XMLSerializer();
    return s.serializeToString(doc)
}

/*
    
    var str = s.serializeToString(doc);
*/