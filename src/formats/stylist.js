import * as cheerio from 'cheerio';

const stylist_default_xml = `<?xml version="1.1" encoding="UTF-8"?>

<!-- Fichier de styles par défaut pour NatBraille
vous pouvez l'éditer mais "à vos risques et périls" -->

<styles>
     <style name="standard" mode="3-1" />
     <style name="integral" special="g1" mode="3-1" />
     <style name="poesie" mode="1-3" />
     <style name="noteTr" mode="7-5" prefixe="p6p23" suffixe="p56p3"/>
     <style name="noIndent" mode="1" />
     <style name="heading1" mode="title1" />
     <style name="heading" mode="title" />
     <style name="chemistry" special="chemistry" />
     <style name="chimie" special="chimie" />
     <style name="echap" special="g0" />
     <style name="envers" mode="1" upsidedown="yes" />
</styles>`


const stylist_xml_skeleton = `<?xml version="1.1" encoding="UTF-8"?>

<!-- Fichier de styles par défaut pour NatBraille
vous pouvez l'éditer mais "à vos risques et périls" -->

<styles>
</styles>`


export const stylistAttributeNames = ['name', 'mode', 'special', 'prefixe', 'suffixe', 'upsidedown']

export const parseStylistXml = xmlString => {
    const $ = cheerio.load(xmlString, { xml: true });
    const styles = []
    $('styles style').each((idx, $style) => {
        const style = {}
        stylistAttributeNames.forEach(aName => {
            // each style attribute is set to empty string if undefined
            const value = $style.attribs[aName]
            style[aName] = (value === undefined) ? '' : value
        })
        styles.push(style)
    })
    return styles
}

export const exportSylistXml = styles => {
    const $ = cheerio.load(stylist_xml_skeleton, { xml: true })
    styles.forEach(style => {
        $('styles').append("\t");
        const $el = $('<style>')
        stylistAttributeNames.forEach(aName => {
            const aValue = style[aName]
            // do not write attribute is value is undefined or empty string
            if ((aValue !== undefined) && (aValue !== '')) {
                $el.attr(aName, aValue)
            }
        })
        $('styles').append($el);
        $('styles').append("\n");
    })
    return $.xml()
}
