import React, { useState, useEffect } from 'react';
import UplotReact from 'uplot-react';
import 'uplot/dist/uPlot.min.css';
import { getPerformanceEndpoint, getPerUserPerformanceEndpoint } from '../serverio/endpoints';
import "./performance-monitor.css"
import uniq from 'uniq';

const getDistinctiveHue = (num, count) => {
    return 360 * num / count
}

const PerformanceMonitor = () => {
    const [data, setData] = useState([])
    const [series, setSeries] = useState([])

    useEffect(() => {
        fetch(getPerUserPerformanceEndpoint())
            .then(r => r.json())
            .then(t => {
                const users = Object.keys(t)
                const timestamps = uniq([...(Object.values(t))].flatMap(x => x).map(([t]) => t).sort((a, b) => a - b))
                const timestampsIndexes = new Map()
                timestamps.forEach((timestamp, index) => timestampsIndexes.set(timestamp, index))

                // console.log('timesstamps', timestamps)
                // console.log('timesstampsi', timestampsIndexes)

                const dataSeries = users.map(user => {
                    const serie = []
                    t[user].forEach(([t, value]) => {
                        serie[timestampsIndexes.get(t)] = value
                    })
                    return serie;
                })
                console.log('dataSeries', dataSeries)

                setData([timestamps.map(x => x / 1000), ...dataSeries])

                const optionSeries = users.map((user, index) => ({
                    spanGaps: false,
                    label: user,
                    // stroke: `hsl(${getDistinctiveHue(index, users.length)} 50% 40%)`,
                    fill: `hsl(${getDistinctiveHue(index, users.length)} 50% 40% / .2)`,
                    points: {
                        space: 0,
                        fill: `hsl(${getDistinctiveHue(index, users.length)} 50% 40% )`,
                        stroke: `hsl(${getDistinctiveHue(index, users.length)} 50% 40%)`,
                    },
                }))
                console.log('optionSeries', optionSeries)

                setSeries([{}, ...optionSeries])
            })
            .catch((error) => console.error(error));
    }, [])

    return <div className="performance-monitor">
        <UplotReact
            options={{
                width: window.innerWidth,
                height: 200,
                series: series,
            }}
            data={data}
            onCreate={(chart) => { }}
            onDelete={(chart) => { }}
        />
    </div>;
}
export default PerformanceMonitor;

// options={options}
// target={target}