import './reverse-editor.scss'
import { OptionsContext } from "../context/optionsContext";
import { useContext, useEffect, useRef, useState } from 'react';
import { postBrailleToHtmlTranscription } from '../serverio/transcription';
import { registerPerkinsKeyboard } from './perkins';
import { loadBrailleTables } from '../braille-tables/brailleTable';
import { UI_OPTION_BRAILLE_FONT_SIZE, UI_OPTION_PERKINS_KEYS, getUIOption } from '../context/uiOptions';
import { getNatFilterOptionValue } from '../context/natfilterOptions';
import { DraggableDiv, getSplitViewColumnsCss } from '../editor/ResizeHandle';

const Captions = {
    'do-detranscription': "détranscrire",
    'do-detranscription-full': "détranscrire le braille",
    //'use-perkins': "saisie Perkins (A-Z-D P-O-K)"
    'use-perkins': "saisie Perkins"
}

const defaultPerkinsConfig = {
    active: true,
    dots: ['KeyD', 'KeyW', 'KeyQ', 'KeyK', 'KeyO', 'KeyM']
}

function insertAtCursor($e, text) {
    /**
     * https://stackoverflow.com/questions/11076975/how-to-insert-text-into-the-textarea-at-the-current-cursor-position
     */
    //IE support
    if (document.selection) {
        $e.focus();
        sel = document.selection.createRange();
        sel.text = text;
    }
    //MOZILLA and others
    else if ($e.selectionStart || $e.selectionStart == '0') {
        var startPos = $e.selectionStart;
        var endPos = $e.selectionEnd;
        $e.value = $e.value.substring(0, startPos)
            + text
            + $e.value.substring(endPos, $e.value.length);
        $e.selectionStart = startPos + text.length;
        $e.selectionEnd = startPos + text.length;
    } else {
        $e.value += text;
    }
}

export const ReverseEditorSplit = () => {

    const { options, brailleSource, setBrailleSource, uiOptions } = useContext(OptionsContext)

    const noirRef = useRef(null);

    // braille table
    const [brailleTables, setBrailleTables] = useState({})
    useEffect(() => {
        const load = async () => {
            const tables = await loadBrailleTables()
            setBrailleTables(tables)
            console.log(tables)
        }
        load()
    }, [])

    // perkins
    const [perkinsSetup, setPerkinsSetup] = useState(defaultPerkinsConfig)//{ active: true })

    const togglePerkinks = (e) => {
        setPerkinsSetup({
            ...perkinsSetup,
            active: e.target.checked
        })
        document.querySelector("#reverse-left-panel").focus()
    }

    const onPerkinsCellEmitted = brailleCell => {
        // const brailleTable = brailleTables.get(options.FORMAT_OUTPUT_BRAILLE_TABLE)
        const brailleTableName = getNatFilterOptionValue(options)('FORMAT_OUTPUT_BRAILLE_TABLE')
        const brailleTable = brailleTables.get(brailleTableName)
        insertAtCursor(document.querySelector("#reverse-left-panel"), brailleTable.fromUnicode(brailleCell))
    }

    useEffect(() => {
        if (perkinsSetup.active) {
            const pk = getUIOption(uiOptions)(UI_OPTION_PERKINS_KEYS) 
            const dots = Object.keys(pk.codes).sort().map( k => pk.codes[k])             
            const { unregister, cellEmittedListeners } = registerPerkinsKeyboard(document.querySelector("#reverse-left-panel"), dots)
            cellEmittedListeners.add(onPerkinsCellEmitted)
            return unregister
        }
    })

    // transcription
    const doBrailleToHtmlTranscription = async () => {
        const braille = document.querySelector("#reverse-left-panel").value
        const result = await postBrailleToHtmlTranscription(braille, options)
        const noir = result.htmlString
        console.log('noir', noir)
        const domParser = new window.DOMParser()
        const $noir = domParser.parseFromString(result.htmlString, 'text/html')
        const $elements = [...$noir.querySelector('body').children]
        console.log('$noir', $elements)
        noirRef.current.innerHTML = ''
        $elements.forEach($e => {
            noirRef.current.append($e)
        })
    }
    const brailleSourceChanged = (e) => {
        setBrailleSource(e.target.value)
    }
    const leftPanelStyle = {
        "fontSize": `${getUIOption(uiOptions)(UI_OPTION_BRAILLE_FONT_SIZE)}px`
    }

    const gridTemplateColumnsDefault = `calc(50% - var(--editor-column-gap) / 2 ) var(--editor-column-gap) calc(50% - var(--editor-column-gap) / 2 )`;
    const [gridTemplateColumns, setGridTemplateColumns] = useState(gridTemplateColumnsDefault); 
    const splitEditorRef = useRef(null);   
    const draggableDivMoved = (splitEditorRef) => ({handleLeft,handleRef}) => {    
        setGridTemplateColumns(getSplitViewColumnsCss(splitEditorRef,handleRef,handleLeft))    
      }

    return <div id="split-editor" ref={splitEditorRef} style={{ gridTemplateColumns }}>
        <div id="reverse-common-toolbar">
            <span></span>
        </div>
        <div id="reverse-left-toolbar" className="rounded-bottom-border">
            <div className="natbraille-editor-menubar-subbar">
                <button className="natbraille-editor-menuicon toolbar-item" onClick={doBrailleToHtmlTranscription}
                    title={Captions['do-detranscription']}
                >{Captions['do-detranscription']}</button>
            </div>
            <div className="natbraille-editor-menubar-subbar">
                <label className="form-check-label">
                    <input className="natbraille-editor-menuicon toolbar-item form-check-input" type="checkbox" checked={perkinsSetup.active} onChange={togglePerkinks}
                        title={Captions["use-perkins"]}></input>
                    &nbsp;{Captions["use-perkins"]}
                </label>
            </div>
        </div>
        <textarea id="reverse-left-panel" className="unicode-braille-text" defaultValue={brailleSource} style={leftPanelStyle} onChange={brailleSourceChanged}></textarea>
        <div id="reverse-right-toolbar" className="rounded-bottom-border">
            <span></span>
        </div>
        <div id="reverse-right-panel" ref={noirRef}>
            <span></span>
        </div>
        <div id="reverse-editor-footer">
            <span>&nbsp;</span>
        </div>
        <DraggableDiv id="resize-handle" moved={draggableDivMoved(splitEditorRef)}></DraggableDiv>
    </div >

}