export const registerPerkinsKeyboard = ($element, dots, physical = true) => {

    let brailleCharCode = 0x2800;
    let downCount = 0;
  
    const getDot = e => {
      const k = physical ? (e.code) : (e.key)
      return dots.indexOf(k)
    }
  
    const cellModifiedListeners = new Set();
    const keydown = e => {        
      const dot = getDot(e)
      if (dot !== -1) {
        e.preventDefault()
        if (e.repeat) return
        brailleCharCode |= 1 << dot
        downCount++      
      }
      cellModifiedListeners.forEach(l => l(String.fromCharCode(brailleCharCode)))
    }
  
    const cellEmittedListeners = new Set();
    const keyup = e => {
      if (getDot(e) !== -1) {
        downCount--
        if (downCount === 0) {
          cellEmittedListeners.forEach(l => l(String.fromCharCode(brailleCharCode)))
          brailleCharCode = 0x2800
          e.preventDefault()
        }
      }
    }
  
    $element.addEventListener('keydown', keydown)
    $element.addEventListener('keyup', keyup)
  
    return {
      unregister: () => {
        $element.removeEventListener('keydown', keydown)
        $element.removeEventListener('keydown', keyup)
      },
      cellModifiedListeners,
      cellEmittedListeners
    }
  }
  