import { defineConfig } from 'vite'
import generateFile from 'vite-plugin-generate-file'
import react from '@vitejs/plugin-react'
import * as child from 'child_process';

const git_version_data = () => {
  const commands = [
    ['revision', 'git rev-parse HEAD'],
    ['branch', 'git rev-parse --abbrev-ref HEAD'],
    ['date', 'git log -1 --date=format:"%Y/%m/%d %T" --format="%ad"']
  ]
  try {
    return commands.map(([key, command]) => [key, child.execSync(command).toString().trim()])
  } catch (e) {
    console.warn(e)
    return []
  }
}
export default defineConfig({

  // uncomment to get unmangled & source maps in prod
  /*
  build: {
    sourcemap: true, // Enable source maps
    cssCodeSplit: false, // Disable CSS code splitting
    terserOptions: {
      compress: false, // Disable compression
      mangle: false, // Disable mangling
    },
    rollupOptions: {
      output: {
        compact: false, // Disable compacting of output
      },
    },
    minify: false,   // Disable minification
    brotliSize: false, // Disable Brotli compression
  },
  */
  plugins: [
    generateFile([{
      type: 'json',
      output: './git-version.json',
      data: git_version_data(),
    }]),
    react()
  ],
})
