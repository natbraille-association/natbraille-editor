# A web gui for Natbraille

    This software is still under active development and may not be usable at this stage

- develop : `npm run serve`
- build  : `npm run build`

# manuals 

- react : https://react.dev/reference/react
- bootstrap 
    - setup : https://getbootstrap.com/docs/5.3/getting-started/webpack/

# aria and title

https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/heading_role

test : 
    
        <div role="heading" aria-level="1">This is a main page heading</div>

# websocket order

https://stackoverflow.com/questions/11804721/can-websocket-messages-arrive-out-of-order

=> alway in order

# Prosemirror

https://prosemirror.net/examples/

manual : https://prosemirror.net/docs/ref/   

async query plugin : https://github.com/lukesmurray/prosemirror-async-query

# parse xml 1.1 in the browser

## browser support (`DOMParser`)

cannot browse xml version="1.1"

## fast-xml-parser

parse/build to/from a js object structure -> info loss

- fast-xml-parser : https://www.npmjs.com/package/fast-xml-parser
- https://github.com/NaturalIntelligence/fast-xml-parser/blob/HEAD/docs/v4/2.XMLparseOptions.md

## libxmljs

-> for node only (use local binary) ?
Module not found: Error: Can't resolve 'path' in '/home/vivien/pr-src/natbraille-editor/node_modules/bindings'

## Libxmljs2

LibXML bindings for node.js This package was forked as the original one is fairly unmaintained.

- https://www.npmjs.com/package/libxmljs2

http://github.com/marudor/libxmljs2/wiki.
Module not found: Error: Can't resolve 'path' in '/home/vivien/pr-src/natbraille-editor/node_modules/bindings'

## xmldoc

- https://www.npmjs.com/package/xmldoc
- https://github.com/nfarina/xmldoc

npm install buffer stream xmldoc

utilise sax-js

ne génère pas de xml

## sax-js callback-based parser,

- https://github.com/isaacs/sax-js

## js-dom

- https://github.com/jsdom/jsdom

 the goal of the project is to emulate enough of a subset of a web browser to be useful for testing and scraping real-world web applications.

## cheerio

- https://www.npmjs.com/package/cheerio
- https://github.com/cheeriojs/cheerio
- https://cheerio.js.org/
- https://cheerio.js.org/docs/api

basé sur htmlparser2

- https://www.npmjs.com/package/htmlparser2

# bootstrap resources

## sidebar 

https://getbootstrap.com/docs/5.0/examples/sidebars/#
https://dev.to/codeply/bootstrap-5-sidebar-examples-38pb

## navbar

https://getbootstrap.com/docs/5.0/components/navbar/

## bootstrap + navbar
https://mdbootstrap.com/docs/react/navigation/navbar/

## bootstrap icons + npm
https://stackoverflow.com/questions/57619850/bootstrap-icons-not-appear-in-the-react-js-project

# java sparks OAuth + JWT ?

https://github.com/pac4j/spark-pac4j


