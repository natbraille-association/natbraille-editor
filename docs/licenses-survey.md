# Licenses survey

```
> npx license-checker --production --summary
├─ MIT: 57
├─ BSD-2-Clause: 8
├─ Apache-2.0: 7
├─ ISC: 4
├─ Python-2.0: 1
├─ AGPL-3.0-or-later: 1
└─ MIT*: 1
```

1. caniuse-lite@1.0.30001522 CC-BY-4.0 (Unknown) - possibly incompatible with AGPL-3.0-or-later (Network Protective)

```
├─┬ @vitejs/plugin-react@4.0.4
│ ├─┬ @babel/core@7.22.10
│ │ ├─┬ @babel/helper-compilation-targets@7.22.10
│ │ │ ├─┬ browserslist@4.21.10
│ │ │ │ ├── caniuse-lite@1.0.30001522
```

OK: canius-lite is a repack of canius-db, which use the same CC license ; canius-db is only data, so ok

2. type-fest@0.20.2 (MIT OR CC0-1.0) (Unknown) - possibly incompatible with AGPL-3.0-or-later (Network Protective)

```
├─┬ eslint@8.47.0
│ ├─┬ globals@13.21.0
│ │ └── type-fest@0.20.2
```

OK: MIT 


4. html@1.0.0 BSD (Unknown) - possibly incompatible with AGPL-3.0-or-later (Network Protective

```
├─┬ prosemirror-dev-tools@4.0.0
│ ├─┬ html@1.0.0
```

OK:contains the MIT license but package.json says BSD
