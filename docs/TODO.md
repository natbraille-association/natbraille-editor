++ make impossible to reregister with a mail
++ password reset procedure

# server

- [ ] g2 : locutions
- [ ] g2 : word translation depends on following punctation sign
- [ ] layout
    - [ ] hyphenation
    - [ ] page number position
    - [ ] empty lines -> empty lines
    - [ ]
- [x] chimie : <math chemistry="true" -> span / paragraphe style de caractère chimie
- [ ] fromodt/todot : take chemistry style name from USER stylist natfilteroption
- [ ] chimie/math : p6 p6p3
- [ ] check https://github.com/nmondal/spark-11 because spark is ~ dead
- [ ] a href and urls and emails

## minor

- [ ] \n is not a letter

# client

- [ ] better source -> dest mapping 
    - [ ] fix math mapping

- [ ] ? a page for ui options.
- [x] reset editor
- [ ] split view
- [ ] mode ligne équialent noire / braille (soit scouper le noir pour faire correspondre ux conraintes de mise en page, sauf couper braille pour faire correspondre à l'apparence)
- [ ] prefxer direct tous les mats en basculant vers detrans 
- [ ] à l'ouverture de l'éditeur de formules, focus sur le champs starmath formule.
- [ ] bug saisie équation syncrhonisatio quand requètes en série ne sont pas envoyées
- [ ] configurable shortcut to jump to braille view
- [ ] rework file management.
- [ ] delete all options/document in local storage when logout (ensure file is saved?) / login
- [x] latex input for equations (anything that can output mathml) https://katex.org/docs/options
- [ ] save option as stopped working.

## minor

- [/] mathml editor overflow
- [ ] mathml editor : signal when mathml preview is yet not up to date (due to 1s debounce)
# both

- [ ] successive word count for UPPERCASE PASSAGE

- [x] make registration procedure more precise (user do not click on the link, or not on the mail and try to login immediatly)

- [ ] g2 : propernames
- [ ] g2 : ambiguities
- [ ] empahsis

- [ ] fr-g2.xsl + coupures.xsl? -> account for Urls
- 
- [x] chemistry
- [ ] pretypeset braille inline
- [x] pretypeset braille blocs
- [ ] tables

- [ ]? ui options server export/import 
- 
- [x] import word

- [ ] when server is restart, client should logout (and message ?) user

- [ ] OFFLINE MODE 

# other

- [ ] odt document with all formating styles and matching default editor 
- [ ] braille title lettering/numbering 
- [ ] keep odt title lettering/numbering 

- [ ] braille toc

#########


=> Boutons reset pour les options : 
    - tout
    - options de transcription
    - options d'ui.
    
=> Noms propres
    - marquer les noms propres "à la main" (style de caractère)
    - alt. Un assistant oui/non/jamais/toujours
    - sur tout interaction à l'intérieur du mot, on enlève la marque de nom propre.
    - pour le moment, pas de réutilisation de la liste entre documents.

=> Voir s'il y a une api pour lire le "tableau blanc interactif" (TBI)

-  [ ] corriger doc dfault pour inclure chimie
.


# ui
- interface homepage
-- 1 texte
-- 2 email retour
-- 3 partenaires
-- 4versions


+ contact dans le menu
bug suggestion amelioration autre


# libreoffice headless

[x] did synchronize unconvert/unoserver calls as it's unable to serve concurrently without hanging libreoffice. This might be slower (but it might not be the case if libreoffice headless process request as queue), but seems a lot more stable. We might need a pool of libreoffice headless / unoserver and multiple queues. 
[/] Maybe do XMLRPC directly in java.   


# instance

- [/] update "email"."fromAddress":  in server.conf.jso


# quick + blocking

- [ ] integration of welcome screen / login screen (blocks login when url is not /login)
- [x] check logos
- [x] fix disconnect 
- [x] better lost connexion + !! SERVER: remove token prior to db export.

# quick

- [ ] cleanup change between equation format in preview + preview before any keystroke.


# alain

- [ ] Sur l'écran d'édition d'équation l'affichage n'est pas complètement cohérent, mais la possibilité d'écrire en latex, ça marche bien ! tu affiches saisissez l'équation au format starmath... alors que je peux choisir Latex aussi.
- [/] Synchro curseur
- [x] handle invalid session token.


# misc

- [/] record (esp unammed documents)
- [ ] insérer l'eq starmath m^me quand latex. Et div pb mise à jour.
- [x] moving cursor should not retriger a translation
- [ ] last document is reopened from localstorage and looses its name

- [x] go to braille view on (configurable) shortcut

- [x] détranscription : keep y-scroll on editors

- [ ] react error reporting "You can provide a way better UX than this when your app throws errors by providing your own ErrorBoundary or errorElement prop on your route."

- [ ] password reset cleanup
    - [ ] uniform hidden fields
    - [x] form round corners
    - [?] "votre inscription est presque terminée" quand le challenge est envoyé et qu'on va sur la page login.
    - [x] remove cookies on password reset.
    - [x] printStackTrace in sql ex. user


# bruno

- pagination (num de pages)
- styles custom 
    - retrait 1 ligne retrait lignes suivantes 
    - centrage
    - carctères particuliers devant et derriere -> note ntr. ->poesie
    - style a l'nvers + tard...

- style g0 g1 g2 caractere ou paragraphe.
- g0 = bloc braille + texte. =>  retouches
- pas de coupure en fin de page (mettre une option).
- gras mev.
- plus tard != niveau m en evidence.


--> tableaux : recherche.

- bandeau de couleur pour la version dev.

- [ ] replace   Process process = Runtime.getRuntime().exec(commandArray.toArray(new String[0])); by ProcessBuilder.


- [x] mise en évidence dans l'ui.
- [x] détrans vertical scrollbar
- [ ] right side of braille page end of paper sheet
- [/] resize handle

- [?] partie droite, le braille pourrait revenir à la ligne (et on voit )
- [x] menus -> when tool small, display a burger menu
- [x] ? redime ntionnement central en % ??


- [ ] tooltip styles h1 h2 p
- [x] check <BrailleColorsStyle> role? in SplitEditor
- [ ] check reverse trans when using non braille coding
- [ ] braille font in reverse trans
- [x] add headerbar burger menu when windows too narrow
- [x] braille en couleur ne fonctionne plus...
- [x] logos + petit
- [/] header + menu goes under left/right menu bar (only in old firefox versions)
- [ ] last modification/save.
- [x] title/h.. won't work on chrome

- [x] proper page split.


-----

- [x] déconnexion à mettre à droite sur l’UI avec les options pour gérer le profil, à la place de “Natbraille”
- [ ] Correspondance sélection noire/sélection Braille
- [ ] Ce qui reste à faire avant de publier l’outil, il faut automatiser le fait qu’un fichier se ferme correctement quand on s’arrête.
- [ ] Polir l’éditeur de maths

-------

- [ ] braille no-layout view using noir structure
- [ ] perkins input in noir -> enforce key choice ; enforce braille table in input and in display.


-----

- [ ] bold odt import/export
- [ ] g0 odt import/export

- [x] strange function remains on nav header links

- [ ] path("/api/users/:username" + ... are not token protected!!!

- [ ] merge FeedbackDiffusion and RegistrationEmailSender

- [ ] Found 2 elements with non-unique id #username in contact form
- [ ] test /api/auth/user-email if username is an unicode chat (or non urlencoded)
- [ ] options ref (#Options) do not work


-----
- [ ] mathml editor : focus on input field when open
- [ ] remove profile

-----
@benoit

- [ ] check fix username/password autocomplete


- [ ] menu au muavsie endroit.
- [ ] menu "natbraille" reste à droite quand non connecté.


------
- [ ] save as new untitled even if loaded from document. 

----
- [ ] maths combination with strong/g0/g1/g2

- [ ] import/export of g0/g1/g2 to odt
- [ ]

- [ ] g1 g2 tests par utilisateur : est-ce visible par lecteur ???
- [ ] chimie saisie en latex ?


- [ ] the base funtion for endpoints keeps url parameters. It should not, but the server might not work without as it should rely on hidden form input.

- [ ] save UI options to server
- [ ] UI option : bool show default doc when none

- [ ] documnent filename is lost when exiting split-editor view


- [ ] review/test save braille quirky

- [ ] [tidy] Move hamburger menu out of editor/
- [ ] The default value for OptionContext might not be needed. The default value is used only if the context is used outside a provider which is never the case.
- [ ] [tidy] replace useContext(OptionContext) by useOptionsContext
- [ ] [even] use EditedFileContext everywhere (last date) + what happens when no document content found in localstorage but a document filename found in localstorage

- [ ]  all non HTML attributes in HTML should be data-* prefixed
- [ ] [even] find a way for better coupling of declarative/code toolbar combos